#ifndef LPC_CONFIG_H_
#define LPC_CONFIG_H_

/*================================================================================================*
 * ---- Константы, зависящие от конфигурации устройства ----
 * Данный файл включается в lpc_io.h и использует определенные там идентификаторы
 * LPC_CLKSEL_..., LPC_CLKCFG(), LPC_BASECFG()
 *================================================================================================*/

/* ОГРАНИЧЕНИЯ LPC:
   Нельзя использовать CAN0 с I2C1, DAC, ADC0, ADC1
   Нельзя использовать CAN1 с I2C0, MCPWM, I2S

   ОГРАНИЧЕНИЯ ПРОГРАММНОЙ РЕАЛИЗАЦИИ LPC_hardware_init:
   - источником PLL1 может быть только IRC, GPCLKIN или OSC (можно через IDIV)
   - PLL0USB только на 480 МГц и только для ограниченного набора частот источника
   - нет инициализации PLL0AUDIO
 */

#ifdef BOARD_MCB43000
#define LPC_CLKF_OSC        12000000UL             /* Частота кварца OSC, Гц; 0 = OSC не используется */

#define LPC_CLKF_GPCLKIN    0UL      /* Частота внешнего входа, 0 = не используется */

#define LPC_PLL1_SRC        LPC_CLKSEL_OSC
#else
#define LPC_CLKF_OSC        0UL             /* Частота кварца OSC, Гц; 0 = OSC не используется */

#define LPC_CLKF_GPCLKIN    20000000UL      /* Частота внешнего входа, 0 = не используется */
#define LPC_GPCLKIN_PIN_ID  LPC_PIN_P4_7_GP_CLKIN

#define LPC_PLL1_SRC        LPC_CLKSEL_GPCLKIN
#endif


/* PLL1 (используется режим FBSEL = 0)
 * рабочая частота Fcco = Fin * M / N = 156...320 МГц, выходная Fout = Fcco / P
 */
#define LPC_USE_PLL1        1               /* Включение PLL1 */

#ifdef BOARD_MCB43000
#define LPC_PLL1_M          50              /* M = 1..256           (Fcco = Fin * M / N) */
#define LPC_PLL1_N          3               /* N = 1..4 */
#else
#define LPC_PLL1_M          10              /* M = 1..256           (Fcco = Fin * M / N) */
#define LPC_PLL1_N          1               /* N = 1..4 */
#endif
#define LPC_PLL1_P          1               /* P = 1, 2, 4, 8, 16   (Fout = Fcco / P) */
#define LPC_PLL1_AUTOBLK    0

#define LPC_USE_PLL0USB     1               /* 1 = инициализировать PLL0USB */
#define LPC_PLL0USB_SRC     LPC_CLKSEL_GPCLKIN
#define LPC_PLL0USB_AUTOBLK 0

/* --- Настройка вторичных тактовых генераторов ---- */

/* Конфигурация делителей IDIVx:       src                  div autoblock */
#define LPC_CLKDIVA_CFG     LPC_CLKCFG(LPC_CLKSEL_IRC,      0,  0) /* div <= 4 */
#define LPC_CLKDIVB_CFG     LPC_CLKCFG(LPC_CLKSEL_IRC,      0,  0) /* div <= 16 */
#define LPC_CLKDIVC_CFG     LPC_CLKCFG(LPC_CLKSEL_IRC,      0,  0) /* div <= 16 */
#define LPC_CLKDIVD_CFG     LPC_CLKCFG(LPC_CLKSEL_IRC,      0,  0) /* div <= 16 */
#define LPC_CLKDIVE_CFG     LPC_CLKCFG(LPC_CLKSEL_PLL1,     8,  0) /* div <= 256 */ //@@

/* Конфигурация базовых генераторов:   src                      autoblock */
/* CORE: Тактовая частота ядра и основной периферии */
#define LPC_BASE_M4_CFG     LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
/* APB1: MCPWM, I2C0, I2S, CAN1 */
#define LPC_BASE_APB1_CFG   LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
/* APB3: I2C1, DAC, ADC0, ADC1, CAN0 */
#define LPC_BASE_APB3_CFG   LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
/* Индивидуальные клоки для отдельных устройств */
#define LPC_BASE_ADCHS_CFG  LPC_BASECFG(LPC_CLKSEL_IRC,         0)
#define LPC_BASE_AUDIO_CFG  LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_LCD_CFG    LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_PERIPH_CFG LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_PHY_RX_CFG LPC_BASECFG(LPC_CLKSEL_ENET_RX,     0)
#define LPC_BASE_PHY_TX_CFG LPC_BASECFG(LPC_CLKSEL_ENET_TX,     0)  
#define LPC_BASE_SDIO_CFG   LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_SPI_CFG    LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_SPIFI_CFG  LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_SSP0_CFG   LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_SSP1_CFG   LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_UART0_CFG  LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_UART1_CFG  LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_UART2_CFG  LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_UART3_CFG  LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_USB0_CFG   LPC_BASECFG(LPC_CLKSEL_PLL0USB,     0)
#define LPC_BASE_USB1_CFG   LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_OUT_CFG    LPC_BASECFG(LPC_CLKSEL_IDIVE,       0) //@@
#define LPC_BASE_CGOUT0_CFG LPC_BASECFG(LPC_CLKSEL_PLL1,        0)
#define LPC_BASE_CGOUT1_CFG LPC_BASECFG(LPC_CLKSEL_GPCLKIN,     0) //@@

/* 1 = все базы всегда включены, 0 = определяется по LPC_CLKEN_... */
#define LPC_CLK_BASES_ALWAYS_ON 0

/* если 1, то включается деление входных клоков на 2 */
#define LPC_CLK_EMC_DIV2        1

#define LPC_CLK_DELAY_EMC       0x7777

/* Включение отдельных устройств */
#define LPC_CLKEN_ADC0          0
#define LPC_CLKEN_ADC1          0
#define LPC_CLKEN_ADCHS         0   /* LPC4370 */
#define LPC_CLKEN_CAN0          0
#define LPC_CLKEN_CAN1          0
#define LPC_CLKEN_DAC           0
#define LPC_CLKEN_DMA           1
#define LPC_CLKEN_EEPROM        0   /* all LPC43xx with flash */
#define LPC_CLKEN_EMC           1
#define LPC_CLKEN_ENET          0   /* LPC433x, 5x */
#define LPC_CLKEN_I2C0          0
#define LPC_CLKEN_I2C1          0
#define LPC_CLKEN_I2S           0
#define LPC_CLKEN_LCD           0   /* LPC4350 */
#define LPC_CLKEN_M0APP         1
#define LPC_CLKEN_MCPWM         0
#define LPC_CLKEN_PERIPH        0   /* LPC4370 M0 subsystem */
#define LPC_CLKEN_QEI           0   /* LPC433x, 5x */
#define LPC_CLKEN_RITIMER       0
#define LPC_CLKEN_SCT           0
#define LPC_CLKEN_SDIO          0
#define LPC_CLKEN_SPI           0
#define LPC_CLKEN_SPIFI         0
#define LPC_CLKEN_SSP0          1
#define LPC_CLKEN_SSP1          0
#define LPC_CLKEN_TIMER0        1 //@@
#define LPC_CLKEN_TIMER1        0
#define LPC_CLKEN_TIMER2        0
#define LPC_CLKEN_TIMER3        0
#define LPC_CLKEN_UART0         0
#define LPC_CLKEN_UART1         0
#define LPC_CLKEN_UART2         1
#define LPC_CLKEN_UART3         1
#define LPC_CLKEN_USB0          0   /* LPC432x, 3x, 5x */
#define LPC_CLKEN_USB1          0   /* LPC433x, 5x */
#define LPC_CLKEN_WDT           1
#define LPC_CLKEN_OUT           1 //@@
#define LPC_CLKEN_CGOUT0        0
#define LPC_CLKEN_CGOUT1        1 //@@


#define LPC_INIT_WDT   0


/* --------------- Настройки SDRAM ---------------------------------*/

#define LPC_EMC_CONFIG          1

#ifdef BOARD_MCB43000
    #define LPC_EMC_ADDR_WIDTH          15

    #define LPC_SDRAM0_EN                1

    #define LPC_SDRAM0_SIZE              128
    #define LPC_SDRAM0_DATA_BUS_WIDTH    32
    #define LPC_SDRAM0_DEV_BUS_WIDTH     32
    #define LPC_SDRAM0_COL_CNT           8

    #define LPC_SDRAM0_LATENCY_CAS  3
    #define LPC_SDRAM0_LATENCY_RAS  3
    #define LPC_SDRAM_READ_DELAY   EMC_DYN_READ_CONFIG_DEL_0_5

    #define LPC_SDRAM_T_REFRESH    EMC_NANOSECOND(64000000 / 4096)

    /*  Precharge Command Period */
    #define LPC_SDRAM_T_RP          EMC_NANOSECOND(18)
    /* Active to Precharge Command Period  */
    #define LPC_SDRAM_T_RAS         EMC_NANOSECOND(42)
    /* Self Refresh Exit Time */
    #define LPC_SDRAM_T_SREX        LPC_SDRAM_T_XSR
    /* Last Data Out to Active Time  */
    #define LPC_SDRAM_T_APR         EMC_NANOSECOND(0)
    /* Data In to Active Command Time */
    #define LPC_SDRAM_T_DAL         EMC_NANOSECOND(0)
    /* Write Recovery Time (DPL)*/
    #define LPC_SDRAM_T_WR          EMC_NANOSECOND(12)
    /* Active to Active Command Period */
    #define LPC_SDRAM_T_RC          EMC_NANOSECOND(60)
    /* Auto-refresh Period */
    #define LPC_SDRAM_T_RFC         EMC_NANOSECOND(60)

    /* Exit Self Refresh active command time */
    #define LPC_SDRAM_T_XSR         EMC_NANOSECOND(70)
    /* Active bank A to active bank B latency */
    #define LPC_SDRAM_T_RRD         EMC_NANOSECOND(12)
    /* Load Mode register to Active Command Time */
    #define LPC_SDRAM_T_MRD         0


    #define LPC_SDRAM_T_APR_CYCLES      1
    #define LPC_SDRAM_T_DAL_CYCLES      5
    #define LPC_SDRAM_T_MRD_CYCLES      2
#else

    #define LPC_EMC_ADDR_WIDTH           15

    #define LPC_SDRAM0_EN                1
    #define LPC_SDRAM0_SIZE              256
    #define LPC_SDRAM0_DATA_BUS_WIDTH    16
    #define LPC_SDRAM0_DEV_BUS_WIDTH     16
    #define LPC_SDRAM0_COL_CNT           9


    #define LPC_SDRAM0_LATENCY_CAS  2
    #define LPC_SDRAM0_LATENCY_RAS  2
    #define LPC_SDRAM_READ_DELAY   EMC_DYN_READ_CONFIG_DEL_0_5

    #define LPC_SDRAM_T_REFRESH    EMC_NANOSECOND(64000000 / 8192)

    /*  Precharge Command Period */
    #define LPC_SDRAM_T_RP          EMC_NANOSECOND(15)
    /* Active to Precharge Command Period  */
    #define LPC_SDRAM_T_RAS         EMC_NANOSECOND(45)
    /* Self Refresh Exit Time */
    #define LPC_SDRAM_T_SREX        LPC_SDRAM_T_XSR
    /* Last Data Out to Active Time  */
    #define LPC_SDRAM_T_APR         EMC_NANOSECOND(10)
    /* Data In to Active Command Time */
    #define LPC_SDRAM_T_DAL         EMC_NANOSECOND(30)
    /* Write Recovery Time (DPL)*/
    #define LPC_SDRAM_T_WR          EMC_NANOSECOND(15)
    /* Active to Active Command Period */
    #define LPC_SDRAM_T_RC          EMC_NANOSECOND(68)
    /* Auto-refresh Period */
    #define LPC_SDRAM_T_RFC         EMC_NANOSECOND(64)

    /* Exit Self Refresh active command time */
    #define LPC_SDRAM_T_XSR         EMC_NANOSECOND(75)
    /* Active bank A to active bank B latency */
    #define LPC_SDRAM_T_RRD         EMC_NANOSECOND(15)
    /* Load Mode register to Active Command Time */
    #define LPC_SDRAM_T_MRD         EMC_NANOSECOND(14)


    #define LPC_SDRAM_T_RP_CYCLES       2
    #define LPC_SDRAM_T_RAS_CYCLES      5
    #define LPC_SDRAM_T_SREX_CYCLES     0
    #define LPC_SDRAM_T_APR_CYCLES      2
    #define LPC_SDRAM_T_DAL_CYCLES      4
    #define LPC_SDRAM_T_WR_CYCLES       2
    #define LPC_SDRAM_T_RC_CYCLES       7
    #define LPC_SDRAM_T_RFC_CYCLES      0
    #define LPC_SDRAM_T_XSR_CYCLES      0
    #define LPC_SDRAM_T_RRD_CYCLES      0
    #define LPC_SDRAM_T_MRD_CYCLES      2


    #define LPC_SRAM0_EN        1
    #define LPC_SRAM1_EN        1
    #define LPC_SRAM0_WIDTH     16
    #define LPC_SRAM1_WIDTH     16
    #define LPC_SRAM0_BLS_RW    1
    #define LPC_SRAM1_BLS_RW    1

#if 0
    #define LPC_SRAM_T_WEN_CYCLES   1//1
    #define LPC_SRAM_T_OEN_CYCLES   0 //2
    #define LPC_SRAM_T_RD_CYCLES    2 //3
    #define LPC_SRAM_T_PAGE_CYCLES  2
    #define LPC_SRAM_T_WR_CYCLES    2//2
    #define LPC_SRAM_T_TURN_CYCLES  2//2

    #define LPC_SRAM0_PAGE_MODE 1
    #define LPC_SRAM1_PAGE_MODE 1
#else
    #define LPC_SRAM_T_WEN_CYCLES   0//1
    #define LPC_SRAM_T_OEN_CYCLES   0 //2
    #define LPC_SRAM_T_RD_CYCLES    3 //3
    #define LPC_SRAM_T_PAGE_CYCLES  2
    #define LPC_SRAM_T_WR_CYCLES    0//2
    #define LPC_SRAM_T_TURN_CYCLES  0//2

    #define LPC_SRAM0_PAGE_MODE 1
    #define LPC_SRAM1_PAGE_MODE 1
#endif

#endif










/*================================================================================================*/
#endif /* LPC_CONFIG_H_ */

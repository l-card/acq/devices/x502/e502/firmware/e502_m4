#ifndef STREAMS_PRIV_H
#define STREAMS_PRIV_H

#include "cycle_buf.h"
#include "e502_cm4_defs.h"

#define STREAM_OUT_PARTS_MAX_CNT       256

#define OUT_CYCLE_MIN_SIZE  1024

typedef enum {
    STREAM_REQ_NONE = 0,
    STREAM_REQ_START = 1,
    STREAM_REQ_STOP  = 2
} t_stream_req;


typedef struct {
    uint32_t *addr;
    uint32_t size;
} t_out_parts;



/* признак, запущен ли поток в данном направлении */
extern volatile int streams_in_run;
extern volatile t_stream_req streams_in_req;
extern volatile int streams_out_run;
extern volatile t_stream_req streams_out_req;
extern volatile int m0_sdram_test_run;
extern volatile t_stream_req m0_sdram_test_req;

extern int stream_out_part_mode;

extern int stream_out_cycle_mode;
/* запрос на смену страницы генерации в циклическом режиме */
extern volatile int stream_out_cycle_sw_req;
/* запрос на начало загрузки новой страницы */
extern volatile int stream_out_cycle_load_req;
/* размер циклического буфера для загрузки */
extern uint32_t stream_out_cycle_load_size;
/* размер циклического буфера для генерации */
extern uint32_t stream_out_cycle_gen_size;
/* флаги при запросе смены страницы или останове */
extern uint32_t stream_out_cycle_sw_flags;

extern uint32_t stream_out_cycle_multipler;


extern t_cycle_buf buf_in_iface;
extern t_cycle_buf buf_out_iface;
extern t_cycle_buf buf_in_sdram;
extern t_cycle_buf buf_out_sdram;


extern volatile uint32_t out_part_wr_start;
extern volatile uint32_t out_part_wr_done;
extern volatile uint32_t out_part_rd_done;
extern volatile t_out_parts out_parts[];





t_e502_cm4_errs priv_stream_start(volatile t_stream_req *stream_req, volatile int *stream_run, const char *name);
t_e502_cm4_errs priv_stream_stop(volatile t_stream_req *stream_req, volatile int *stream_run, const char *name);

#endif // STREAMS_PRIV_H


#ifndef FPGA_H
#define FPGA_H

#include <stdint.h>

#include "e502_cm4_defs.h"
#include "e502_fpga_regs.h"

#define PIN_FPGA_NCONFIG    LPC_PIN_P6_1_GPIO3_0
#define PIN_FPGA_NSTATUS    LPC_PIN_PE_0_GPIO7_0
#define PIN_FPGA_USER_MODE  LPC_PIN_P3_8_GPIO5_11
#define PIN_FPGA_REGS_CS    LPC_PIN_P3_8_SSP0_SSEL





int fpga_is_loaded(void);
t_e502_cm4_errs fpga_firm_write_to_flash(const uint8_t *firm, uint32_t size);
t_e502_cm4_errs fpga_firm_load(void);
t_e502_cm4_errs fpga_reg_read(uint16_t addr, uint32_t *val);
t_e502_cm4_errs fpga_reg_write(uint16_t addr, uint32_t val);

#endif // FPGA_H

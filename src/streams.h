#ifndef E502_PROC_H
#define E502_PROC_H

#include <stdint.h>
#include "e502_cm4_defs.h"

#define STREAM_IN_BUF_SIZE          (4*1024*1024)
#define STREAM_OUT_BUF_SIZE         (3*1024*1024)
#define STREAM_OUT_CYCLE_BUF_CNT    (2)
#define STREAM_OUT_CYCLE_BUF_SIZE   (STREAM_OUT_BUF_SIZE/STREAM_OUT_CYCLE_BUF_CNT)

extern uint32_t in_buf[STREAM_IN_BUF_SIZE];
extern uint32_t out_buf[STREAM_OUT_BUF_SIZE];

#define X502_STREAM_IN_MSG_END       0x01010001


void e502_proc_start(void);
void e502_proc_stop(void);
void e502_proc_pull(void);



typedef int32_t (*t_stream_tx_data_func)(uint32_t *data, uint32_t len);
typedef int32_t (*t_stream_tx_clear_func)(void);

typedef int32_t (*t_stream_rx_start_func)(uint32_t *data, uint32_t len);
typedef int32_t (*t_stream_rx_clear_func)(void);


typedef struct {
    uint32_t tx_max_step;
    t_stream_tx_data_func tx_data;
    t_stream_tx_clear_func tx_clear;
} t_stream_in_iface;

typedef struct {
    int part_mode;
    uint32_t rx_min_buf;
    t_stream_rx_start_func rx_start;
    t_stream_rx_clear_func rx_clear;
} t_stream_out_iface;





void streams_hard_init(void);

int32_t stream_in_set_step(uint16_t step_size, uint8_t flags);
int32_t stream_in_start(const t_stream_in_iface *iface);
int32_t stream_in_stop(void);
int stream_in_is_running(void);
void stream_in_send_done(uint32_t size);
void stream_in_iface_closed(const t_stream_in_iface *iface);

t_e502_cm4_errs stream_out_start(const t_stream_out_iface *iface);
t_e502_cm4_errs stream_out_stop(void);
int stream_out_is_running(void);
void stream_out_recv_done(uint32_t *addr, uint32_t size);
void stream_out_iface_closed(const t_stream_out_iface *iface);

t_e502_cm4_errs stream_out_cycle_load_start(const t_stream_out_iface *iface, uint32_t size);
t_e502_cm4_errs stream_out_cycle_setup(uint32_t flags);
t_e502_cm4_errs stream_out_cycle_stop(uint32_t flags);

int stream_out_cycle_setup_done(void);


void stream_pull(void);

#endif // E502_PROC_H

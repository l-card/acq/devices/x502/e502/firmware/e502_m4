#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stddef.h>

#include "e502_cm4_defs.h"
#include "l502_bf_cmd_defs.h"
#include "dbg_config.h"
#include "chip.h"

#include "fpga.h"
#include "ltimer.h"
#include "lprintf.h"

#define BF_CMD_DEFAULT_TOUT  150

#define BF_LDR_HDR_SIZE  (16)
#define BF_LDR_HDRSGN    (0xAD)

#define BF_LDR_HDRPOS_SGN  (3)

#define BF_LDR_FLAG_SAVE      (0x0010) //не используется
#define BF_LDR_FLAG_AUX       (0x0020) //не используется
#define BF_LDR_FLAG_FILL      (0x0100)
#define BF_LDR_FLAG_QUICKBOOT (0x0200) //не используется
#define BF_LDR_FLAG_CALLBACK  (0x0400) //не используется
#define BF_LDR_FLAG_INIT      (0x0800) //не используется
#define BF_LDR_FLAG_IGNORE    (0x1000)
#define BF_LDR_FLAG_INDIRECT  (0x2000) //не используется
#define BF_LDR_FLAG_FIRST     (0x4000)
#define BF_LDR_FLAG_FINAL     (0x8000)

#define BF_WAIT_LOAD_RDY_TOUT  500

#define LDR_BUFF_SIZE 4096


/** Стандартный таймаут на выполнение запроса к BlackFin в мс */
#define BF_REQ_TOUT  500


#define BF_CHECK_ADDR(addr)  (((addr) < 0xFFA0C000) && ((addr)>= 0xFFA0000)) || \
    (((addr) < 0xFF908000) && ((addr) >=0xFF900000)) || \
    (((addr) < 0xFF808000) && ((addr) >=0xFF800000)) || \
    (((addr) < 0x2000000)) ? 0 : E502_CM4_ERR_BF_INVALID_ADDR

#define BF_CHECK_ADDR_SIZE(addr, size) BF_CHECK_ADDR(addr) ? E502_CM4_ERR_BF_INVALID_ADDR : \
    BF_CHECK_ADDR(addr+size*4-1) ? E502_CM4_ERR_BF_INVALID_ADDR : 0


#define BF_CMD_FIRST_DATA_BLOCK_SIZE ((E502_BF_REQ_DATA_SIZE_MAX-offsetof(t_l502_bf_cmd, data))/4)

typedef struct st_bf_ldr_pkt {
    uint8_t res;
    uint8_t dma_mode;
    uint16_t flags;
    uint32_t addr;
    uint32_t size;
    uint32_t arg;
} t_bf_ldr_pkt;

static t_l502_bf_cmd f_cur_cmd;

/* Разбираем заголовок блока LDR-формата из буфера размером BF_LDR_HDR_SIZE
   и сохраняем параметры в структуре pkt */
int f_parse_ldr_hdr(const uint8_t* hdr, t_bf_ldr_pkt* pkt) {
    int err = 0;
    uint32_t* pdw_buff = (uint32_t*)hdr;
    uint8_t xor_ch = 0;
    int i;
    for (i=0; i < BF_LDR_HDR_SIZE; i++) {
        xor_ch ^= hdr[i];
    }

    if ((xor_ch!=0) || (hdr[BF_LDR_HDRPOS_SGN] != BF_LDR_HDRSGN)) {
        err = E502_CM4_ERR_LDR_FILE_FORMAT;
    } else {
        pkt->res = 0;
        pkt->dma_mode = pdw_buff[0]&0xF;
        pkt->flags = pdw_buff[0]&0xFFF0;
        pkt->addr = pdw_buff[1];
        pkt->size = pdw_buff[2];
        pkt->arg = pdw_buff[3];

        if ((pkt->flags & BF_LDR_FLAG_INIT) && (pkt->flags & BF_LDR_FLAG_FILL))
            err = E502_CM4_ERR_LDR_FILE_FORMAT;
        else if (pkt->flags & (BF_LDR_FLAG_CALLBACK | BF_LDR_FLAG_INDIRECT | BF_LDR_FLAG_INIT))
            err = E502_CM4_ERR_LDR_FILE_UNSUP_FEATURE;
        else if ((pkt->flags & BF_LDR_FLAG_INIT) && (pkt->addr != 0xFFA00000))
            err = E502_CM4_ERR_LDR_FILE_UNSUP_STARTUP_ADDR;

        if (!err) {
#if PRINT_DBG_BF_LDR_PARSE
            lprintf("ldr hdr: flags = 0x%4X, add = 0x%08X, size = %d, arg = 0x%08X\n",
                    pkt->flags, pkt->addr, pkt->size, pkt->arg);
#endif
        } else {
            lprintf("parse hdr err = %d\n", err);
        }
    }
    return err;
}

static t_e502_cm4_errs f_bf_wait_cmd_done(void) {
    int32_t err = 0;
    t_ltimer tmr;
    uint32_t status;
    ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(BF_REQ_TOUT));
    do {
        err = fpga_reg_read(E502_REGS_BF_STATUS, &status);
    } while ((status &  E502_REGBIT_BF_STATUS_BUSY_Msk) &&
             !err && !ltimer_expired(&tmr));

    if (!err && (status & E502_REGBIT_BF_STATUS_BUSY_Msk))
        err = E502_CM4_ERR_BF_REQ_TIMEOUT;
    return err;
}


t_e502_cm4_errs bf_mem_wr(uint32_t addr, const uint32_t* regs, uint32_t size, int fill) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    if (!err)
        err = f_bf_wait_cmd_done();

    /* данные записываем блоками по L502_BF_REQ_DATA_SIZE */
    while (!err && size) {
        int i;
        int put_size = (size < E502_BF_REQ_DATA_SIZE_MAX) ? size :
                                         E502_BF_REQ_DATA_SIZE_MAX;
        /* записываем блок данных в буфер ПЛИС */
        for (i=0; (i < put_size) && !err; i++) {
            err = fpga_reg_write(E502_REGS_BF_REQ_DATA+i, regs[fill ? 0 : i]);
        }

        /* записываем переметры передачи - размер и адрес в памяти BlackFin */
        if (!err)
            err = fpga_reg_write(E502_REGS_BF_REQ_SIZE, put_size);
        if (!err)
            err = fpga_reg_write(E502_REGS_BF_REQ_ADDR, addr);
        /* даем команду на запис */
        if (!err)
            err = fpga_reg_write(E502_REGS_BF_CMD, E502_BF_CMD_WRITE);

        /* ждем, пока операция не будет завершена */
        if (!err)
            err = f_bf_wait_cmd_done();

        if (!err) {
            size -= put_size;
            addr += put_size*4;
            if (!fill)
                regs += put_size;
        }
    }
    return err;
}


t_e502_cm4_errs bf_mem_rd(uint32_t addr, uint32_t* regs, uint32_t size) {
    int err = E502_CM4_ERR_OK;

    if (!err)
        err = f_bf_wait_cmd_done();

    while (!err && size) {
        int i;
        int get_size = (size < E502_BF_REQ_DATA_SIZE_MAX) ? size :
                                                       E502_BF_REQ_DATA_SIZE_MAX;

        /* записываем переметры передачи - размер и адрес в памяти BlackFin */
        if (!err)
            err = fpga_reg_write(E502_REGS_BF_REQ_SIZE, get_size);
        if (!err)
            err = fpga_reg_write(E502_REGS_BF_REQ_ADDR, addr);
        /* даем команду на запис */
        if (!err)
            err = fpga_reg_write(E502_REGS_BF_CMD, E502_BF_CMD_READ);

        /* ждем, пока операция не будет завершена */
        if (!err)
            err = f_bf_wait_cmd_done();

        /* записываем блок данных в буфер ПЛИС */
        for (i=0; (i < get_size) && !err; i++) {
            err = fpga_reg_read(E502_REGS_BF_REQ_DATA+i, &regs[i]);
        }

        if (!err) {
            size -= get_size;
            regs += get_size;
            addr += get_size*4;
        }
    }
    return err;
}


t_e502_cm4_errs bf_cmd_start(uint16_t cmd_code, uint32_t par,
                      const uint32_t* data, uint32_t size) {
    /** @todo проверка, что нет незавершенной команды */
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    if (size > L502_BF_CMD_DATA_SIZE_MAX)
        err = E502_CM4_ERR_BF_INVALID_CMD_DATA_SIZE;

    if (!err) {
        f_cur_cmd.code = cmd_code;
        f_cur_cmd.data_size = size;
        f_cur_cmd.param = par;
        f_cur_cmd.result = 0;
        f_cur_cmd.status = L502_BF_CMD_STATUS_REQ;

        if (size) {
            memcpy(f_cur_cmd.data, data, size*sizeof(data[0]));
        }

        /* если размер больше, чем можем записать за раз, то сперва записываем
         * хвост, чтобы не получилось, что информация о команде будет записана
         * раньше данных */
        if (size > BF_CMD_FIRST_DATA_BLOCK_SIZE) {
            err = bf_mem_wr(E502_BF_MEMADDR_CMD+E502_BF_REQ_DATA_SIZE_MAX*4,
                              &f_cur_cmd.data[BF_CMD_FIRST_DATA_BLOCK_SIZE],
                              size - BF_CMD_FIRST_DATA_BLOCK_SIZE, 0);
            size = BF_CMD_FIRST_DATA_BLOCK_SIZE;
        }

        if (!err) {
            err = bf_mem_wr(E502_BF_MEMADDR_CMD, (const uint32_t*)&f_cur_cmd,
                               (offsetof(t_l502_bf_cmd, data))/4 + size, 0);
        }
    }
    return err;
}

t_e502_cm4_errs bf_cmd_check(int32_t *res) {
    t_e502_cm4_errs err;

    err = bf_mem_rd(E502_BF_MEMADDR_CMD, (uint32_t*)&f_cur_cmd, E502_BF_REQ_DATA_SIZE_MIN);
    if (!err) {
        if (f_cur_cmd.status != L502_BF_CMD_STATUS_DONE) {
            err = E502_CM4_ERR_BF_CMD_IN_PROGRESS;
        } else if (res!=NULL) {
            *res = f_cur_cmd.result;
        }
    }

    return err;
}

t_e502_cm4_errs bf_cmd_rd_resp(uint32_t* rcv_data, uint32_t rcv_size, uint32_t* recvd_size) {
    t_e502_cm4_errs err;

    err = bf_mem_rd(E502_BF_MEMADDR_CMD, (uint32_t*)&f_cur_cmd,
                       (offsetof(t_l502_bf_cmd, data))/4 + rcv_size);
    memcpy(rcv_data, f_cur_cmd.data, rcv_size*sizeof(rcv_data[0]));
    if (recvd_size!=NULL)
        *recvd_size = f_cur_cmd.data_size;

    return err;
}




t_e502_cm4_errs bf_cmd_exec(uint16_t cmd_code, uint32_t par,
                            const uint32_t* snd_data, uint32_t snd_size,
                            uint32_t* rcv_data, uint32_t rcv_size, uint32_t tout,
                            uint32_t* recvd_size) {
    int32_t err = bf_cmd_start(cmd_code, par, snd_data, snd_size);
    int done = 0;
    t_ltimer tmr;

    ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(tout));
    while (!err && !done) {
        int32_t cmd_result = 0;
        err = bf_cmd_check(&cmd_result);
        if (!err) {
            done = 1;
            if (rcv_size != 0) {
                err = bf_cmd_rd_resp(rcv_data, rcv_size, recvd_size);
            }
            if (!err)
                err = cmd_result;
        } else if (err==E502_CM4_ERR_BF_CMD_IN_PROGRESS) {
            if (ltimer_expired(&tmr)) {
                err = E502_CM4_ERR_BF_CMD_TIMEOUT;
            } else {
                err = 0;
            }
        }
    }
    return err;
}



static t_e502_cm4_errs f_check_bf_firm(void) {
    int32_t err = fpga_reg_write(E502_REGS_BF_CMD, E502_BF_CMD_HDMA_RST);
    if (!err) {
        uint32_t rcv_wrds[2], recvd;

        /* Проверяем версию прошивки BlackFin */
        err = bf_cmd_exec(L502_BF_CMD_CODE_GET_PARAM, L502_BF_PARAM_FIRM_VERSION,
                   NULL, 0, rcv_wrds, 2, BF_CMD_DEFAULT_TOUT, &recvd);
        if (!err) {
            if (recvd >= 1) {
                //nd->bf_ver = rcv_wrds[0];
                lprintf("bf firm version = 0x%08X\n", rcv_wrds[0]);
            } else {
                err = E502_CM4_ERR_BF_CMD_RETURN_INSUF_DATA;
            }
            //hnd->bf_features = recvd >= 2 ? rcv_wrds[1] : 0;
        }
    }

    return err;
}





t_e502_cm4_errs bf_firm_load(uint8_t *ldr, uint32_t size) {
    /** @todo check blackfin present*/
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    t_e502_cm4_errs next_err = E502_CM4_ERR_OK;

    if (!err) {
        int stop = 0;
        t_ltimer tmr;

        uint32_t *ldr_buf=NULL;
        t_bf_ldr_pkt pkt, pkt_next;
        uint32_t bf_val = 0;

        err = fpga_reg_read(E502_REGS_BF_CTL, &bf_val);

        if (!err) {
            err = fpga_reg_write(E502_REGS_BF_CTL, E502_REGBIT_BF_CTL_DSP_MODE_Msk
                        | (bf_val & 0xF00)); //set rst
        }

        if (!err) {
            ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(500));
            while (!ltimer_expired(&tmr)) {
                lpc_wdt_reset();
            };

            err = fpga_reg_write(E502_REGS_BF_CTL, E502_REGBIT_BF_CTL_DSP_MODE_Msk |
                        E502_REGBIT_BF_CTL_BF_RESET_Msk | (bf_val & 0xF00));  //release rst
        }

        if (!err) {
            uint32_t reg;
            ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(BF_WAIT_LOAD_RDY_TOUT));
            do {
                fpga_reg_read(E502_REGS_BF_CTL, &reg);
                if ((reg&E502_REGBIT_BF_CTL_HOST_WAIT_Msk) && ltimer_expired(&tmr))
                    err = E502_CM4_ERR_BF_LOAD_RDY_TOUT;
                lpc_wdt_reset();
            } while (!err && (reg&E502_REGBIT_BF_CTL_HOST_WAIT_Msk));

            ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(200));
            while (!ltimer_expired(&tmr)) {};
            err = 0;
        }

        if (!err) {
            err = f_parse_ldr_hdr(ldr, &pkt);
            ldr+=BF_LDR_HDR_SIZE;
            size-=BF_LDR_HDR_SIZE;
        }

        while (!err && !stop) {

            lpc_wdt_reset();

            if (next_err) {
                err = next_err;
            } else if (((pkt.flags&BF_LDR_FLAG_FILL) == 0) && (pkt.size!=0)) {

                if (pkt.size > size) {
                    err = E502_CM4_ERR_BF_LDR_FILE_SIZE;
                } else {
                    ldr_buf = (uint32_t*)ldr;
                    size-=pkt.size;
                    ldr+=pkt.size;
                }
            }

            if (!err) {


                next_err = size >= BF_LDR_HDR_SIZE ?
                          f_parse_ldr_hdr(ldr, &pkt_next) : E502_CM4_ERR_BF_LDR_FILE_SIZE;
                ldr+=BF_LDR_HDR_SIZE;
                size-=BF_LDR_HDR_SIZE;
                if (next_err) {
                        pkt_next.size = 0;
                }

                if (pkt.size!=0) {
                    int fill = 0;                    
                    uint32_t wr_size = ((pkt.size+31)/(32))*8;
                    if (pkt.flags & BF_LDR_FLAG_FILL) {
                        fill = 1;
                        ldr_buf = &pkt.arg;
                    }

                    if ((pkt.flags & BF_LDR_FLAG_FINAL)
                        || ((pkt_next.flags & BF_LDR_FLAG_FINAL) && (pkt_next.size==0))) {

                        uint32_t buf_pos = 0;
                        err = BF_CHECK_ADDR_SIZE(pkt.addr, wr_size);

                        if (!err && (wr_size > 8)) {
                            err = bf_mem_wr(pkt.addr, ldr_buf, wr_size-8, fill);
                            pkt.addr+=4*(wr_size-8);                            
                            if (!fill)
                                buf_pos = wr_size-8;
                            wr_size = 8;
                        }

                        if (!err)
                             err = fpga_reg_write(E502_REGS_BF_CMD, E502_BF_CMD_HIRQ);
                        if (!err)
                            err = bf_mem_wr(pkt.addr, &ldr_buf[buf_pos], wr_size, fill);
                        stop=1;

                        if (!err) {
                            err = fpga_reg_write(E502_REGS_BF_CTL, E502_REGBIT_BF_CTL_DSP_MODE_Msk |
                                        E502_REGBIT_BF_CTL_BF_RESET_Msk);
                        }
                    } else if (!(pkt.flags & BF_LDR_FLAG_IGNORE)) {
                        err = BF_CHECK_ADDR_SIZE(pkt.addr, wr_size);
                        if (!err)
                            err = bf_mem_wr(pkt.addr, ldr_buf, wr_size, fill);
                    }
                }
                pkt = pkt_next;
            }
        }
    }



    if (!err) {
        err = f_check_bf_firm();
    }


#if 0
    if (!err) {
        static uint32_t buf[1024];
        for (size_t i=0; i < sizeof(buf)/sizeof(buf[0]); i++)
            buf[i] = i;
        err = bf_mem_wr(0, buf, sizeof(buf)/sizeof(buf[0]), 0);
        if (!err) {
            memset(buf,0, sizeof(buf));
            err = bf_mem_rd(0, buf, sizeof(buf)/sizeof(buf[0]));
        }

        if (!err) {
            for (size_t i=0; i < sizeof(buf)/sizeof(buf[0]); i++) {
                lprintf("bf buf[%3d] = 0x%08X\n", i, buf[i]);
            }
        }
    }
#endif
#if 0
    if (!err) {
        static t_l502_bf_test_res res;
        err = bf_cmd_exec(L502_BF_CMD_CODE_TEST, L502_BF_CMD_TEST_SPORT,
                          NULL, 0, NULL, 0, BF_CMD_DEFAULT_TOUT, NULL);

        do {
            uint32_t rcv_data;
            err = bf_cmd_exec(L502_BF_CMD_CODE_TEST, L502_BF_CMD_TEST_GET_RESULT,
                              NULL, 0, (uint32_t*)&res, sizeof(res)/sizeof(uint32_t),
                              4000, &rcv_data);
            if (!err && (rcv_data==sizeof(res)/sizeof(uint32_t))) {
                lprintf("sport res: err = %d, cntr = %d\n", res.err, res.cntr);
            }

        } while (1);
    }
#endif

    return err;
}

void bf_init(void) {
    uint32_t bf_val;
    t_e502_cm4_errs err = fpga_reg_read(E502_REGS_BF_CTL, &bf_val);
    if (!err) {
        if (!(bf_val & E502_REGBIT_BF_CTL_BF_RESET_Msk)) {
            err = fpga_reg_write(E502_REGS_BF_CTL, bf_val | E502_REGBIT_BF_CTL_BF_RESET_Msk);
        }
    }
}






#include "chip.h"
#include "streams_priv.h"
#include "e502_cm4.h"
/*******************************************************************************
 * Реализация потоков передачи данных между ПЛИС и буфером интерфейса.
 * Выполняется на отдельном ядре Cortex-M0, чтобы работа интерфейсов не влияла
 * на времена выполнения процедуры обмена.
 * Все передачи выполняются с использованием одного канала DMA строго последовательно.
 *
 * in :  fpga->buf_tmp->sdram->intf_buf
 * out:  intf_buf->sdram->buf_tmp->fpga
 *
 * Общий временный буфер (f_buf_tmp) используется, чтобы не передавать данные
 * напрямую из sdram в ПЛИС, так как при разделении циклов SDRAM и асинхронной SRAM
 * передача идет быстрее. При этом после записи во временный буфер сразу идет чтение
 * из него из прерывания об окончании записи (остальные передачи могут чередоваться).
 *
 * Интерфейсный буфер (intf_buf) для каждого направления свой и используется также,
 * чтобы обмен с интерфейсом не затрагивал SDRAM.
 *
 * Решение передачи в ПЛИС выполнено с использованием 2-х механизмов:
 *   - прерывание при наполовину заполненности (in) или опустошенности (out)
 *      внутреннего буфера ЦАП. На эти события надо реагировать как можно быстрее,
 *      чтобы не произошло переполнение или опустошение буфера. При этом ввод имеет
 *      приоритет над выводом. Эти прерывания запрещены только на время
 *      работы DMA или решения по очередной передаче DMA. Также прерывания запрещены,
 *      если в буфере sdram нет места (in) или нет данных (out).
 *   - передача данных по счетчику принятых (in) или отданных (out) слов. Выполняется
 *      при отсутствии обмена с FPGA за определенный период. Используется либо
 *      для того, чтобы прочитать "хвост" данных, либо на низкой скорости
 *      обмена. Выполняется в фоне.
 *
 * Передача между sdram и интерфейсным буфером выполняется в фоне, когда не работает
 * DMA. При этом приоритет между потоками (in/out) выполняется по кольцу.
 *
 * Cortex-M0 выполняет только передачу до/от интерфейсного буфера, передача
 * данных из интерфейсного буфера по интерфейсу в ПК и наоборот выполняется
 * уже основным ядром Cortex-M4, ответственным за все интерфейсы. Этот обмен
 * не привязан к активности канала DMA, так как там используются свои
 * каналы DMA для этих интерфейсов.
 *
 * Так как прием данных с USB может закончится раньше, чем будет принято запрошенное
 * кол-во данных (по короткому пакету), то принимаемые данные могут быть не
 * непрерывны в памяти. Поэтому по каждому завершенному запросу сохраняется
 * информация в виде стркутуры t_out_parts, которая и используется при передаче
 * из интерфейсного буфера в SDRAM
 ******************************************************************************/
#include "streams.h"
#include "chip.h"
#include "fpga_sram_buf.h"
#include "e502_fpga_regs.h"
#include "fpga.h"
#include "lprintf.h"

#include <stdlib.h>
#include <inttypes.h>
#include "ltimer.h"

#include "dbg_config.h"
#include "m0_defs.h"


void m0_sdram_test(void);


#define DMA_TRANSF_MAX_SIZE 2048



#define IN_TRANSF_TIME_TRESHOLD    LTIMER_MS_TO_CLOCK_TICKS(2)
#define OUT_TRANSF_TIME_TRESHOLD   LTIMER_MS_TO_CLOCK_TICKS(2)


#define FPGA_OUT_BUF_SIZE 2048

#define FPGA_IN_IRQ_SIZE  1024
#define FPGA_OUT_IRQ_SIZE (FPGA_OUT_BUF_SIZE/2)


#define SDRAM_BURST_SIZE_CODE    7
#define FPGA_RD_BURST_SIZE_CODE  1
#define FPGA_WR_BURST_SIZE_CODE  0



#define STREAMS_DMA_CHANNEL      7


#define FPGA_TEST_PIN   LPC_PIN_PB_1_GPIO5_21

#define PIN_IN_BUF_HALF_FULL   LPC_PIN_P7_0_GPIO3_8
#define PIN_OUT_BUF_HALF_FULL  LPC_PIN_P3_4_GPIO1_14

#define PIN_OUT_FLUSH          LPC_PIN_PD_0_GPIO6_14


#define STREAM_IN_IFACE_BUF_SIZE        8192
#define STREAM_OUT_IFACE_BUF_SIZE       8192

#define STREAM_FPGA_BUF_SIZE            2048
#define STREAM_IN_FPGA_TRANS_SIZE       1024
#define STREAM_OUT_FPGA_TRANS_SIZE      1024

#define GINT1_CTL_WRD (GPIOGR_INT | GPIOGR_TRIG)


_M0_DATA_Z_ volatile int streams_in_run;
_M0_DATA_Z_ volatile t_stream_req streams_in_req;
_M0_DATA_Z_ volatile int streams_out_run;
_M0_DATA_Z_ volatile t_stream_req streams_out_req;
_M0_DATA_Z_ int stream_out_part_mode;


_M0_DATA_Z_ static int f_sdram_prev_out = 0;
_M0_DATA_Z_ volatile t_out_parts out_parts[STREAM_OUT_PARTS_MAX_CNT];
_M0_DATA_Z_ volatile uint32_t out_part_wr_start;
_M0_DATA_Z_ volatile uint32_t out_part_wr_done;
_M0_DATA_Z_ static uint32_t f_out_part_rd_start;
_M0_DATA_Z_ volatile uint32_t out_part_rd_done;


_M0_DATA_Z_ int stream_out_cycle_mode;
_M0_DATA_Z_ volatile int stream_out_cycle_load_req;
_M0_DATA_Z_ volatile int stream_out_cycle_sw_req;
_M0_DATA_Z_ uint32_t stream_out_cycle_load_size;
_M0_DATA_Z_ uint32_t stream_out_cycle_gen_size;
_M0_DATA_Z_ uint32_t stream_out_cycle_sw_flags;
_M0_DATA_Z_ uint32_t stream_out_cycle_multipler;
_M0_DATA_Z_ static int stream_out_flush;

/* счетчики считанных/записанных слов в FPGA */
_M0_DATA_Z_ static volatile uint16_t f_fpga_rd_wrds;
_M0_DATA_Z_ static volatile uint16_t f_fpga_wr_wrds;

uint32_t in_buf[STREAM_IN_BUF_SIZE] __attribute__((section(".sdram")));
uint32_t out_buf[STREAM_OUT_BUF_SIZE] __attribute__((section(".sdram")));

_M0_DATA_Z_ volatile int m0_wdt_sign;

_M0_DATA_Z_ static t_cycle_buf f_buf_tmp;

_M0_DATA_Z_ t_cycle_buf buf_in_iface;
_M0_DATA_Z_ t_cycle_buf buf_out_iface;
_M0_DATA_Z_ t_cycle_buf buf_in_sdram;
_M0_DATA_Z_ t_cycle_buf buf_out_sdram;
_M0_DATA_Z_ t_cycle_buf buf_out_sdram_cycle[STREAM_OUT_CYCLE_BUF_CNT];

_M0_DATA_Z_ static t_cycle_buf *f_cur_out_gen_buf;
_M0_DATA_Z_ static t_cycle_buf *f_cur_out_load_buf;

_M0_DATA_Z_ static t_lclock_ticks f_dma_in_last_time, f_dma_out_last_time;
_M0_DATA_Z_ static unsigned f_dma_size;



#define CYCLE_NEXT_BUF(buf) ((buf) == &buf_out_sdram_cycle[STREAM_OUT_CYCLE_BUF_CNT-1] ? \
                                      &buf_out_sdram_cycle[0] : buf+1)


static volatile enum {
    DMA_STAGE_IN_FPGA_RD,
    DMA_STAGE_IN_BUF_CPY,
    DMA_STAGE_IN_IFACE_CPY,
    DMA_STAGE_OUT_FPGA_WR,
    DMA_STAGE_OUT_BUF_CPY,
    DMA_STAGE_OUT_IFACE_CPY,
    DMA_STAGE_IDLE
} f_dma_stage;

typedef enum {
    CYCLE_BUF_DIR_WR,
    CYCLE_BUF_DIR_RD
} t_cycle_buf_dir;


static uint32_t f_fpga_tmp_buf[STREAM_FPGA_BUF_SIZE] __attribute__ ((section (".fpga_buf"))) ;
static uint32_t f_in_iface_buf[STREAM_IN_IFACE_BUF_SIZE]__attribute__ ((section (".iface_buf"))) ;
static uint32_t f_out_iface_buf[STREAM_OUT_IFACE_BUF_SIZE]__attribute__ ((section (".iface_out_buf"))) ;


#define FPGA_IN_BURST_PIN      LPC_PIN_P7_2_GPIO3_10


static void m0_delay_clk(unsigned int clk_count) {
#define CYCLES_OVERHEAD     7
    /* Задержка примерно на clk_count тактов, min = 11 тактов */
    asm volatile (
         " .syntax unified\n"
         "  subs %[reg], %[reg], %[adj]\n"
         "  asrs %[reg], %[reg], #2\n"  /* 4 cycles per loop */
         "0:"
         "  subs %[reg], %[reg], #0\n"
         "  subs %[reg], %[reg], #1\n"
         "  bgt 0b\n"
         " .syntax divided"
         : [reg] "+r" (clk_count)
         : [adj] "i" (CYCLES_OVERHEAD - 3)
         : "cc"
        );
#undef CYCLES_OVERHEAD
    }

static inline _M0_CODE_ void f_in_dma_burst_disable(void) {
    m0_delay_clk(30);
    LPC_PIN_OUT(FPGA_IN_BURST_PIN, 0);
    m0_delay_clk(80);
}

static inline _M0_CODE_ void f_in_dma_burst_enable(void) {
    LPC_PIN_OUT(FPGA_IN_BURST_PIN, 1);
    m0_delay_clk(150);
}


_M0_CODE_ static void out_cycle_extend_buf(t_cycle_buf *buf) {
    /* для малого кол-ва точек увеличиваем буфер путем создания нескольких одинаковых
       периодов сигнала. в противном случае слишком частый перезапуск DMA приведет
       к тому, что не будем успевать подкачивать данные */
    if (stream_out_cycle_multipler > 1) {
        uint32_t part_size = buf->buf_size;
        buf->buf_size *= stream_out_cycle_multipler;
        unsigned pos = part_size;
        for (unsigned part = 1; part < stream_out_cycle_multipler; part++) {
            for (unsigned i = 0; i < part_size; i++) {
                buf->buf[pos++] = buf->buf[i];
            }
        }
        buf->wr_done = buf->wr_start = buf->buf_size;
    }
}

_M0_CODE_ static void out_cycle_start_gen_buf(t_cycle_buf *buf) {
    if (f_cur_out_gen_buf) {
        f_cur_out_gen_buf->buf_size = 0;
        cycle_buf_init(f_cur_out_gen_buf);
    }

    f_cur_out_load_buf = CYCLE_NEXT_BUF(f_cur_out_load_buf);

    f_cur_out_gen_buf = buf;
    out_cycle_extend_buf(f_cur_out_gen_buf);

    stream_out_cycle_gen_size = buf->buf_size;

    stream_out_cycle_sw_req = 0;
}



_M0_CODE_ static inline int f_dma_stage_is_in(void) {
    return (f_dma_stage < DMA_STAGE_OUT_FPGA_WR);
}

_M0_CODE_ static inline  int f_dma_stage_is_out(void) {
    return (f_dma_stage>=DMA_STAGE_OUT_FPGA_WR)  && (f_dma_stage < DMA_STAGE_IDLE);
}


#if DBG_STREAM_IN_TST_CNTR
static uint32_t f_tst_cntr = 0;
#endif
#if DBG_STREAM_OUT_TST_CNTR
static uint32_t f_out_tst_cntr = 0;
#endif

#if PRINT_DBG_STREAM_STAT
typedef struct {
    uint32_t transf_cnt;
    uint32_t time_max;
    uint32_t time_min;
} t_transf_stat;


static t_transf_stat f_stat_transf[DMA_STAGE_IDLE];
static uint64_t f_idle_time;
static uint32_t f_stat_cntr;






static inline void tmr_init(void) {
    LPC_TIMER0->TCR = TIMER_RESET;
    LPC_TIMER0->MCR = TIMER_STOP_ON_MATCH(0);
    LPC_TIMER0->MR[0] = 0xFFFFFF0;
}

static inline void tmr_start(void) {
    LPC_TIMER0->TCR = TIMER_RESET;
    LPC_TIMER0->TCR = TIMER_ENABLE;
}

static inline void tmr_stop(void) {
    LPC_TIMER0->TCR = TIMER_RESET;
}

static inline uint32_t tmr_cnt(void) {
    return LPC_TIMER0->TC;
}


static inline void f_stat_init(void) {
    tmr_init();
    f_idle_time = 0;
    for (unsigned i=0; i < sizeof(f_stat_transf)/sizeof(f_stat_transf[0]); i++) {
        f_stat_transf[i].transf_cnt=0;
        f_stat_transf[i].time_max = 0;
        f_stat_transf[i].time_min = 0xFFFFFFFF;
    }
}

static inline void f_stat_transf_start(void) {
    f_idle_time += tmr_cnt();
    tmr_start();
}

static inline void f_stat_transf_done(unsigned stage, unsigned size) {
    if (stage < sizeof(f_stat_transf)/sizeof(f_stat_transf[0])) {
        uint32_t time = tmr_cnt();
        //tmr_stop();
        tmr_start();

        time = time*1024/size;

        f_stat_transf[stage].transf_cnt++;
        if (f_stat_transf[stage].time_min > time)
            f_stat_transf[stage].time_min = time;
        if (f_stat_transf[stage].time_max < time)
            f_stat_transf[stage].time_max = time;
    }
}
#else
    #define f_stat_init()
    #define f_stat_transf_start()
    #define f_stat_transf_done(stage, size)
#endif



_M0_CODE_ static inline void m0_wdt_clear(void) {
    m0_wdt_sign = 1;
}

_M0_CODE_ static inline void stream_in_pin_irq_en(void) {
    NVIC_EnableIRQ(PIN_INT4_IRQn);
}

_M0_CODE_ static inline void stream_in_pin_irq_dis(void) {
    NVIC_DisableIRQ(PIN_INT4_IRQn);
}

_M0_CODE_ static inline void stream_out_pin_irq_en(void) {
    NVIC_EnableIRQ(GINT1_IRQn);
}

_M0_CODE_ static inline void stream_out_pin_irq_dis(void) {
    NVIC_DisableIRQ(GINT1_IRQn);
}

_M0_CODE_ static void f_cpy_bufs(t_cycle_buf *src, t_cycle_buf *dst, uint32_t size) {
    static DMA_TransferDescriptor_t lli[3];
    GPDMA_CH_T *dma_ch = &LPC_GPDMA->CH[STREAMS_DMA_CHANNEL];
    int cur_lli = 0;
    int last = 0;
    uint32_t src_pos  = src->cycle ? src->rd_start : 0;
    uint32_t dst_pos  = dst->cycle ? dst->wr_start : 0;

    f_dma_size = size;

    f_stat_transf_start();

    do {
        uint32_t ctl_wrd;
        uint32_t cur_size = size;
        if (src->cycle && ((src_pos + cur_size) > src->buf_size)) {
            cur_size = src->buf_size - src_pos;
        }

        if (dst->cycle && ((dst_pos + cur_size) > dst->buf_size)) {
            cur_size = dst->buf_size - dst_pos;
        }

        if (cur_size > DMA_TRANSF_MAX_SIZE)
            cur_size = DMA_TRANSF_MAX_SIZE;

        last = (cur_size == size) || (cur_lli==(sizeof(lli)/sizeof(lli[0]) -1));

        ctl_wrd = GPDMA_DMACCxControl_TransferSize(cur_size) |
                GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_WORD) |
                GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_WORD) |
                GPDMA_DMACCxControl_SBSize(SDRAM_BURST_SIZE_CODE) | GPDMA_DMACCxControl_DBSize(SDRAM_BURST_SIZE_CODE) |
                GPDMA_DMACCxControl_DI | GPDMA_DMACCxControl_SI | GPDMA_DMACCxControl_DestTransUseAHBMaster1 |
                (last ? GPDMA_DMACCxControl_I : 0);

        if (cur_lli==0) {
            dma_ch->SRCADDR   = (uint32_t)&src->buf[src_pos];
            dma_ch->DESTADDR  = (uint32_t)&dst->buf[dst_pos];
            dma_ch->CONTROL = ctl_wrd;

            dma_ch->LLI = last ? 0 : (uint32_t)&lli[0];
        } else {
            lli[cur_lli-1].src   = (uint32_t)&src->buf[src_pos];
            lli[cur_lli-1].dst   = (uint32_t)&dst->buf[dst_pos];
            lli[cur_lli-1].ctrl  = ctl_wrd;
            lli[cur_lli-1].lli   = last ? 0 : (uint32_t)&lli[cur_lli];
        }

        cur_lli++;

        src_pos += cur_size;
        dst_pos += cur_size;
        size -= cur_size;

        if (src_pos==src->buf_size)
            src_pos=0;
        if ((dst_pos==dst->buf_size) && !dst->wr_single)
            dst_pos=0;
    } while (!last);


    if (src->cycle)
        src->rd_start = src_pos;
    if (dst->cycle) {
        dst->wr_start = dst_pos;
        cycle_buf_update_full(dst);
    }

    dma_ch->CONFIG = GPDMA_DMACCxConfig_TransferType(GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA) |
                    GPDMA_DMACCxConfig_ITC;
    dma_ch->CONFIG |= GPDMA_DMACCxConfig_E;
}


_M0_CODE_ static void f_in_fpga_to_buf(const uint32_t *buf, uint16_t size) {
    f_dma_size = size;

    if (f_dma_stage == DMA_STAGE_IDLE) {
        t_lclock_ticks dma_time = lclock_get_ticks();

        f_dma_stage = DMA_STAGE_IN_FPGA_RD;

        f_stat_transf_start();
        f_in_dma_burst_enable();
        f_fpga_rd_wrds += size;
        f_dma_in_last_time = dma_time;


        GPDMA_CH_T *in_ch = &LPC_GPDMA->CH[STREAMS_DMA_CHANNEL];
        in_ch->SRCADDR = (uint32_t)FPGA_SRAM_DMA_ADC_BUF;
        in_ch->DESTADDR = (uint32_t)&buf[0];


        in_ch->CONTROL = GPDMA_DMACCxControl_TransferSize(size*2) |
                GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_HALFWORD) |
                GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_HALFWORD) |
                GPDMA_DMACCxControl_SBSize(FPGA_RD_BURST_SIZE_CODE) | GPDMA_DMACCxControl_DBSize(SDRAM_BURST_SIZE_CODE) |
                GPDMA_DMACCxControl_DI | GPDMA_DMACCxControl_SI |
                GPDMA_DMACCxControl_I | GPDMA_DMACCxControl_DestTransUseAHBMaster1;

        in_ch->LLI = 0;

        in_ch->CONFIG = GPDMA_DMACCxConfig_TransferType(GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA) |
                        GPDMA_DMACCxConfig_ITC;

        in_ch->CONFIG |= GPDMA_DMACCxConfig_E;
    } else {
        lprintf("-------- err (dma = %d)!!!\n", f_dma_stage);
    }
}


_M0_CODE_ void f_out_buf_to_fpga(const uint32_t *buf, uint16_t size) {
    f_dma_size = size;

    f_dma_stage = DMA_STAGE_OUT_FPGA_WR;

    f_stat_transf_start();
    f_fpga_wr_wrds += size;


    GPDMA_CH_T *in_ch = &LPC_GPDMA->CH[STREAMS_DMA_CHANNEL];
    in_ch->SRCADDR = (uint32_t)&buf[0];
    in_ch->DESTADDR = (uint32_t)FPGA_SRAM_DMA_DAC_BUF;

    in_ch->CONTROL = GPDMA_DMACCxControl_TransferSize(size*2) |
            GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_HALFWORD) |
            GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_HALFWORD) |
            GPDMA_DMACCxControl_SBSize(SDRAM_BURST_SIZE_CODE) | GPDMA_DMACCxControl_DBSize(FPGA_WR_BURST_SIZE_CODE) |
            GPDMA_DMACCxControl_DI | GPDMA_DMACCxControl_SI |
            GPDMA_DMACCxControl_I | GPDMA_DMACCxControl_DestTransUseAHBMaster1;

    in_ch->LLI = 0;

    in_ch->CONFIG = GPDMA_DMACCxConfig_TransferType(GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA) |
                    GPDMA_DMACCxConfig_ITC;
    in_ch->CONFIG |= GPDMA_DMACCxConfig_E;
}


_M0_CODE_ static void f_in_buf_to_sdram(uint16_t size) {
    f_dma_stage = DMA_STAGE_IN_BUF_CPY;
    f_cpy_bufs(&f_buf_tmp, &buf_in_sdram, size);
}

_M0_CODE_ static uint32_t f_out_sdram_rd_rdy(void) {

    if (!stream_out_cycle_mode)
        return cycle_buf_rd_rdy(f_cur_out_gen_buf);

    /* в циклическом режиме всегда готовы передавать буфер целиком, если он есть,
     * кроме случая смены страницы */
    if (f_cur_out_gen_buf == NULL)
        return 0;

     /* при смене буферов на границе буфера нужно смотреть до конца массива */
    if (stream_out_cycle_sw_req && !(stream_out_cycle_sw_flags & X502_OUT_CYCLE_FLAGS_FORCE))
        return f_cur_out_gen_buf->buf_size - f_cur_out_gen_buf->rd_start;

    return f_cur_out_gen_buf->buf_size;
}


_M0_CODE_ static void f_out_sdram_to_buf(uint16_t size) {
    if (f_dma_stage == DMA_STAGE_IDLE) {
        uint32_t rdy_size = f_out_sdram_rd_rdy();
        if (size > rdy_size)
            size = rdy_size;
        if (size) {
            f_dma_stage = DMA_STAGE_OUT_BUF_CPY;
            f_cpy_bufs(f_cur_out_gen_buf, &f_buf_tmp, size);
        }
    }
}


_M0_CODE_ static int f_in_try_sdram_to_iface(void) {
    uint32_t rd_rdy = cycle_buf_rd_rdy(&buf_in_sdram);
    uint32_t wr_rdy = cycle_buf_wr_rdy(&buf_in_iface);

    if (rd_rdy > wr_rdy) {
        rd_rdy = wr_rdy;
    }

    if (rd_rdy) {
        if (rd_rdy > STREAM_IN_FPGA_TRANS_SIZE)
            rd_rdy = STREAM_IN_FPGA_TRANS_SIZE;
        f_dma_stage = DMA_STAGE_IN_IFACE_CPY;
        f_sdram_prev_out = 0;
        f_cpy_bufs(&buf_in_sdram, &buf_in_iface, rd_rdy);
    }
    return rd_rdy;
}

_M0_CODE_ static int f_out_try_sdram_to_iface(void) {
    uint32_t wr_rdy = 0;


    if (f_cur_out_load_buf != NULL) {
        if (stream_out_part_mode) {
            if (f_out_part_rd_start!=out_part_wr_done) {
                wr_rdy = cycle_buf_wr_rdy(f_cur_out_load_buf);

                if (wr_rdy) {
                    if (out_parts[f_out_part_rd_start].size!=0) {


                        if (wr_rdy > out_parts[f_out_part_rd_start].size)  {
                            wr_rdy = out_parts[f_out_part_rd_start].size;
                        }

                        if (wr_rdy > STREAM_OUT_FPGA_TRANS_SIZE)
                            wr_rdy = STREAM_OUT_FPGA_TRANS_SIZE;

                        f_dma_stage = DMA_STAGE_OUT_IFACE_CPY;
                        f_sdram_prev_out = 1;


                        buf_out_iface.rd_start = out_parts[f_out_part_rd_start].addr - buf_out_iface.buf;

                        out_parts[f_out_part_rd_start].size -= wr_rdy;
                        out_parts[f_out_part_rd_start].addr += wr_rdy;
                    } else {
                        wr_rdy = 0;
                    }


                    if (out_parts[f_out_part_rd_start].size==0) {
                        if (++f_out_part_rd_start==STREAM_OUT_PARTS_MAX_CNT) {
                            f_out_part_rd_start = 0;
                        }

                    }

                    if (wr_rdy) {
                        f_cpy_bufs(&buf_out_iface, f_cur_out_load_buf, wr_rdy);
                    } else {
                        out_part_rd_done = f_out_part_rd_start;
                    }
                }
            }
        } else {
            uint32_t rd_rdy = cycle_buf_rd_rdy(&buf_out_iface);
            if (rd_rdy > 0) {                
                wr_rdy = cycle_buf_wr_rdy(f_cur_out_load_buf);
                if (wr_rdy) {
                    if (wr_rdy > rd_rdy)
                        wr_rdy = rd_rdy;
                    if (wr_rdy > STREAM_OUT_FPGA_TRANS_SIZE)
                        wr_rdy = STREAM_OUT_FPGA_TRANS_SIZE;



                    f_dma_stage = DMA_STAGE_OUT_IFACE_CPY;
                    f_sdram_prev_out = 1;
                    f_cpy_bufs(&buf_out_iface, f_cur_out_load_buf, wr_rdy);
                }
            }
        }
    }
    return wr_rdy;
}



_M0_CODE_ static void f_dma_fpga_en(void) {
    if (f_dma_stage == DMA_STAGE_IDLE) {
        if (streams_in_run && !buf_in_sdram.full) {
            stream_in_pin_irq_en();
        }

        if (streams_out_run && (f_out_sdram_rd_rdy()!=0)) {
            stream_out_pin_irq_en();
        }
    }
}


_M0_CODE_ static int f_dma_start_in(void) {
    int started = 0;
    if (!buf_in_sdram.full) {
        f_in_fpga_to_buf(f_fpga_tmp_buf, FPGA_IN_IRQ_SIZE-1);
        started = 1;
    }
    return started;
}


_M0_CODE_ static void f_dma_idle_work(void) {
    t_lclock_ticks time = lclock_get_ticks();
    int started = 0;
    int in_priority;


    /* по таймауту отсутствия чтения из буфера ПЛИСа проверяем, не остались
     * ли там данные */
    if (streams_in_run && !buf_in_sdram.full && ((time - f_dma_in_last_time) > IN_TRANSF_TIME_TRESHOLD)) {
        uint32_t cur_in_cntr = *FPGA_SRAM_DMA_ADC_CNTR;
        f_dma_in_last_time = time;
        int16_t rcv_size = ((cur_in_cntr - f_fpga_rd_wrds) & FPGA_SRAM_BUF_CNTR_MSK);
        if (rcv_size > 0) {            
            f_in_fpga_to_buf(f_fpga_tmp_buf, rcv_size);
            started = 1;
        }
    }


    if (!started && streams_out_run && ((time - f_dma_out_last_time) > OUT_TRANSF_TIME_TRESHOLD)) {
        uint32_t rdy_size = f_out_sdram_rd_rdy();

        if (rdy_size > 0) {
            uint32_t cur_out_cntr;
            uint16_t snd_size;
            cur_out_cntr = *FPGA_SRAM_DMA_DAC_CNTR;
            f_dma_out_last_time = time;

            snd_size  = FPGA_OUT_BUF_SIZE - 1 -
                    ((f_fpga_wr_wrds - cur_out_cntr) & FPGA_SRAM_BUF_CNTR_MSK);

            if (snd_size > rdy_size)
                snd_size = rdy_size;
            if (snd_size > FPGA_SRAM_BUF_SIZE)
                snd_size = FPGA_SRAM_BUF_SIZE;
            if (snd_size > 0) {
                f_out_sdram_to_buf(snd_size);
                started = 1;
            }
        }
    }


    /* в зависимости от того, какой была предыдущая операция (ввод или вывод),
     * приоритет получает другая */
    in_priority = f_sdram_prev_out;
    if (!started && ((in_priority ? f_in_try_sdram_to_iface() : f_out_try_sdram_to_iface())!=0))
        started = 1;
    if (!started && ((in_priority ? f_out_try_sdram_to_iface() : f_in_try_sdram_to_iface())!=0))
        started = 1;

    if (!started)
        f_dma_fpga_en();
}


_M0_CODE_ void GPIO4_IRQHandler_m0(void) {
    if (LPC_GPIO_PIN_INT->IST & PININTCH(4)) {
        //LPC_PIN_OUT(LED_PIN_DEBUG, 1);
        stream_in_pin_irq_dis();
        stream_out_pin_irq_dis();
        if (f_dma_stage == DMA_STAGE_IDLE)
            f_dma_start_in();
    }
}

_M0_CODE_ void GINT1_IRQHandler_m0(void) {
    if (LPC_GPIOGROUP[1].CTRL & GPIOGR_INT) {

        LPC_GPIOGROUP[1].CTRL = GINT1_CTL_WRD;
        stream_in_pin_irq_dis();
        stream_out_pin_irq_dis();
        if (f_dma_stage == DMA_STAGE_IDLE) {
            if (!LPC_PIN_IN(PIN_IN_BUF_HALF_FULL) || !f_dma_start_in()) {
                f_out_sdram_to_buf(FPGA_OUT_IRQ_SIZE-1);
            }
        }
    }
}

_M0_CODE_ void DMA_IRQHandler_m0(void) {
    stream_in_pin_irq_dis();
    stream_out_pin_irq_dis();
    uint32_t intstat = LPC_GPDMA->INTTCSTAT;
    LPC_GPDMA->INTTCCLEAR = intstat;



    if (intstat & (1<<STREAMS_DMA_CHANNEL)) {
        f_stat_transf_done(f_dma_stage, f_dma_size);


        if (f_dma_stage == DMA_STAGE_IN_FPGA_RD) {
            f_in_dma_burst_disable();

#if DBG_STREAM_IN_TST_CNTR
            for (unsigned i=0; i < f_dma_size; i++) {
                f_fpga_tmp_buf[i] = f_tst_cntr++;
            }
#endif

            f_in_buf_to_sdram(f_dma_size);

        } else if (f_dma_stage == DMA_STAGE_IN_BUF_CPY) {
            buf_in_sdram.wr_done = buf_in_sdram.wr_start;

            cycle_buf_update_full(&buf_in_sdram);
            if (f_in_try_sdram_to_iface()==0) {
                f_dma_stage = DMA_STAGE_IDLE;
            }
        } else if (f_dma_stage == DMA_STAGE_IN_IFACE_CPY) {
            buf_in_iface.wr_done = buf_in_iface.wr_start;
            buf_in_sdram.rd_done = buf_in_sdram.rd_start;
            if (buf_in_sdram.full) {
                cycle_buf_update_full(&buf_in_sdram);
            }

            f_dma_stage = DMA_STAGE_IDLE;
        } else if (f_dma_stage == DMA_STAGE_OUT_IFACE_CPY) {
            out_part_rd_done = f_out_part_rd_start;
            buf_out_iface.rd_done = buf_out_iface.rd_start;
            f_cur_out_load_buf->wr_done = f_cur_out_load_buf->wr_start;


            if (buf_out_iface.full) {
                cycle_buf_update_full(&buf_out_iface);
            }

            f_dma_stage = DMA_STAGE_IDLE;
        } else if (f_dma_stage == DMA_STAGE_OUT_BUF_CPY) {
#if DBG_STREAM_OUT_TST_CNTR
            //lprintf("out rdy %d: 0x%08X\n", f_dma_size, f_fpga_tmp_buf[0]);
            for (unsigned i=0; i < f_dma_size; i++) {
                if (f_out_tst_cntr != f_fpga_tmp_buf[i]) {
                    lprintf("out invalid cntr: expected 0x%08X, recvd 0x%08X\n",
                            f_out_tst_cntr, f_fpga_tmp_buf[i]);
                    f_out_tst_cntr = f_fpga_tmp_buf[i];
                }
                f_out_tst_cntr++;
            }
#endif
            f_cur_out_gen_buf->rd_done = f_cur_out_gen_buf->rd_start;

            if (f_cur_out_gen_buf->full) {
                cycle_buf_update_full(f_cur_out_gen_buf);
            }
            f_out_buf_to_fpga(f_fpga_tmp_buf, f_dma_size);

        } else if (f_dma_stage == DMA_STAGE_OUT_FPGA_WR) {
            f_dma_stage = DMA_STAGE_IDLE;
            if (stream_out_cycle_mode) {
                if (stream_out_cycle_sw_req &&
                        !(stream_out_cycle_sw_flags & X502_OUT_CYCLE_FLAGS_FORCE) &&
                        (f_cur_out_gen_buf->rd_done == 0)) {
                    t_cycle_buf *next_buf = CYCLE_NEXT_BUF(f_cur_out_gen_buf);
                    if (cycle_buf_is_full(next_buf)) {
                        out_cycle_start_gen_buf(next_buf);
                    }
                }
            }
        }

        if (f_dma_stage == DMA_STAGE_IDLE) {
            if (!LPC_PIN_IN(PIN_IN_BUF_HALF_FULL) || !f_dma_start_in()) {
                f_dma_fpga_en();
            }
        }
    }
}


_M0_CODE_ static void f_streams_init(void) {
    /* Разрешение DMA */
    LPC_GPDMA->CONFIG = GPDMA_DMACConfig_E;

    /* настройка PINT4 - прерывание при превышении половины буфера на ввод */
    LPC_SCU->PINTSEL1 &= ~0xFF;
    LPC_SCU->PINTSEL1 |= (LPC_PIN_ID_n(PIN_IN_BUF_HALF_FULL) << 5) |
            LPC_PIN_ID_m(PIN_IN_BUF_HALF_FULL);
    LPC_PIN_CONFIG(PIN_IN_BUF_HALF_FULL,  1, LPC_PIN_FAST | LPC_PIN_NOPULL);

    LPC_GPIO_PIN_INT->ISEL  |= PININTCH(4); //level sensetive
    LPC_GPIO_PIN_INT->SIENR  = PININTCH(4);
    LPC_GPIO_PIN_INT->SIENF  = PININTCH(4); //active - high!


    /* настройка GROUP INT1 - прерывание, при опустошении половины буфера на вывод */
    LPC_GPIOGROUP[1].PORT_ENA[LPC_PIN_ID_n(PIN_OUT_BUF_HALF_FULL)] =
            (1 << LPC_PIN_ID_m(PIN_OUT_BUF_HALF_FULL)); //enable pin
    LPC_GPIOGROUP[1].PORT_POL[LPC_PIN_ID_n(PIN_OUT_BUF_HALF_FULL)] &=
            ~(1 << LPC_PIN_ID_m(PIN_OUT_BUF_HALF_FULL)); //low polarity
    LPC_GPIOGROUP[1].CTRL = GINT1_CTL_WRD;


    LPC_PIN_CONFIG(PIN_OUT_BUF_HALF_FULL, 1, LPC_PIN_FAST | LPC_PIN_NOPULL);

    LPC_PIN_CONFIG(FPGA_IN_BURST_PIN, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_DIR_OUT(FPGA_IN_BURST_PIN);
    f_in_dma_burst_disable();

    LPC_PIN_CONFIG(FPGA_TEST_PIN, 1, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_DIR_IN(FPGA_TEST_PIN);

    LPC_PIN_CONFIG(PIN_OUT_FLUSH, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_DIR_OUT(PIN_OUT_FLUSH);
    stream_out_flush = 0;
    LPC_PIN_OUT(PIN_OUT_FLUSH, stream_out_flush);





    //LPC_GPIO_PIN_INT->CIENF  = PININTCH(1); //active - low!


    streams_in_run = 0;
    streams_in_req = STREAM_REQ_NONE;
    streams_out_run = 0;
    streams_out_req = STREAM_REQ_NONE;

    f_buf_tmp.buf_size      = sizeof(f_fpga_tmp_buf)/sizeof(f_fpga_tmp_buf[0]);
    f_buf_tmp.min_free_size = 0;
    f_buf_tmp.buf           = f_fpga_tmp_buf;
    f_buf_tmp.cycle         = 0;
    f_buf_tmp.wr_single     = 0;

    buf_in_sdram.buf_size      = STREAM_IN_BUF_SIZE;
    buf_in_sdram.min_free_size = STREAM_FPGA_BUF_SIZE;
    buf_in_sdram.buf           = in_buf;
    buf_in_sdram.cycle         = 1;
    buf_in_sdram.wr_single     = 0;

    buf_in_iface.buf_size = STREAM_IN_IFACE_BUF_SIZE;
    buf_in_iface.min_free_size = 128;
    buf_in_iface.buf           = f_in_iface_buf;
    buf_in_iface.cycle         = 1;
    buf_in_iface.wr_single     = 0;


    buf_out_sdram.buf_size      = STREAM_OUT_BUF_SIZE;
    buf_out_sdram.min_free_size = 1;
    buf_out_sdram.buf           = out_buf;
    buf_out_sdram.cycle         = 1;
    buf_out_sdram.wr_single     = 0;


    buf_out_iface.buf_size = STREAM_OUT_IFACE_BUF_SIZE;
    buf_out_iface.min_free_size = 128;
    buf_out_iface.buf           = f_out_iface_buf;
    buf_out_iface.cycle         = 1;
    buf_out_iface.wr_single     = 0;

    cycle_buf_init(&buf_out_iface);


    for (int i=0; i < STREAM_OUT_CYCLE_BUF_CNT; i++) {
        buf_out_sdram_cycle[i].buf_size = STREAM_OUT_CYCLE_BUF_SIZE;
        buf_out_sdram_cycle[i].min_free_size = 0;
        buf_out_sdram_cycle[i].buf = &out_buf[i*STREAM_OUT_CYCLE_BUF_SIZE];
        buf_out_sdram_cycle[i].cycle         = 1;
        buf_out_sdram_cycle[i].wr_single     = 1;
    }

    NVIC_EnableIRQ(DMA_IRQn);
    f_dma_stage = DMA_STAGE_IDLE;
}







_M0_CODE_ static void f_in_start(void) {
#if DBG_STREAM_IN_TST_CNTR
    f_tst_cntr = 0;
#endif
#if PRINT_DBG_STREAM_STAT
    f_stat_cntr = 0;
#endif
    cycle_buf_init(&buf_in_sdram);
    cycle_buf_init(&buf_in_iface);
    f_stat_init();
    f_fpga_rd_wrds = 0;

    streams_in_run = 1;

    f_dma_in_last_time = lclock_get_ticks();

    stream_in_pin_irq_en();
}

_M0_CODE_ static void f_in_stop(void) {
    stream_in_pin_irq_dis();
    streams_in_run = 0;
    while (f_dma_stage_is_in()) {}
}


_M0_CODE_ static void f_out_start(void) {
    f_fpga_wr_wrds = 0;

    if (stream_out_cycle_mode) {        
        f_cur_out_gen_buf = f_cur_out_load_buf = NULL;
    } else {
        cycle_buf_init(&buf_out_sdram);        
        f_cur_out_gen_buf = &buf_out_sdram;
        f_cur_out_load_buf = &buf_out_sdram;
    }

    out_part_wr_start = out_part_wr_done = f_out_part_rd_start = out_part_rd_done = 0;

    streams_out_run = 1;

#if DBG_STREAM_OUT_TST_CNTR
    f_out_tst_cntr = 0;
#endif
    f_dma_out_last_time = lclock_get_ticks();
}

_M0_CODE_ static void f_out_stop(void) {
    stream_out_pin_irq_dis();
    streams_out_run = 0;
    while (f_dma_stage_is_out()) {}
}





_M0_CODE_ void m0_main(void) {

    t_ltimer wdt_timr;
    ltimer_set(&wdt_timr, LTIMER_MS_TO_CLOCK_TICKS(M0_WDT_TOUT/4));
    m0_wdt_clear();

    f_streams_init();

    for (;;) {
        if (streams_in_req != STREAM_REQ_NONE) {
            if ((streams_in_req == STREAM_REQ_START) &&
                    !streams_in_run) {
                f_in_start();
            } else if ((streams_in_req == STREAM_REQ_STOP) &&
                       streams_in_run) {
                f_in_stop();
            }
            streams_in_req = STREAM_REQ_NONE;
        }

        if (streams_out_req != STREAM_REQ_NONE) {
            if ((streams_out_req == STREAM_REQ_START) &&
                    !streams_out_run) {
                f_out_start();
            } else if ((streams_out_req == STREAM_REQ_STOP) &&
                       streams_out_run) {
                f_out_stop();
                f_cur_out_gen_buf = f_cur_out_load_buf = NULL;
            }
            streams_out_req = STREAM_REQ_NONE;
        }

        if (streams_out_run && stream_out_cycle_mode) {
            if (stream_out_cycle_load_req) {
                if (f_cur_out_load_buf == NULL) {
                    f_cur_out_load_buf = &buf_out_sdram_cycle[0];
                }

                f_cur_out_load_buf->buf_size = stream_out_cycle_load_size;

                if (f_cur_out_load_buf == &buf_out_sdram_cycle[1]) {
                    f_cur_out_load_buf->buf = &out_buf[STREAM_OUT_BUF_SIZE-stream_out_cycle_load_size*stream_out_cycle_multipler];
                }


                cycle_buf_init(f_cur_out_load_buf);
                stream_out_cycle_load_req = 0;
            }

            if (stream_out_cycle_sw_req && cycle_buf_is_full(f_cur_out_load_buf)) {
                /* переход к генерации буфера может быть выполнен, только когда
                 * буфер для генерации полностью заполнен. Первый переход и
                 * мгновенный переход можем выполнить прям тут, а переход по гранце
                 * нужно делать по заверешнию передачи последнего отсчета
                 * старого буфера (в прерывании от DMA) */
                if (f_cur_out_gen_buf == NULL) {                    
                    out_cycle_start_gen_buf(&buf_out_sdram_cycle[0]);
                } else if (stream_out_cycle_sw_flags & X502_OUT_CYCLE_FLAGS_FORCE) {
                    out_cycle_start_gen_buf(CYCLE_NEXT_BUF(f_cur_out_gen_buf));
                }
            }
        }

        /* делаем flush выходного потока, если буфер на передачу пуст */
        int out_flush = streams_out_run && (f_out_sdram_rd_rdy() == 0);
        if (out_flush != stream_out_flush) {
            stream_out_flush = out_flush;
            LPC_PIN_OUT(PIN_OUT_FLUSH, stream_out_flush);
        }


        if (streams_in_run || streams_out_run) {
            /* если нет передачи по DMA - то пробуем запустить слудующую */
            if (f_dma_stage == DMA_STAGE_IDLE) {
                stream_in_pin_irq_dis();
                stream_out_pin_irq_dis();
                if (f_dma_stage == DMA_STAGE_IDLE)
                    f_dma_idle_work();
            }
        }

        /* сброс программного сторожего таймера, контролируемого ядром Cortex-M4 */
        if (ltimer_expired(&wdt_timr)) {
            ltimer_reset(&wdt_timr);
            m0_wdt_clear();
        }

        if (m0_sdram_test_req == STREAM_REQ_START) {
            m0_sdram_test_req = STREAM_REQ_NONE;
            m0_sdram_test_run = 1;
            m0_wdt_clear();
            m0_sdram_test();
            m0_wdt_clear();
            m0_sdram_test_req = STREAM_REQ_NONE;
            m0_sdram_test_run = 0;
        }
    }
}

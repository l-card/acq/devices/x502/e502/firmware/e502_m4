#include <stdint.h>
#include "m0_defs.h"
#include "tests/tests.h"
#include "streams_priv.h"

#define SDRAM_ADDR 0x28000000


/* модуль счетчика, используемого для тестов SDRAM и SPORT */
#define TEST_CNTR_MODULE 35317

/* размер одного банка SDRAM */
#define SDRAM_BANK_SIZE_WORDS       (4UL*1024*1024)
#define SDRAM_BANK_SIZE             (SDRAM_BANK_SIZE_WORDS*2UL)

#define SDRAM_BANK_CNT              4UL
#define SDRAM_SIZE_WORDS            (SDRAM_BANK_SIZE_WORDS*SDRAM_BANK_CNT)

/* адреса банков SDRAM */
static volatile uint16_t* bank1 = (uint16_t*)(SDRAM_ADDR + 0);
static volatile uint16_t* bank2 = (uint16_t*)(SDRAM_ADDR + SDRAM_BANK_SIZE);
static volatile uint16_t* bank3 = (uint16_t*)(SDRAM_ADDR + 2*SDRAM_BANK_SIZE);
static volatile uint16_t* bank4 = (uint16_t*)(SDRAM_ADDR + 3*SDRAM_BANK_SIZE);

_M0_DATA_Z_ volatile t_stream_req m0_sdram_test_req;
_M0_DATA_Z_ volatile int m0_sdram_test_run;

#define TEST_CHECK_OUT() \
    do { \
        if (state->err || (m0_sdram_test_req == STREAM_REQ_STOP)) \
            return ;\
    } while (0);

_M0_CODE_ void m0_sdram_test(void) {
    volatile t_e502_cm4_test_state *state = &test_state;
    unsigned i;
    unsigned short cntr = 0;

    while (!state->err && (m0_sdram_test_req != STREAM_REQ_STOP)) {

        state->stage = 0;

        /* заполняем всю память счетчиком */
        for (i = 0, cntr=0; i < SDRAM_SIZE_WORDS; i++) {
            bank1[i] = cntr;
            if (++cntr==TEST_CNTR_MODULE) {
                cntr = 0;
                TEST_CHECK_OUT();
            }
        }

        TEST_CHECK_OUT();


         /* читаем последовательно и сверяем результат */
        for (i = 0, cntr = 0; i < SDRAM_SIZE_WORDS; i++) {
            uint16_t word = bank1[i];
            if (word!=cntr) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank1[i];
                state->last_wr = cntr;
                state->last_rd = word;
                break;
            }
            if (++cntr==TEST_CNTR_MODULE) {
                cntr = 0;
                TEST_CHECK_OUT();
            }
        }

        TEST_CHECK_OUT();
        state->stage++;


        /* записываем по слову в каждый банк для проверки перекрестной
            записи по разным банкам */
        for (i = 0, cntr=0; i < SDRAM_BANK_SIZE_WORDS; i++) {
            bank1[i] = cntr;
            bank2[i] = ~cntr;
            bank3[i] = cntr^0xAA55;
            bank4[i] = cntr^0x55AA;
            if (++cntr==TEST_CNTR_MODULE) {
                cntr = 0;
                TEST_CHECK_OUT();
            }
        }
        TEST_CHECK_OUT();


        for (i = 0, cntr = 0; i < SDRAM_BANK_SIZE_WORDS; i++) {
            uint16_t word = bank1[i];
            if (word!=cntr) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank1[i];
                state->last_wr = cntr;
                state->last_rd = word;
                break;
            }

            word = bank2[i];
            if (word!=(~cntr & 0xFFFF)) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank2[i];
                state->last_wr = (~cntr & 0xFFFF);
                state->last_rd = word;
                break;
            }
            word = bank3[i];
            if (word!=((cntr^0xAA55) & 0xFFFF)) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank3[i];
                state->last_wr = ((cntr^0xAA55) & 0xFFFF);
                state->last_rd = word;
                break;
            }

            word = bank4[i];
            if (word!=((cntr^0x55AA) & 0xFFFF)) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank4[i];
                state->last_wr = ((cntr^0x55AA) & 0xFFFF);
                state->last_rd = word;
                break;
            }

            if (++cntr==TEST_CNTR_MODULE) {
                cntr = 0;
                TEST_CHECK_OUT();
            }
        }


        TEST_CHECK_OUT();

        /* запись в 4 разных банка с последующим чтением */
        state->stage++;

        for (i = 0, cntr=0; i < (4UL*1024*1024); i++) {
            uint16_t word[4], wr_val[4] = {~cntr, cntr, cntr^0x55AA, cntr^0xAA55};

            bank1[i] = wr_val[0];
            bank2[i] = wr_val[1];
            bank3[i] = wr_val[2];
            bank4[i] = wr_val[3];

            word[1] = bank2[i];
            word[0] = bank1[i];
            word[3] = bank4[i];
            word[2] = bank3[i];


            if (word[0]!=wr_val[0]) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank1[i];
                state->last_wr = wr_val[0];
                state->last_rd = word[0];
                break;
            }
            if (word[1]!=wr_val[1]) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank2[i];
                state->last_wr = wr_val[1];
                state->last_rd = word[1];
                break;
            }
            if (word[2]!=wr_val[2]) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank3[i];
                state->last_wr = wr_val[2];
                state->last_rd = word[2];
                break;
            }
            if (word[3]!=wr_val[3]) {
                state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                state->last_addr = (uint32_t)&bank4[i];
                state->last_wr = wr_val[3];
                state->last_rd = word[3];
                break;
            }

            if (++cntr==TEST_CNTR_MODULE) {
                cntr = 0;
                TEST_CHECK_OUT();
            }
        }
        TEST_CHECK_OUT();

        state->cntr++;
    }
}

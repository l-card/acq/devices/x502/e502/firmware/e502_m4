#include <stdint.h>
#include <string.h>
#include "lprintf.h"

#include "e502_tcp_protocol.h"
#include "eth_settings_func.h"
#include "chip.h"
#include "firmware_buf.h"
#include "fast_crc.h"

#define FLASH_WR_RETRY_CNT 5



#define ETH_SETTINGS_SIGN 0xE502A55A

typedef struct {
    uint32_t sign;
    uint32_t size;
    char     passwd[E502_ETHCONFIG_PASSWD_SIZE];
    uint8_t  data[0];
} t_eth_flash_settings_hdr;





#define ETH_SETTINGS_CRC_SIZE         4
/* размер дополнительных данных, которые добавляются только при сохранении во flash
   (crc, sign, size) */
#define ETH_SETTINGS_FLASH_META_SIZE  (sizeof(t_eth_flash_settings_hdr) + ETH_SETTINGS_CRC_SIZE)
#define ETH_SETTINGS_FLASH_MIN_LEN    (ETH_SETTINGS_FLASH_META_SIZE  + offsetof(t_e502_eth_config, tcp_cmd_port))
#define ETH_SETTINGS_FLASH_MAX_LEN    (512)

#define ETH_SETTIGS_INSTANCES_CNT  2

typedef struct {
    unsigned sector;
    unsigned addr;
    unsigned size;
} t_settings_place_descr;

static const t_settings_place_descr f_settings_places[ETH_SETTIGS_INSTANCES_CNT] = {
    {6, 0x1A00C000, 8*1024},
    {7, 0x1A00E000, 8*1024}
};

static const t_e502_eth_config f_default_set = {
    .flags = E502_ETH_FLAGS_IFACE_ENABLED | E502_ETH_FLAGS_AUTO_IP,
    .inst_name = "",
    .mac = {0x00, 0x05, 0xF7, 0xFF, 0xFF, 0xFA },
    .tcp_cmd_port = E502_TCP_DEFAULT_CMD_PORT,
    .tcp_data_port = E502_TCP_DEFAULT_DATA_PORT,
    .ipv4 = {
        .addr = {192,168,12,233},
        .mask = {255,255,255,0 },
        .gate = {192,168,12,1}
    }
};

static char f_passwd[E502_ETHCONFIG_PASSWD_SIZE];
static uint8_t flash_wr_buf[ETH_SETTINGS_FLASH_MAX_LEN];



static unsigned f_iap_command[6];
static unsigned f_iap_result[5];

static void f_iap_init(void) {
    static int f_init_done = 0;
    if (!f_init_done) {
        IAP_ENTRY_T iap_entry =(IAP_ENTRY_T) IAP_LOCATION;
        f_iap_command[0] = IAP_INIT_CMD;
        iap_entry (f_iap_command, f_iap_result);
        f_init_done = 1;
    }
}


static t_e502_cm4_errs f_erase_sector(unsigned sector) {
    __disable_irq();
    IAP_ENTRY_T iap_entry =(IAP_ENTRY_T) IAP_LOCATION;
    f_iap_init();

    for (unsigned i=0, done=0; (i < FLASH_WR_RETRY_CNT) && !done; i++) {
        f_iap_command[0] = IAP_PREWRRITE_CMD;
        f_iap_command[1] = sector; //sector
        f_iap_command[2] = sector;
        f_iap_command[3] = 0;
        iap_entry (f_iap_command, f_iap_result);
        if (f_iap_result[0] == 0)
            done = 1;
    }

    if (f_iap_result[0] == 0) {
        for (unsigned i=0, done=0; (i < FLASH_WR_RETRY_CNT) && !done; i++) {
            f_iap_command[0] = IAP_ERSSECTOR_CMD;
            f_iap_command[1] = sector; //sector
            f_iap_command[2] = sector;
            f_iap_command[3] = LPC_SYSCLK/1000; //clk (kHz)
            f_iap_command[4] = 0;
            iap_entry (f_iap_command, f_iap_result);
            if (f_iap_result[0] == 0)
                done = 1;
        }
    }
    __enable_irq();

    return f_iap_result[0] == 0 ? E502_CM4_ERR_OK : E502_CM4_ERR_FLASH_OP;
}

static t_e502_cm4_errs f_flash_write(unsigned sector, unsigned addr,
                             const uint8_t *buf, unsigned size) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    IAP_ENTRY_T iap_entry =(IAP_ENTRY_T) IAP_LOCATION;

    __disable_irq();

    f_iap_init();

    for (unsigned i=0, done = 0; (i < FLASH_WR_RETRY_CNT) && !done; i++) {
        f_iap_command[0] = IAP_PREWRRITE_CMD;
        f_iap_command[1] = sector; //sector
        f_iap_command[2] = sector;
        f_iap_command[3] = 0;
        iap_entry (f_iap_command, f_iap_result);
        if (f_iap_result[0] == 0)
            done = 1;
    }

    if (f_iap_result[0] == 0) {
        for (unsigned i=0, done=0; (i < FLASH_WR_RETRY_CNT) && !done; i++) {
            //copy RAM to ROM
            f_iap_command[0] = IAP_WRISECTOR_CMD;
            f_iap_command[1] =  addr; //dest
            f_iap_command[2] = (uint32_t)buf; //src
            f_iap_command[3] = size;   //size
            f_iap_command[4] = LPC_SYSCLK/1000; //clk freq (kHz)
            f_iap_command[5] = 0;
            iap_entry (f_iap_command, f_iap_result);
            if (f_iap_result[0] == 0)
                done = 1;
        }
    }

    __enable_irq();

    if (f_iap_result[0]!=0) {
        err = E502_CM4_ERR_FLASH_OP;
    } else if (memcmp((void*) addr, buf, size)) {
        err = E502_CM4_ERR_FLASH_DATA_COMPARE;
    }

    return err;
}





t_e502_cm4_errs f_wr_set(const t_e502_eth_config *settings, const char *passwd) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    return err;
}

t_e502_cm4_errs eth_settings_save(const t_e502_eth_config *settings, const char *cur_passwd, const char *new_passwd) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    if ((cur_passwd!=NULL) && (strncmp(cur_passwd, f_passwd, sizeof(f_passwd))!=0)) {
        err = E502_CM4_ERR_INVALID_PASSWORD;
    } else {
        uint32_t crc;
        t_eth_flash_settings_hdr *set_hdr = (t_eth_flash_settings_hdr *)flash_wr_buf;
        set_hdr->sign = ETH_SETTINGS_SIGN;
        set_hdr->size = sizeof(t_e502_eth_config) + ETH_SETTINGS_FLASH_META_SIZE;
        strncpy(set_hdr->passwd, new_passwd != NULL ? new_passwd : f_passwd, E502_ETHCONFIG_PASSWD_SIZE - 1);
        set_hdr->passwd[E502_ETHCONFIG_PASSWD_SIZE - 1] = '\0';
        memcpy(set_hdr->data, settings, sizeof(*settings));
        crc = CRC32_Block8(0, (uint8_t*)set_hdr, set_hdr->size - ETH_SETTINGS_CRC_SIZE);
        memcpy(&set_hdr->data[sizeof(*settings)], &crc, sizeof(crc));
        for (unsigned i=0; (i < ETH_SETTIGS_INSTANCES_CNT) && (err == E502_CM4_ERR_OK); i++) {
            if (memcmp(set_hdr, (void*)f_settings_places[i].addr, set_hdr->size)!=0) {
                err = f_erase_sector(f_settings_places[i].sector);
                if (err == E502_CM4_ERR_OK) {
                    err = f_flash_write(f_settings_places[i].sector, f_settings_places[i].addr,
                                        flash_wr_buf, sizeof(flash_wr_buf));
                }
            }
        }
    }

    return err;
}



void eth_settings_load(t_e502_eth_config *settings) {
    lprintf("start load ethernet config!\n");
    unsigned done = 0;
    for (unsigned i=0; (i < ETH_SETTIGS_INSTANCES_CNT) && !done; i++) {
        t_eth_flash_settings_hdr *set_hdr = (t_eth_flash_settings_hdr *)f_settings_places[i].addr;
        if ((set_hdr->sign == ETH_SETTINGS_SIGN) && (set_hdr->size >= ETH_SETTINGS_FLASH_MIN_LEN) &&
                (set_hdr->size <= ETH_SETTINGS_FLASH_MAX_LEN)) {            
            uint32_t calc_crc = CRC32_Block8(0, (uint8_t*)set_hdr, set_hdr->size
                                             - ETH_SETTINGS_CRC_SIZE);
            uint32_t crc;
            uint32_t data_len = set_hdr->size - ETH_SETTINGS_FLASH_META_SIZE;
            memcpy(&crc, &set_hdr->data[data_len], ETH_SETTINGS_CRC_SIZE);
            if (crc == calc_crc) {
                if (data_len > sizeof(t_e502_eth_config))
                    data_len = sizeof(t_e502_eth_config);
                memcpy(settings, set_hdr->data, data_len);
                done = data_len;
                /* прочитанный пароль сохраняем тут для возможной дальнейшей
                   проверки, а в переданной конфигурации возвращаем
                   нулвевую строку */
                memcpy(f_passwd, set_hdr->passwd, sizeof(f_passwd));

                lprintf("found valid eth config: idx = %d, size = %d\n", i, done);
            }
        }
    }

    if (done == 0) {
        lprintf("ethernet config not found! use default config\n");
    }

    if (done < sizeof(t_e502_eth_config)) {
        memcpy(&((uint8_t*)settings)[done], &((uint8_t*)&f_default_set)[done],
               sizeof(t_e502_eth_config)-done);
    }

}



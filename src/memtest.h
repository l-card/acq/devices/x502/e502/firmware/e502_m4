/*================================================================================================*
 * Memory test
 *================================================================================================*/

#ifndef MEMTEST_H_
#define MEMTEST_H_

#include <stdint.h>
#include <stdlib.h>


typedef struct {
    void *addr;
    uint32_t rd_val;
    uint32_t wr_val;
} t_memtest_result;

/*================================================================================================*/
/* Notification function to be called after each pass on every block of data.
   May be used to reset watchdog. */
typedef void (*memtest_callback_t)(void);
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
/* Quick memory test (write sequential numbers)
   Returns: NULL on success or fault address
 */
t_memtest_result memtest_quick_32bit
    (
    void* memory,
    size_t block_size,
    size_t nblocks,
    memtest_callback_t callback
    );
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/* "March C-" on 16-bit memory.
   Returns: NULL on success or fault address
 */
void* memtest_march_c_16bit
    (
    void* memory,
    size_t block_size,
    size_t nblocks,
    memtest_callback_t callback
    );
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
#endif /* MEMTEST_H_ */

#ifndef M0_DEFS_H
#define M0_DEFS_H


#define _M0_CODE_               __attribute__ ((section(".text_m0")))
#define _M0_DATA_I_             __attribute__ ((section(".data_m0")))
#define _M0_DATA_Z_             __attribute__ ((section(".bss_m0")))


extern volatile int m0_wdt_sign;

#define M0_WDT_TOUT 2000

#endif // M0_DEFS_H


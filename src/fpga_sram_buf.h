#ifndef FPGA_SRAM_BUF_H
#define FPGA_SRAM_BUF_H


#define FPGA_SRAM_BUF_CNTR_MSK  0xFFFF
#define FPGA_SRAM_BUF_SIZE      2048
#define FPGA_SRAM_DMA_DAC_BUF   ((volatile uint16_t*)(EMC_ADDRESS_CS0 + 0))
#define FPGA_SRAM_DMA_ADC_BUF   ((const volatile uint16_t*)(EMC_ADDRESS_CS1 + 0))
#define FPGA_SRAM_DMA_ADC_CNTR  ((const volatile uint16_t*)(EMC_ADDRESS_CS1 + 0x2000))
#define FPGA_SRAM_DMA_DAC_CNTR  ((const volatile uint16_t*)(EMC_ADDRESS_CS1 + 0x2002))

#define FPGA_SRAM_DMA_BUF_RING_SIZE FPGA_SRAM_BUF_SIZE

#endif // FPGA_SRAM_BUF_H

#include "fpga.h"

#include "ltimer.h"
#include "lip.h"
#include "chip.h"
#include "lprintf.h"


#include "e502_flash.h"
#include "e502_cm4.h"
#include "e502_cm4_defs.h"
#include "streams.h"
#include "dbg_config.h"

#include "fast_crc.h"

extern uint8_t firmware_buf[];



#define FILL_HARD_ID_FLAGS(devflags, hard_id) do { \
    if (hard_id & 0x01) { \
        devflags |= X502_DEVFLAGS_DAC_PRESENT; \
    } else { \
        devflags &= ~X502_DEVFLAGS_DAC_PRESENT; \
    } \
    if (hard_id & 0x02) { \
        devflags |= X502_DEVFLAGS_GAL_PRESENT; \
    } else { \
        devflags &= ~X502_DEVFLAGS_GAL_PRESENT; \
    } \
    if (hard_id & 0x04) {\
        devflags |= X502_DEVFLAGS_BF_PRESENT; \
    } else { \
        devflags &= ~X502_DEVFLAGS_BF_PRESENT; \
    } \
} while(0)



#define FPGA_FIRM_MAX_SIZE              (FLASH_ADDR_FPGA_FIRM_LAST-FLASH_ADDR_FPGA_FIRM_START+1)

#define FPGA_FIRM_SIGN   0xE502A5A5

/* Время ожидания сигнала NSTATUS от ПЛИС (подтверждение перехода в режим загрузки) */
#define FPGA_LOAD_NSTATUS_TOUT        20
/* Время ожидания начала сигнала о завершении загрузки ПЛИС */
#define FPGA_LOAD_UMODE_WAIT_TOUT    200
/* Время ожидания конца импулься от начала о готовности ПЛС */
#define FPGA_LOAD_CONFIG_PULSE_TOUT  100

/* кол-во мс ожидания после завершения загрузки ПЛИС */
#define FPGA_LOAD_WAIT_AFTER         100

/* максимальное число циклов ожидания при чтении регистров ПЛИС */
#define FPGA_REGS_MAX_WAIT_CYCLES    15

#define FPGA_REGS_WR_CYCLES          6
#define FPGA_REGS_RD_CYCLES          5


typedef struct {
    uint32_t prefix;
    uint32_t firm;
    uint32_t last;
} t_firm_addrs;


typedef struct {
    uint32_t size;
    uint32_t sign;
    uint32_t crc32;
    uint32_t flags;
} t_firm_prefix;

static t_firm_addrs f_firm_addrs[2] = {
    {0x180000, 0x180010, 0x1EFFFF},
    {0x100000, 0x100010, 0x16FFFF}
};



static uint8_t f_sendrecv(LPC_SSP_T *SSP, uint8_t val) {
    uint8_t ret;
    while (!(SSP->SR & SSP_STAT_TNF)) {}
    SSP->DR = val;
    while (!(SSP->SR & SSP_STAT_RNE)) {}
    ret = SSP->DR;
    return ret;
}



static int f_load_fpga(uint8_t *firm, uint32_t size) {
    t_ltimer tmr;
    int done = 0;
    int err = 0;

    e502_proc_stop();

    fpga_reg_write(E502_REGS_IOHARD_GO_SYNC_IO, 0);


    lprintf("fpga: load start (size = %d)\n", size);

    LPC_PIN_CONFIG(PIN_FPGA_USER_MODE, 1, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_DIR_IN(PIN_FPGA_USER_MODE);

    lpc_wdt_reset();

    devflgas &= ~X502_DEVFLAGS_FPGA_LOADED;

    ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(FPGA_LOAD_NSTATUS_TOUT));

    for (done=0; !done && !ltimer_expired(&tmr); ) {
        done = (LPC_PIN_IN(PIN_FPGA_NSTATUS)==1);
    }

    if (!done)
        err = E502_CM4_ERR_FPGA_NSTATUS_TOUT;

    if (!err) {
        LPC_PIN_OUT(PIN_FPGA_NCONFIG, 0);

        ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(FPGA_LOAD_NSTATUS_TOUT));
        for (done=0; !done && !ltimer_expired(&tmr); ) {
            done = (LPC_PIN_IN(PIN_FPGA_NSTATUS)==0);
        }

        LPC_PIN_OUT(PIN_FPGA_NCONFIG, 1);

        if (!done)
            err = E502_CM4_ERR_FPGA_NSTATUS_TOUT;
    }

    if (!err) {
        ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(FPGA_LOAD_NSTATUS_TOUT));
        for (done=0; !done && !ltimer_expired(&tmr); ) {
            done = (LPC_PIN_IN(PIN_FPGA_NSTATUS)==1);
        }
        if (!done)
            err = E502_CM4_ERR_FPGA_NSTATUS_TOUT;
    }

    if (!err) {
        lpc_wdt_reset();

        for (size_t i =0; i < size; i++) {
            uint8_t val = firm[i] = ((firm[i] & 0x0001) << 7) | ((firm[i] & 0x0002) << 5)
                    | ((firm[i] & 0x0004) << 3) | ((firm[i] & 0x0008) << 1)
                    | ((firm[i] & 0x0010) >> 1) | ((firm[i] & 0x0020) >> 3)
                    | ((firm[i] & 0x0040) >> 5) | ((firm[i] & 0x0080) >> 7);

            f_sendrecv(LPC_SSP0, val);
        }

        lpc_wdt_reset();

        ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(FPGA_LOAD_UMODE_WAIT_TOUT));
        for (done=0; !done && !ltimer_expired(&tmr); ) {
            f_sendrecv(LPC_SSP0, 0xFF);
            done = (LPC_PIN_IN(PIN_FPGA_USER_MODE)==0);
        }
        if (!done)
            err = E502_CM4_ERR_FPGA_CONF_DONE_TOUT;
    }

    if (!err) {
        ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(FPGA_LOAD_CONFIG_PULSE_TOUT));
        for (done=0; !done && !ltimer_expired(&tmr); ) {
            done = (LPC_PIN_IN(PIN_FPGA_USER_MODE)==1);
        }
        if (!done)
            err = E502_CM4_ERR_FPGA_CONF_DONE_TOUT;
    }




    if (!err) {
        uint32_t val;
        lpc_wdt_reset();
        ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(FPGA_LOAD_WAIT_AFTER));
        while (!ltimer_expired(&tmr)) {}

        lpc_wdt_reset();

        lprintf("fpga: load done!\n");
        devflgas |= X502_DEVFLAGS_FPGA_LOADED;

        err = fpga_reg_read(E502_REGS_IOHARD_IO_MODE, &val);
        if (!err) {
            if (val & E502_REGBIT_ADC_SLV_CLK_LOCK_Msk) {                
                uint32_t id;
                if (fpga_reg_read(E502_REGS_ARM_HARD_ID, &id) == E502_CM4_ERR_OK) {                    
                    FILL_HARD_ID_FLAGS(devflgas, id);
                    if (((devflgas & X502_DEVFLAGS_INDUSTRIAL) ? 1 : 0) != ((id & (1UL << 31)) ? 1 : 0))
                        err = E502_CM4_ERR_FPGA_FW_INVALID_TEMP_RANGE;
                    lprintf("fpga: dev_id = 0x%08X, devflags = 0x%08X\n", id, devflgas);
                }
            } else {
                lprintf("fatal error: PLDa freq doesn't lock!!!\n");
            }
        }
    }

    if (err) {
        lprintf("fpga: load error (%d)\n", err);
    }


    if (!err)
        e502_proc_start();

    return err;
}





#define FPGA_REGS_BIT_START  (1UL << 15)
#define FPGA_REGS_BIT_WR     (1UL << 14)
#define FPGA_REGS_BIT_ADDR   (0x3FFFUL)


#define FPGA_REGS_RESP_MSK     0x3

#define FPGA_REGS_RESP_NACK    0x0
#define FPGA_REGS_RESP_ERROR   0x3
#define FPGA_REGS_RESP_ACK     0x2
#define FPGA_REGS_RESP_WAIT    0x1


static t_e502_cm4_errs f_fpga_res_errs[] = {
    E502_CM4_ERR_FPGA_REG_NACK,
    E502_CM4_ERR_FPGA_REG_WT_TOUT,
    0,
    E502_CM4_ERR_FPGA_REG_ERROR
};



t_e502_cm4_errs fpga_reg_read(uint16_t addr, uint32_t *val) {
    t_e502_cm4_errs err = devflgas & X502_DEVFLAGS_FPGA_LOADED ? E502_CM4_ERR_OK : E502_CM4_ERR_FPGA_NOT_LOADED;
    if (!err) {
        int ack = FPGA_REGS_RESP_WAIT;
        int retry = 0;

        uint32_t cr0 = LPC_SSP0->CR0;
        uint32_t rcv=0;



        LPC_PIN_CONFIG(PIN_FPGA_REGS_CS, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);

        LPC_SSP0->CR1 &= ~SSP_CR1_SSP_EN;

        LPC_SSP0->CR0 = SSP_CR0_DSS(16) | SSP_CR0_SCR(0);
        LPC_SSP0->CR1 |= SSP_CR1_SSP_EN;


        LPC_SSP0->DR = FPGA_REGS_BIT_START | (addr & FPGA_REGS_BIT_ADDR);
        for (int i=1; i < FPGA_REGS_RD_CYCLES; i++)
            LPC_SSP0->DR = 0;


        for (int i=0; i < FPGA_REGS_RD_CYCLES; i++) {
            volatile uint16_t dummy;
            while (!(LPC_SSP0->SR & SSP_STAT_RNE)) {}
            dummy = LPC_SSP0->DR;

            if (i==(FPGA_REGS_RD_CYCLES-3)) {
                ack = dummy & FPGA_REGS_RESP_MSK;
                if ((ack == FPGA_REGS_RESP_WAIT)  && (++retry!=FPGA_REGS_MAX_WAIT_CYCLES)) {
                    i--;
                    LPC_SSP0->DR = 0;
                }
            }

            if (ack==FPGA_REGS_RESP_ACK) {
                if (i==(FPGA_REGS_RD_CYCLES-2)) {
                    rcv = (uint32_t)dummy << 16;
                } else if (i==(FPGA_REGS_RD_CYCLES-1)) {
                    rcv |= dummy;
                }
            }
        }


        LPC_PIN_CONFIG(PIN_FPGA_USER_MODE, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);

        LPC_SSP0->CR1 &= ~SSP_CR1_SSP_EN;
        LPC_SSP0->CR0 = cr0;
        LPC_SSP0->CR1 |= SSP_CR1_SSP_EN;


        err = f_fpga_res_errs[ack];
        if (err) {
            lprintf("fpga read (addr = 0x%04X) error: %d\n", addr, err);
#if PRINT_DBG_FPGA_REG_RD
        } else {
            lprintf("fpga read done: addr = %04X, val = 0x%08X\n", addr, rcv);
#endif
        }

        if (!err && (val!=NULL))
            *val = rcv;
    }
    return err;
}

t_e502_cm4_errs fpga_reg_write(uint16_t addr, uint32_t val) {
    t_e502_cm4_errs err = devflgas & X502_DEVFLAGS_FPGA_LOADED  ? E502_CM4_ERR_OK : E502_CM4_ERR_FPGA_NOT_LOADED;
    if (!err) {
        uint32_t cr0 = LPC_SSP0->CR0;
        int ack = FPGA_REGS_RESP_WAIT;

#if PRINT_DBG_FPGA_REG_WR
        lprintf("fpga write: addr = %04X, val = 0x%08x\n", addr, val);
#endif

        LPC_PIN_CONFIG(PIN_FPGA_REGS_CS, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);

        LPC_SSP0->CR1 &= ~SSP_CR1_SSP_EN;

        LPC_SSP0->CR0 = SSP_CR0_DSS(16) | SSP_CR0_SCR(0);
        LPC_SSP0->CR1 |= SSP_CR1_SSP_EN;

        while (LPC_SSP0->SR & SSP_STAT_RNE) {
            volatile uint16_t dummy __attribute__((unused));
            dummy = LPC_SSP0->DR;
        }


        LPC_SSP0->DR = FPGA_REGS_BIT_START | FPGA_REGS_BIT_WR | (addr & FPGA_REGS_BIT_ADDR);
        LPC_SSP0->DR = (val>>24) & 0xFF;
        LPC_SSP0->DR = (val>>16) & 0xFF;
        LPC_SSP0->DR = (val>>8) & 0xFF;
        LPC_SSP0->DR = val & 0xFF;

        for (int i=5; i < FPGA_REGS_WR_CYCLES; i++)
            LPC_SSP0->DR = 0;

        for (int i=0; (ack==FPGA_REGS_RESP_WAIT) &&
             (i < (FPGA_REGS_WR_CYCLES+FPGA_REGS_MAX_WAIT_CYCLES)); i++) {
            volatile uint16_t dummy __attribute__((unused));

            if (i>=FPGA_REGS_WR_CYCLES)
                LPC_SSP0->DR = 0;

            while (!(LPC_SSP0->SR & SSP_STAT_RNE)) {}
            dummy = LPC_SSP0->DR;

            if (i>=(FPGA_REGS_WR_CYCLES-1))
                ack = dummy & FPGA_REGS_RESP_MSK;
        }

        LPC_PIN_CONFIG(PIN_FPGA_USER_MODE, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);

        LPC_SSP0->CR1 &= ~SSP_CR1_SSP_EN;
        LPC_SSP0->CR0 = cr0;
        LPC_SSP0->CR1 |= SSP_CR1_SSP_EN;

        err = f_fpga_res_errs[ack];
        if (err)
            lprintf("fpga write (addr = 0x%04X, val = 0x%08X) error: %d\n", addr, val, err);
    }
    return err;
}



t_e502_cm4_errs fpga_boot_for_flag(uint8_t flag) {
    t_firm_prefix prefix;
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    flag &= 1;


    err = e502_flash_read(f_firm_addrs[flag].prefix, (uint8_t*)&prefix, sizeof(prefix));
    if (!err) {
        if ((prefix.sign != FPGA_FIRM_SIGN) || (prefix.size == 0) || (prefix.size > (f_firm_addrs[flag].last - f_firm_addrs[flag].firm + 1))) {
            err = E502_CM4_ERR_FPGA_FW_NOT_PRESENT;
        }
    }

    if (!err) {
        lprintf("fpga: find firmware for flag %d\n", flag);
        err = e502_flash_read(f_firm_addrs[flag].firm, firmware_buf, prefix.size);
        if (!err) {
            uint32_t crc=0;
            crc = CRC32_Block8(crc, firmware_buf, prefix.size);
            if (crc != prefix.crc32) {
                err = E502_CM4_ERR_FPGA_FW_NOT_PRESENT;
                lprintf("fpga: invalid crc: calc - 0x%08X, in info - 0x%08X\n",
                        crc, prefix.crc32);
            }
        }
    }

    if (!err) {
        err = f_load_fpga(firmware_buf, prefix.size);
    }
    return err;
}




t_e502_cm4_errs fpga_firm_load(void) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    uint8_t boot_flag;

    err = e502_flash_read(FLASH_ADDR_FPGA_BOOT_FLAG, &boot_flag, 1);
    if (!err) {
        err = fpga_boot_for_flag(boot_flag);
        if (err) {
            err = fpga_boot_for_flag(boot_flag ^ 1);
        }
    }
    return err;
}

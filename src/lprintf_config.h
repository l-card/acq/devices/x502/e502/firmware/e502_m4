#ifndef LPRINTF_CONFIG_H
#define LPRINTF_CONFIG_H

/* номер порта UART контроллера */
#define LPRINTF_UART_NUM       3
/* конфигурация пина, используемого для передачи данных по UART */
#define LPRINTF_UART_TX_PIN    LPC_PIN_P2_3_U3_TXD

#endif // LPRINTF_CONFIG_H

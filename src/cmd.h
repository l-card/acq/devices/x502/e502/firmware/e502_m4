#ifndef CMD_H
#define CMD_H

#include "chip.h"
#include "e502_cm4_defs.h"

int32_t cmd_proc_rx(uint32_t cmd, const uint8_t* buf, int length, uint32_t param,  t_e502_ifaces intf);
int32_t cmd_proc_tx(uint32_t cmd, uint8_t** buf, int *len, uint32_t param, t_e502_ifaces intf);

#endif // CMD_H

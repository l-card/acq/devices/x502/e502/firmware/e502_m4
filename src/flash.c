#include "chip.h"
#include "e502_flash.h"
#include "flash_iface_lpc43xx_ssp.h"
#include "devices/flash_dev_sst25.h"
#include "x502_eeprom.h"
#include "firmware_buf.h"
#include "fast_crc.h"
#include "e502_cm4.h"
#include "tcp.h"
#include <string.h>

#include "fpga.h"
#include "lprintf.h"

#define PIN_SPI_CS_FLASH    LPC_PIN_P6_2_GPIO3_1

static t_flash_iface data_flash;

#define PROT_MASK (SST25_STATUS_BP0 | SST25_STATUS_BP1 | SST25_STATUS_BP2 | SST25_STATUS_BP3)

static uint8_t prot_bits[] = {
    SST25_STATUS_BP2 | SST25_STATUS_BP1 | SST25_STATUS_BP0,
    SST25_STATUS_BP2 | SST25_STATUS_BP0,
    SST25_STATUS_BP2,
    SST25_STATUS_BP0,
    0
};

static t_e502_cm4_errs flash_err(t_flash_errs err) {
    return err ? E502_CM4_ERR_FLASH_OP : E502_CM4_ERR_OK;
}

/* загрузка информации о модуле из flash-памяти */
t_e502_cm4_errs e502_flash_reload_info(void) {
    uint32_t sign, size;
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    /* проверяем признак правильного описателя в EEPROM и его размер */
    err = e502_flash_read(X502_EEPROM_ADDR_DESCR, (uint8_t*)&sign, (uint32_t)sizeof(sign));
    if (!err)
        err = e502_flash_read(X502_EEPROM_ADDR_DESCR+sizeof(sign), (uint8_t*)&size, (uint32_t)sizeof(size));

    if (!err) {
        if ((sign == X502_EEPROM_SIGN) && (size >= X502_DESCR_MIN_SIZE) && (size <= X502_DESCR_MAX_SIZE)) {
            t_x502_descr* pdescr = (t_x502_descr*)firmware_buf;
            /* читаем весь описатель */
            err = e502_flash_read(X502_EEPROM_ADDR_DESCR, (uint8_t*)pdescr, size);
            /* сверяем crc */
            if (!err) {
                uint32_t crc, crc2;
                crc = CRC32_Block8(0, (uint8_t*)pdescr, (uint32_t)(size-sizeof(crc)));
                crc2 = *(uint32_t*)(&((uint8_t*)pdescr)[pdescr->hdr.size-4]);
                if (crc == crc2) {
                    memcpy(devinfo.serial, pdescr->hdr.serial, sizeof(pdescr->hdr.serial));
                    tcp_fill_default_mac(pdescr->hdr.mac);
                }
            }
        }
    }
    return err;
}



int32_t e502_flash_init(void) {
    LPC_PIN_CONFIG(LPC_PIN_P3_3_SSP0_SCK,  0, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_CONFIG(LPC_PIN_P3_6_SSP0_MISO, 1, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_CONFIG(LPC_PIN_P3_7_SSP0_MOSI, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);
    //LPC_PIN_CONFIG(LPC_PIN_P3_8_SSP0_SSEL, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);



    LPC_PIN_CONFIG(PIN_SPI_CS_FLASH, 0, LPC_PIN_FAST | LPC_PIN_NOPULL);
    LPC_PIN_DIR_OUT(PIN_SPI_CS_FLASH);
    LPC_PIN_OUT(PIN_SPI_CS_FLASH, 1);

    LPC_PIN_CONFIG(PIN_FPGA_NSTATUS, 1, LPC_PIN_NOPULL);
    LPC_PIN_DIR_IN(PIN_FPGA_NSTATUS);

    LPC_PIN_CONFIG(PIN_FPGA_NCONFIG, 0, LPC_PIN_NOPULL);
    LPC_PIN_DIR_OUT(PIN_FPGA_NCONFIG);
    LPC_PIN_OUT(PIN_FPGA_NCONFIG, 1);


    LPC_SSP0->CR0  = SSP_CR0_DSS(8) | SSP_CR0_SCR(0);
    LPC_SSP0->CPSR = 8; /* 2..254 */
    LPC_SSP0->CR1  = SSP_CR1_SSP_EN;



    int err = flash_iface_init_lpc43xx_ssp(&data_flash, 0, PIN_SPI_CS_FLASH);
    if (!err) {
        err = flash_set(&data_flash, &flash_info_sst25);
    }

    if (!err) {
        err = e502_flash_set_prot(X502_EEPROM_PROT_ALL, NULL, 0);
    }

    if (!err) {
        e502_flash_reload_info();
    }

    if (err) {
        lprintf("flash init error - %d!\n", err);
    } else {
        lprintf("flash init done.\n");
    }

    return err;
}


t_e502_cm4_errs e502_flash_erase(uint32_t addr, uint32_t size) {
    return flash_err(flash_erase(&data_flash, addr, size));
}

t_e502_cm4_errs e502_flash_write(uint32_t addr, const uint8_t *data, uint32_t size) {
    return flash_err(flash_write(&data_flash, addr, data, size, 0));
}

t_e502_cm4_errs e502_flash_read(uint32_t addr, uint8_t *data, uint32_t size) {
    return flash_err(flash_read(&data_flash, addr, data, size));
}


t_e502_cm4_errs e502_flash_set_prot(t_x502_eeprom_prot_state prot, const uint8_t* data, uint32_t size) {
    t_e502_cm4_errs err = 0;
    if (prot > X502_EEPROM_PROT_WR_SETTINGS) {
       if ((prot > X502_EEPROM_PROT_WR_ALL) || (size!=2)) {
           err = E502_CM4_ERR_FLASH_PROT_CODE;
       } else {
           uint16_t prot_code = data[0] | (data[1] << 8);
           if (prot_code != 0xA502)
               err = E502_CM4_ERR_FLASH_PROT_CODE;
       }
    }

    if (!err) {
        err = flash_err(flash_sst25_set_status(&data_flash, prot_bits[prot]));
        if (!err) {
            unsigned char status;
            flash_sst25_get_status(&data_flash, &status);
            if ((status & PROT_MASK) !=  prot_bits[prot]) {
                err = E502_CM4_ERR_FLASH_SET_PROT_BITS;
            }
        }
    }

    return err;
}

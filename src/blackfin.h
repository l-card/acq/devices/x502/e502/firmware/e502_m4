#ifndef BLACKFIN_H
#define BLACKFIN_H

#include "e502_cm4_defs.h"
#include <stdint.h>


void bf_init(void);
t_e502_cm4_errs bf_firm_load(uint8_t *ldr, uint32_t size);

t_e502_cm4_errs bf_mem_wr(uint32_t addr, const uint32_t* regs, uint32_t size, int fill);
t_e502_cm4_errs bf_mem_rd(uint32_t addr, uint32_t* regs, uint32_t size);
t_e502_cm4_errs bf_cmd_start(uint16_t cmd_code, uint32_t par,
                      const uint32_t* data, uint32_t size);
t_e502_cm4_errs bf_cmd_check(int32_t *res);
t_e502_cm4_errs bf_cmd_rd_resp(uint32_t* rcv_data, uint32_t rcv_size, uint32_t* recvd_size);
t_e502_cm4_errs bf_cmd_exec(uint16_t cmd_code, uint32_t par,
                            const uint32_t* snd_data, uint32_t snd_size,
                            uint32_t* rcv_data, uint32_t rcv_size, uint32_t tout,
                            uint32_t* recvd_size);

#endif // BLACKFIN_H

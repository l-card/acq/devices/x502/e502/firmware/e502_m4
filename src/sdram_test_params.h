#ifndef SDRAM_TEST_PARAMS_H
#define SDRAM_TEST_PARAMS_H

extern unsigned int _sdram;

#define SDRAM_TEST_MEMPTR   (&_sdram)
#define SDRAM_TEST_MEMSIZE  (32U << 20)
#define SDRAM_TEST_BLKSIZE  (1U << 20)  /* порция, после которой сбрасывается watchdog */



#endif // SDRAM_TEST_PARAMS_H


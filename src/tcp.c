#include "lip.h"
#include "e502_tcp_protocol.h"
#include "lprintf.h"
#include "cmd.h"
#include "streams.h"
#include "e502_cm4.h"
#include "dbg_config.h"
#include "tcp.h"
#include "eth_settings_func.h"



#if DBG_MDNS_TEST
    #include "examples/mdns_test/lip_sample_mdns_test.h"
#endif

#define TCP_CLOSE_TIMEOUT         (30*1000)


#define LIP_CLOSE_TIMEOUT         400

/* буфер на прием должен быть не меньше размера в 1.5 команды, т.к. окно TCP
   может овобождаться только по пол буфера. Делаем на 2 команды, чтобы был запас */
#define TCP_CMD_RX_SIZE  LIP_TCP_RX_BUF_MIN(E502_TCP_CMD_HDR_SIZE + E502_TCP_CMD_RX_DATA_SIZE_MAX)
#define TCP_CMD_TX_SIZE  (E502_TCP_CMD_RESP_SIZE+ E502_TCP_CMD_TX_DATA_SIZE_MAX + 50)

#define TCP_CMD_SOCK_CNT  3
#define TCP_DATA_SOCK_CNT 3


#if LIP_TCP_USER_SOCKET_CNT < (TCP_CMD_SOCK_CNT + TCP_DATA_SOCK_CNT)
    #error "insufficient socket coun in lip config!"
#endif


static int32_t f_tx_data(uint32_t *data, uint32_t len);
static int32_t f_tx_clear(void);
static int32_t f_rx_start(uint32_t* data, uint32_t len);
static uint32_t f_rx_dd_size_done;

static t_stream_in_iface f_in_iface = {
    1024,
    f_tx_data,
    f_tx_clear
};

static t_stream_out_iface f_out_iface = {
    0,
    1,
    f_rx_start,
    NULL
};


/* текущие настройки ethernet */
static t_e502_eth_config f_eth_config;
/* признак, что нужно изменить настройки ethernet */
static int f_eth_config_reload=0;
static uint8_t f_factory_mac[E502_ETHCONFIG_MAC_ADDR_SIZE];


static t_lip_dns_txt_keyval f_srv_txt_vals[] = {
    {"txtvers", "1"},
    {"serial",  NULL},
    {"devname", "E502"}
};


typedef enum {
      CMD_STATE_WT_CMD,
      CMD_STATE_RX_DATA,
      CMD_STATE_RX_DISCARD,
      CMD_STATE_CLOSING
} t_cmd_proc_state;

typedef struct {
    t_cmd_proc_state state;
    int sock;
    uint8_t rx_buf[TCP_CMD_RX_SIZE];
    uint8_t tx_buf[TCP_CMD_TX_SIZE];
    t_e502_tcp_cmd_hdr cmd_hdr;
} t_cmd_proc;

static t_cmd_proc f_cmd_proc[TCP_CMD_SOCK_CNT];
static uint8_t f_tmp_cmd_buf[E502_TCP_CMD_TX_DATA_SIZE_MAX];
static int f_data_socks[TCP_DATA_SOCK_CNT];
static int f_cur_data_sock=-1;


static void f_tcp_send_done_cb(int sock_id, const t_lip_tcp_dd *pdd, t_lip_tcp_dd_status status);
static void f_tcp_recv_done_cb(int sock_id, const t_lip_tcp_dd *pdd, t_lip_tcp_dd_status status);

#if !DBG_MDNS_TEST
static const char *f_e502_name_start = "e502_";
static char f_e502_hostname[LBOOT_SERIAL_SIZE + sizeof(f_e502_name_start)+5];
static char f_e502_inst_name[E502_ETHCONFIG_INSTANCE_NAME_SIZE];




static void f_itoa(unsigned val, char *str) {
    unsigned div, digts;
    const unsigned max_div = (1UL << (sizeof(int)*8-1))/10;

    /* определяем кол-во цифр */
    for (digts=1, div=10; (val >= div) && (div < max_div); digts++, div*=10)
    {}

    if (val >= div) {
        digts++;
    } else {
        div/=10;
    }

    while (div!=0) {
        char c;
        c = val/div;
        val %= div;
        *str++ = '0' + c;
        div/=10;
    }
    *str++ = '\0';
}


static void f_fill_host_name(unsigned conflict_cnt) {
    strcpy(f_e502_hostname, f_e502_name_start);
    strcat(f_e502_hostname, devinfo.serial);
    if (conflict_cnt!=0) {
        int len = strlen(f_e502_hostname);
        f_e502_hostname[len++] = '_';
        f_itoa(conflict_cnt+1, &f_e502_hostname[len]);
    }
}

static void f_fill_srv_inst_name(unsigned conflict_cnt) {
    if (f_eth_config.inst_name[0]=='\0') {
        strcpy(f_e502_inst_name, f_e502_hostname);
    } else {
        strncpy(f_e502_inst_name, f_eth_config.inst_name, sizeof(f_e502_inst_name)-5);
        f_e502_inst_name[sizeof(f_e502_inst_name)-5-1] = '\0';
    }

    if (conflict_cnt!=0) {
        int len = strlen(f_e502_inst_name);
        f_e502_inst_name[len++] = '_';
        f_itoa(conflict_cnt+1, &f_e502_inst_name[len]);
    }
}


static void f_mdns_name_conflict_cb(const t_lip_dns_name *name, t_lip_dns_name *new_name, unsigned conflict_cnt) {
    if (name->name == f_e502_hostname) {
        f_fill_host_name(conflict_cnt);
        new_name->name = f_e502_hostname;
        new_name->parent = lip_mdns_get_parent_domain();
    } else if (name->name == f_e502_inst_name) {
        f_fill_srv_inst_name(conflict_cnt);
        new_name->name = f_e502_inst_name;
        new_name->parent = name->parent;
    }
}

#endif





/* передача ответа на команду */
static void f_send_resp(int sock, int32_t res, uint8_t* buf, uint32_t len) {
    t_e502_tcp_resp_hdr resp;
    resp.sign = E502_TCP_CMD_SIGNATURE;
    resp.res = res;
    if (len > E502_TCP_CMD_TX_DATA_SIZE_MAX)
        len = E502_TCP_CMD_TX_DATA_SIZE_MAX;
    resp.len = len;

    lsock_send(sock, (uint8_t*)&resp, sizeof(resp));
    //xxx варинт с len>socket buf - сейчас - ограничивается, мб нужно поэтапно передавать
    if (len > 0)
        lsock_send(sock, buf, len);
}






void tcp_init(void) {    
    eth_settings_load(&f_eth_config);
    f_eth_config_reload = 0;
    if (f_eth_config.flags & E502_ETH_FLAGS_IFACE_ENABLED) {
        int factory_mac_valid = (((f_factory_mac[0] != 0) || (f_factory_mac[1] != 0) ||
                                 (f_factory_mac[2] != 0) || (f_factory_mac[3] != 0) ||
                                 (f_factory_mac[4] != 0) || (f_factory_mac[5] != 0)) &&
                                  ((f_factory_mac[0] != 0xFF) || (f_factory_mac[1] != 0xFF) ||
                                  (f_factory_mac[2] != 0xFF) || (f_factory_mac[3] != 0xFF) ||
                                  (f_factory_mac[4] != 0xFF) || (f_factory_mac[5] != 0xFF))) ;

        lip_init();

        /* MAC-адрес всегда устанавливается сразу после lip_init().
           Для изменения нужно вызвать lip_close(), lip_init() и снова
            lip_eth_set_mac_addr()*/
        lip_eth_set_mac_addr((!factory_mac_valid || (f_eth_config.flags & E502_ETH_FLAGS_USER_MAC))
                             ? f_eth_config.mac : f_factory_mac);
        if (f_eth_config.flags & E502_ETH_FLAGS_AUTO_IP) {
            lip_ip_link_local_enable(1);
            lip_dhcpc_enable(1);
        } else {
            /* Установка статических параметров IPv4. Можно изменять и во время
             * работы стека */
            lip_ipv4_set_addr(f_eth_config.ipv4.addr);
            lip_ipv4_set_mask(f_eth_config.ipv4.mask);
            lip_ipv4_set_gate(f_eth_config.ipv4.gate);
        }
#if DBG_MDNS_TEST
        lip_sample_mdns_test_init();
#else
        f_fill_host_name(0);
        f_fill_srv_inst_name(0);
        f_srv_txt_vals[1].value = devinfo.serial;

        lip_mdns_set_hostname(f_e502_hostname, f_mdns_name_conflict_cb);
        lip_dnssd_register_local(f_e502_inst_name, E502_TCP_SERVICE_NAME,
                                 f_eth_config.tcp_cmd_port, f_mdns_name_conflict_cb,
                                 f_srv_txt_vals, sizeof(f_srv_txt_vals)/sizeof(f_srv_txt_vals[0]));

#endif
        //открываем сокеты для прослушивания команд и данных
        for (unsigned i=0; i < TCP_CMD_SOCK_CNT; i++) {
            int val = 1;
            f_cmd_proc[i].sock = lsock_create();
            lsock_set_recv_fifo(f_cmd_proc[i].sock, f_cmd_proc[i].rx_buf, TCP_CMD_RX_SIZE);
            lsock_set_send_fifo(f_cmd_proc[i].sock, f_cmd_proc[i].tx_buf, TCP_CMD_TX_SIZE);
            lsock_listen(f_cmd_proc[i].sock, f_eth_config.tcp_cmd_port);
            lsock_set_opt(f_cmd_proc[i].sock, SOL_SOCKET, SO_KEEPALIVE, &val, sizeof(val));
            lsock_set_opt(f_cmd_proc[i].sock, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val));
            f_cmd_proc[i].state = CMD_STATE_WT_CMD;
        }

        for (unsigned i=0; i < TCP_DATA_SOCK_CNT; i++) {
            int val = 1;
            f_data_socks[i] = lsock_create();
            lsock_listen(f_data_socks[i], f_eth_config.tcp_data_port);
            //lsock_rx_drop_mode(f_data_socks[i]);
            lsock_set_opt(f_data_socks[i], SOL_SOCKET, SO_KEEPALIVE, &val, sizeof(val));
            lsock_set_opt(f_data_socks[i], IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val));
        }
        f_cur_data_sock = -1;
    }
}


void tcp_pull(void) {
    if (lip_state() == LIP_STATE_CLOSE_REQUEST) {
        if (lip_pull() != LIP_ERR_SUCCESS) {
            lprintf("lip close!\n");
            lip_close();
        }
    } else if (f_eth_config_reload) {
        if (!tcp_closed() && (lip_state() != LIP_STATE_CLOSE_REQUEST)) {
            tcp_close_req();
        }

        if (tcp_closed()) {
            f_eth_config_reload = 0;
            tcp_init();
        }
    } else if (!tcp_closed()) {
        if (lip_pull() != LIP_ERR_SUCCESS) {
            lprintf("tcp process error!\n");
            lip_close();
        }
    }

    if (!tcp_closed()) {
        for (unsigned i=0; i < TCP_CMD_SOCK_CNT; i++) {
            int dstate = lsock_state(f_data_socks[i]);
            if (f_data_socks[i] == f_cur_data_sock) {
                if (dstate != LIP_TCP_SOCK_STATE_ESTABLISHED) {
                    f_rx_dd_size_done = 0;
                    lprintf("data sock %d: closed, state = %d\n", i, dstate);
                    lsock_close(f_data_socks[i], TCP_CLOSE_TIMEOUT);
                    stream_out_iface_closed(&f_out_iface);
                    stream_in_iface_closed(&f_in_iface);
                    f_cur_data_sock = -1;
                } else {
                    t_lip_tcp_dd dd;
                    if (lsock_recv_cur_dd(f_cur_data_sock, &dd) == LIP_ERR_SUCCESS) {
                        int32_t new_transfered = (dd.trans_cnt - f_rx_dd_size_done)/4;
                        if (new_transfered > 0) {
                            stream_out_recv_done((uint32_t*)&dd.buf[f_rx_dd_size_done],
                                                  new_transfered);
                            f_rx_dd_size_done += new_transfered * sizeof(out_buf[0]);
                        }
                    }
                }
            } else {
                if (dstate == LIP_TCP_SOCK_STATE_ESTABLISHED) {
                    /* если нет активного соединения по данным - используем новое */
                    if (f_cur_data_sock < 0) {
                        lprintf("data sock %d: new active connection\n", i);
                        f_cur_data_sock = f_data_socks[i];
                        f_rx_dd_size_done = 0;
                        //f_tx_end_req = 0;
                    } else {
                        /* иначе - закрываем его (больше одного быть не может) */
                        lsock_close(f_data_socks[i], TCP_CLOSE_TIMEOUT);
                    }
                } else if (dstate == LIP_TCP_SOCK_STATE_CLOSED) {
                    lprintf("data sock %d: restart listen\n", i);
                    lsock_listen(f_data_socks[i], f_eth_config.tcp_data_port);
                } else if (dstate == LIP_TCP_SOCK_STATE_CLOSE_WAIT) {
                    lsock_close(f_data_socks[i], TCP_CLOSE_TIMEOUT);
                }
            }
        }


        for (unsigned i=0; i < TCP_CMD_SOCK_CNT; i++) {
            unsigned rcv_size;
            int res;
            int cstate = lsock_state(f_cmd_proc[i].sock);

            if (cstate == LIP_TCP_SOCK_STATE_CLOSED) {
                /* если завершили закрытие предыдущего соединения
                    начинаем снова прослушивать порт */
                lsock_listen(f_cmd_proc[i].sock, f_eth_config.tcp_cmd_port);
                f_cmd_proc[i].state = CMD_STATE_WT_CMD;
                lprintf("cmd sock %d: restart listen\n", i);
            } else if (cstate == LIP_TCP_SOCK_STATE_CLOSE_WAIT) {
                lprintf("cmd sock %d: closed\n", i);
                lsock_close(f_cmd_proc[i].sock, TCP_CLOSE_TIMEOUT);
                f_cmd_proc[i].state = CMD_STATE_CLOSING;
    #if 0
                //если были изменены настройки - обновление при разрыве соединения
                //по сокету, который запросил обновление настроек
                if (f_upd_socket == f_cmd_proc[i].state)
                    f_eth_set_new_settings();
    #endif
            }

            /* смотрим, сколько данных пришло */
            rcv_size = lsock_recv_rdy_size(f_cmd_proc[i].sock);
            if (f_cmd_proc[i].state == CMD_STATE_WT_CMD) {
                /* если пришел заголовок команды */
                if (rcv_size >= E502_TCP_CMD_HDR_SIZE) {
                    /* принимаем заголовок команды */
                    lsock_recv(f_cmd_proc[i].sock, (uint8_t*)&f_cmd_proc[i].cmd_hdr,
                               E502_TCP_CMD_HDR_SIZE);
                    /* проверяем сигнатуру команды */
                    if (f_cmd_proc[i].cmd_hdr.sign != E502_TCP_CMD_SIGNATURE) {
                        f_send_resp(f_cmd_proc[i].sock, E502_CM4_ERR_CMD_SIGNATURE, 0,0);
                    } else {
                        /* если сигнатура верна - разбор команды */
                        if ((f_cmd_proc[i].cmd_hdr.data_len == 0) &&
                                (f_cmd_proc[i].cmd_hdr.resp_len == 0)) {
                            /* команда без данных -> обрабатываем */
                            res = cmd_proc_rx(f_cmd_proc[i].cmd_hdr.cmd, 0, 0,
                                              f_cmd_proc[i].cmd_hdr.par, E502_IFACE_TCP);
                            f_send_resp(f_cmd_proc[i].sock, res, 0,0);
                        } else if ((f_cmd_proc[i].cmd_hdr.data_len > 0)
                                   && (f_cmd_proc[i].cmd_hdr.resp_len == 0)) {
                            /* если данных больше размера чем буфер -
                               посылаем ошибку и принимаем и выбрасываем данные */
                            if (f_cmd_proc[i].cmd_hdr.data_len > E502_TCP_CMD_RX_DATA_SIZE_MAX) {
                                 f_send_resp(f_cmd_proc[i].sock, E502_CM4_ERR_INVALID_CMD_DATA_SIZE, 0,0);
                                 f_cmd_proc[i].state = CMD_STATE_RX_DISCARD;
                            } else {
                                /* если есть данные на прием - переходим в состояние
                                 * ожидания данных */
                                f_cmd_proc[i].state = CMD_STATE_RX_DATA;
                            }
                        } else if ((f_cmd_proc[i].cmd_hdr.data_len == 0) &&
                                   (f_cmd_proc[i].cmd_hdr.resp_len > 0)) {
                            //команда с данными на передачу...
                            int len = (int)sizeof(f_tmp_cmd_buf);
                            uint8_t* snd_buf = f_tmp_cmd_buf;

                            res = cmd_proc_tx(f_cmd_proc[i].cmd_hdr.cmd, &snd_buf, &len,
                                              f_cmd_proc[i].cmd_hdr.par, E502_IFACE_TCP);
                            //возвращаем данных не больше, чем запросили
                            if (len > (int)f_cmd_proc[i].cmd_hdr.resp_len)
                                len = (int)f_cmd_proc[i].cmd_hdr.resp_len;
                            f_send_resp(f_cmd_proc[i].sock, res, snd_buf, len);
                        }
                    }
                } //if (rcv_size >= LPRT_CMD_HDR_SIZE)
            } else if (f_cmd_proc[i].state == CMD_STATE_RX_DATA) {
                /* если приняли данные от команды - выполнение */
                if (rcv_size >= f_cmd_proc[i].cmd_hdr.data_len) {
                    lsock_recv(f_cmd_proc[i].sock, f_tmp_cmd_buf, f_cmd_proc[i].cmd_hdr.data_len);
                    res = cmd_proc_rx(f_cmd_proc[i].cmd_hdr.cmd, f_tmp_cmd_buf,
                                      f_cmd_proc[i].cmd_hdr.data_len,
                                      f_cmd_proc[i].cmd_hdr.par, E502_IFACE_TCP);
                    f_send_resp(f_cmd_proc[i].sock, res, 0, 0);
                    f_cmd_proc[i].state = CMD_STATE_WT_CMD;
                }
            } else if (f_cmd_proc[i].state == CMD_STATE_RX_DISCARD)    {
                /** @todo */
#if 0
                //принимаем данные от команды, которую не можем обработать
                //пока не примем все данные
                if (rcv_size > 0) {
                    if (rcv_size > f_cur_cmd[i].data_len)
                        rcv_size = f_cur_cmd[i].data_len;
                    lsock_recv(f_cmd_socks[i], f_cmd_tmp, rcv_size);
                    f_cur_cmd[i].data_len-=rcv_size;
                    if (!f_cur_cmd[i].data_len)
                        f_state[i] = TCPSTATE_WT_CMD;
                }
#endif
            }
        }
    }
}

static void f_tcp_send_done_cb(int sock_id, const t_lip_tcp_dd *pdd, t_lip_tcp_dd_status status) {
    stream_in_send_done(pdd->trans_cnt/sizeof(in_buf[0]));
}



static void f_tcp_recv_done_cb(int sock_id, const t_lip_tcp_dd *pdd, t_lip_tcp_dd_status status) {
    if (status == LIP_TCP_DD_STATUS_DONE) {
        if (pdd->trans_cnt != f_rx_dd_size_done) {
            stream_out_recv_done((uint32_t*)&pdd->buf[f_rx_dd_size_done],
                                 (pdd->trans_cnt - f_rx_dd_size_done)/sizeof(out_buf[0]));
        }
        f_rx_dd_size_done = 0;
    }
}


static int32_t f_tx_data(uint32_t *data, uint32_t len) {
    /* ограничение на размер идет с тем, чтобы мы не могли поставить один дескриптор
       на весь буфер, т.к. тогда мы его сможем заполнять только после полного завершения
       передачи. при ограничении же всегда используется несколько заданий и мы можем
       подкачивать его постепенно по мере их выполнений */
    if (len > 2048)
        len = 2048;
    int ret = lsock_send_start(f_cur_data_sock, (uint8_t*)data, len*sizeof(data[0]), f_tcp_send_done_cb);
    return ret < 0 ? ret : (int)len;
}

static int32_t f_tx_clear(void) {
    lprintf("tcp: tx clear req!\n");
    lsock_shutdown(f_cur_data_sock);
    return 0;
}


static int32_t f_rx_start(uint32_t* data, uint32_t len) {
    if (len > 2048)
        len = 2048;
    int ret = lsock_recv_start(f_cur_data_sock, (uint8_t*)data, len*sizeof(data[0]), f_tcp_recv_done_cb);

   return ret < 0 ? ret : (int)len;
}



t_e502_cm4_errs tcp_stream_in_start(void) {
    return stream_in_start(&f_in_iface);
}

t_e502_cm4_errs tcp_stream_out_start(void) {
    return stream_out_start(&f_out_iface);
}

t_e502_cm4_errs tcp_stream_out_cycle_start(uint32_t size) {
    return stream_out_cycle_load_start(&f_out_iface, size);
}



t_e502_cm4_errs tcp_drop_data_con(void) {
    if (f_cur_data_sock>=0) {
        lprintf("drop data sock by cmd!\n");        
        lsock_close(f_cur_data_sock, TCP_CLOSE_TIMEOUT);
        f_cur_data_sock = -1;
    }
    return 0;
}


void tcp_close_req(void) {
    if (lip_state() != LIP_STATE_OFF) {
        lip_close_request(LIP_CLOSE_TIMEOUT);
        /** @todo останов сбора, если идет по TCP */
    }
}

t_e502_cm4_errs tcp_set_eth_config(uint32_t flags, const uint8_t *buf, unsigned size, t_e502_ifaces intf) {
    t_e502_cm4_errs err = (size > sizeof(t_e502_eth_set_config_params)) || (size < E502_ETHCONFIG_SET_HDR_SIZE) ?
                E502_CM4_ERR_INVALID_CMD_PARAMS : E502_CM4_ERR_OK;
    if (err == E502_CM4_ERR_OK) {
        static t_e502_eth_config f_set_cpy;
        t_e502_eth_set_config_params *params = (t_e502_eth_set_config_params*)buf;
        unsigned cfg_size = size - E502_ETHCONFIG_SET_HDR_SIZE;
        int no_pass = (intf == E502_IFACE_USB) && !strncmp(params->passwd, devinfo.serial, E502_ETHCONFIG_PASSWD_SIZE);


        memcpy(&f_set_cpy, &params->cfg, cfg_size);
        if (cfg_size != sizeof(t_e502_eth_config)) {
            memcpy(&((uint8_t*)&f_set_cpy)[cfg_size], &((uint8_t*)&f_eth_config)[cfg_size], sizeof(t_e502_eth_config)-cfg_size);
        }

        err = eth_settings_save(&f_set_cpy, no_pass ? NULL : params->passwd,
                                flags & E502_ETH_CONFIG_FLAGS_SET_NEW_PASSWD ? params->new_passwd : NULL);
        lprintf("tcp config saved (res = %d)\n", err);
        if (err == E502_CM4_ERR_OK) {            
            f_eth_config_reload = 1;
        }
    }
    return err;
}

void tcp_config_reload(void) {
     f_eth_config_reload = 1;
}


void tcp_get_eth_config(uint8_t **buf, int *size) {
    *buf = (uint8_t*)&f_eth_config;
    *size = sizeof(f_eth_config);
}


int tcp_closed() {
    return lip_state() == LIP_STATE_OFF;
}


void tcp_fill_default_mac(const uint8_t *mac) {
    memcpy(f_factory_mac, mac, sizeof(f_factory_mac));
}


void tcp_fill_lboot_params(t_lboot_specpar_tftp *tftp_params) {
    if (!tcp_closed() && lip_ipv4_cur_addr_is_valid()) {
        memset(tftp_params, 0, sizeof(t_lboot_specpar_tftp));
        memcpy(tftp_params->mac, lip_eth_cur_mac(), LIP_MAC_ADDR_SIZE);
        lip_ipv4_addr_cpy(tftp_params->l_ip, lip_ipv4_cur_addr());
        lip_ipv4_addr_cpy(tftp_params->mask, lip_ipv4_cur_mask());
        lip_ipv4_addr_cpy(tftp_params->gate, lip_ipv4_cur_gate());
    }
}



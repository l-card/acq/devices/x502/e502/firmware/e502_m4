#ifndef USB_H
#define USB_H

#include "streams.h"

void usb_init(void);
void usb_pull(void);
void usb_close(void);



t_e502_cm4_errs usb_stream_in_start(void);
t_e502_cm4_errs usb_stream_out_start(void);
t_e502_cm4_errs usb_stream_out_cycle_start(uint32_t size);

void indicate_fatal_error(void);

#endif // USB_H

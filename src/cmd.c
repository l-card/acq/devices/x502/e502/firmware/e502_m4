/* файл содержит обработку команд */
#include "e502_cm4.h"
#include "cmd.h"
#include "tests/tests.h"
#include "fpga.h"
#include "usb.h"
#include "tcp.h"
#include "blackfin.h"
#include "firmware_buf.h"
#include "e502_flash.h"


t_e502_cm4_errs boot_set_req(t_e502_ifaces intf);

int32_t cmd_proc_rx(uint32_t cmd, const uint8_t* buf, int length, uint32_t param,  t_e502_ifaces intf) {
    int32_t err=0;
    switch (cmd) {
        case E502_CM4_CMD_TEST_START:
            if (test_is_running()) {
                err = test_stop();
            }
            if (!err)
                err = test_start(param & 0xFFFF, (param >> 16) & 0xFFFF);
            break;
        case E502_CM4_CMD_TEST_STOP:
            err = test_stop();
            break;
        case E502_CM4_CMD_FPGA_REG_WRITE:
            if (length>=4) {
                uint32_t val = buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
                err = fpga_reg_write(param&0xFFFF,  val);
            } else {
                err = E502_CM4_ERR_INVALID_CMD_PARAMS;
            }
            break;
#ifdef USE_LBOOT
        case E502_CM4_CMD_BOOT:
            err = boot_set_req(intf);
            break;
#endif
        case E502_CM4_CMD_STREAM_START: {
                if (test_is_running()) {
                    err = E502_CM4_ERR_TEST_ALREADY_RUNNING;
                } else {
                    uint16_t idx = (param >> 16) & 0xFFFF;
                    if (idx == E502_STREAM_CH_IN) {
                        err = intf == E502_IFACE_TCP ? tcp_stream_in_start() : usb_stream_in_start();
                    } else if (idx == E502_STREAM_CH_OUT) {
                        err = intf == E502_IFACE_TCP ? tcp_stream_out_start() : usb_stream_out_start();
                    } else {
                        err = E502_CM4_ERR_INVALID_CMD_PARAMS;
                    }
                }
            }
            break;
        case E502_CM4_CMD_STREAM_STOP: {
                if (test_is_running()) {
                    err = E502_CM4_ERR_TEST_ALREADY_RUNNING;
                } else {
                    uint16_t idx = (param >> 16) & 0xFFFF;
                    if (idx == E502_STREAM_CH_IN) {
                        err = stream_in_stop();
                    } else if (idx == E502_STREAM_CH_OUT) {
                        err = stream_out_stop();
                    } else {
                        err = E502_CM4_ERR_INVALID_CMD_PARAMS;
                    }
                }
            }
            break;            
        case E502_CM4_CMD_STREAM_SET_STEP: {
                uint16_t idx   = (param >> 16) & 0xFF;
                uint16_t flags = (param >> 24) & 0xFF;
                if (idx == E502_STREAM_CH_IN) {
                    err = stream_in_set_step(param & 0xFFFF, flags);
                } else if (idx != E502_STREAM_CH_OUT) {
                    err = E502_CM4_ERR_INVALID_CMD_PARAMS;
                }
            }
            break;
        case E502_CM4_CMD_OUT_CYCLE_LOAD:
            err = intf == E502_IFACE_TCP ? tcp_stream_out_cycle_start(param) :
                                           usb_stream_out_cycle_start(param);
            break;
        case E502_CM4_CMD_OUT_CYCLE_SETUP:
            err = stream_out_cycle_setup(param);
            break;
        case E502_CM4_CMD_OUT_CYCLE_STOP:
            err = stream_out_cycle_stop(param);
            break;
        case E502_CM4_CMD_BF_MEM_WRITE: {
                if (length % 4) {
                    err = E502_CM4_ERR_INVALID_CMD_PARAMS;
                } else {
                    uint32_t addr = param;
                    uint32_t size = length/4;
                    err = bf_mem_wr(addr, (uint32_t*)buf, size, 0);
                }
            }
            break;
        case E502_CM4_CMD_FIRM_BUF_WRITE: {
                 uint32_t addr = param;
                 err = firmware_buf_put(addr, buf, length);
            }
            break;
        case E502_CM4_CMD_BF_FIRM_LOAD:
            err = bf_firm_load(firmware_buf, firmware_buf_put_size);
            break;
        case E502_CM4_CMD_DROP_DATA_CON:
            err = tcp_drop_data_con();
            break;
        case E502_CM4_CMD_RELOAD_FPGA:
            err = fpga_firm_load();
            break;
        case E502_CM4_CMD_FLASH_WR:
            err = e502_flash_write(param, buf, length);
            break;
        case E502_CM4_CMD_FLASH_ERASE:
            if (length < 4) {
                err = E502_CM4_ERR_INVALID_CMD_PARAMS;
            } else {
                uint32_t erase_len = buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
                err = e502_flash_erase(param, erase_len);
            }
            break;
        case E502_CM4_CMD_FLASH_SET_PROTECT:
            err = e502_flash_set_prot(param, buf, length);
            break;
        case E502_CM4_CMD_FLASH_RELOAD_INFO:
            err = e502_flash_reload_info();
            break;
        case E502_CM4_CMD_ETH_CFG_SET:
            err = tcp_set_eth_config(param, buf, length, intf);
            break;
        default:
            err = E502_CM4_ERR_UNKNOWN_CMD;
            break;
    }
    return err;
}

int32_t cmd_proc_tx(uint32_t cmd, uint8_t** buf, int* length, uint32_t param, t_e502_ifaces intf) {
    uint8_t *wr_buf = *buf;
    int32_t err = 0;
    switch (cmd) {
        case E502_CM4_CMD_GET_MODULE_NAME:
            wr_buf = (uint8_t*)devinfo.devname;
            *length = LBOOT_DEVNAME_SIZE;
            break;
        case E502_CM4_CMD_GET_MODULE_INFO:
            wr_buf = (uint8_t*)&devinfo;
            *length = sizeof(devinfo);
            break;
        case E502_CM4_CMD_GET_MODULE_MODE:
            wr_buf[0] = LBOOT_MODE_APPL;
            *length = 1;
            break;
        case E502_CM4_CMD_GET_DEVFLAGS:
            wr_buf = (uint8_t*)&devflgas;
            *length = sizeof(devflgas);
            break;
        case E502_CM4_CMD_TEST_GET_STATE:
            wr_buf = (uint8_t*)&test_state;
            *length = sizeof(t_e502_cm4_test_state);
            break;
        case E502_CM4_CMD_FPGA_REG_READ: {
                err = fpga_reg_read(param&0xFFFF, (uint32_t*)wr_buf);
                if (err == E502_CM4_ERR_OK) {
                    *length = 4;
                }
            }
            break;
        case E502_CM4_CMD_STREAM_IS_RUNNING: {
                uint16_t idx = (param >> 16) & 0xFFFF;
                if (idx == E502_STREAM_CH_IN) {
                    wr_buf[0] = stream_in_is_running() ? 1 : 0;
                    *length = 1;
                } else if (idx == E502_STREAM_CH_OUT){
                    wr_buf[0] = stream_out_is_running() ? 1 : 0;
                    *length = 1;
                } else {
                    err = E502_CM4_ERR_INVALID_CMD_PARAMS;
                }
            }
            break;
        case E502_CM4_CMD_BF_MEM_READ: {
                if (*length % 4) {
                    err = E502_CM4_ERR_INVALID_CMD_PARAMS;
                } else {
                    uint32_t addr = param;
                    uint32_t size = *length/4;
                    err = bf_mem_rd(addr, (uint32_t*)wr_buf, size);
                }
            }
            break;
        case E502_CM4_CMD_FLASH_RD:
            err = e502_flash_read(param, wr_buf, *length);
            break;
        case E502_CM4_CMD_ETH_CFG_GET:
            tcp_get_eth_config(&wr_buf, length);
            break;
        case E502_CM4_CMD_OUT_CYCLE_SETUP_CHECK:
            wr_buf[0] = stream_out_cycle_setup_done() ? 1 : 0;
            *length = 1;
            break;
        default:
            err = E502_CM4_ERR_UNKNOWN_CMD;
            break;
    }
    *buf = wr_buf;
    return err;
}

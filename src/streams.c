/*******************************************************************************
 * Данный модуль рализует передачу запросов на запуск/останов потоков
 * ввода/вывода второму ядру (Cortex-M0),
 * а также выполняет передачу данных между интферфейсом и интерфейсным
 * буфром
 ******************************************************************************/



#include "streams.h"
#include "streams_priv.h"
#include "chip.h"
#include "fpga_sram_buf.h"
#include "e502_fpga_regs.h"
#include "e502_cm4.h"
#include "fpga.h"
#include "lprintf.h"

#include <stdlib.h>
#include <inttypes.h>
#include "ltimer.h"

#include "dbg_config.h"


#define IN_IFACE_TIME_TRESHOLD     LTIMER_MS_TO_CLOCK_TICKS(10)

#define STREAM_START_REQ_TOUT      30
#define STREAM_STOP_REQ_TOUT       100




static t_ltimer f_stat_tmr;
static volatile int f_out_wt_buf;
static volatile int f_out_cycle_stop_req_wt;

static t_lclock_ticks f_iface_in_last_time;

static const t_stream_in_iface  *f_in_iface;
static const t_stream_out_iface *f_out_iface;

static uint16_t f_in_tx_step = 4*1024;
static uint8_t f_in_tx_step_force = 0;

#if DBG_STREAM_OUT_CHECK_EMPTY
    static unsigned f_out_empty_cntr;
#endif




int32_t stream_in_set_step(uint16_t step_size, uint8_t flags) {
    f_in_tx_step = step_size;
    f_in_tx_step_force = (flags & 0x80) != 0;
    return 0;
}

t_e502_cm4_errs priv_stream_start(volatile t_stream_req *stream_req, volatile int *stream_run, const char *name) {
    t_ltimer tmr;
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    lprintf("stream %s: start req!\n", name);
    ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(STREAM_START_REQ_TOUT));
    *stream_req = STREAM_REQ_START;

    while (!*stream_run && !ltimer_expired(&tmr)) {}

    if (!*stream_run) {
        err = E502_CM4_ERR_M0_STREAM_START_REQ;
        lprintf("stream %s start error! request tout!\n", name);
    } else {
        lprintf("stream %s: started!\n", name);
    }
    return err;
}

t_e502_cm4_errs priv_stream_stop(volatile t_stream_req *stream_req, volatile int *stream_run, const char *name) {
    t_ltimer tmr;
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    lprintf("stream %s: stop req!\n", name);
    ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(STREAM_STOP_REQ_TOUT));
    *stream_req = STREAM_REQ_STOP;
    while (*stream_run && !ltimer_expired(&tmr)) {}
    if (*stream_run) {
        err = E502_CM4_ERR_M0_STREAM_STOP_REQ;
        lprintf("stream %s stop error! request tout!\n", name);
    } else {
        lprintf("stream %s: stopped!\n", name);
    }
    return err;
}






int32_t stream_in_start(const t_stream_in_iface *iface) {
    t_e502_cm4_errs err;

    if (streams_in_run) {
        stream_in_stop();
    }
    f_in_iface = iface;
    ltimer_set(&f_stat_tmr, LTIMER_MS_TO_CLOCK_TICKS(1000));

    fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_ADC_BUF_CLR_Msk);

    err = priv_stream_start(&streams_in_req, &streams_in_run, "in");
    if (!err)
        f_iface_in_last_time = lclock_get_ticks();
    return err;
}

int32_t stream_in_stop(void) {    
    t_e502_cm4_errs err = priv_stream_stop(&streams_in_req, &streams_in_run, "in");
    f_in_iface->tx_clear();    
    return err;
}

int stream_in_is_running(void) {
    return streams_in_run;
}

void stream_in_iface_closed(const t_stream_in_iface *iface) {
    if (iface == f_in_iface)
        stream_in_stop();
}

t_e502_cm4_errs stream_out_start(const t_stream_out_iface *iface) {
    t_e502_cm4_errs err;
    if (streams_out_run) {
        stream_out_stop();
    }
    ltimer_set(&f_stat_tmr, LTIMER_MS_TO_CLOCK_TICKS(1000));

    f_out_iface = iface;

    if (f_out_iface->rx_clear != NULL) {
        f_out_iface->rx_clear();        
    }
    cycle_buf_init(&buf_out_iface);


    fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_DAC_BUF_CLR_Msk);
    stream_out_part_mode = iface->part_mode;

    err = priv_stream_start(&streams_out_req, &streams_out_run, "out");

    if (!err) {
        f_out_wt_buf = 0;
#if DBG_STREAM_OUT_CHECK_EMPTY
        f_out_empty_cntr = 0;
#endif
    }
    return err;
}

t_e502_cm4_errs stream_out_stop(void) {
    t_e502_cm4_errs err = priv_stream_stop(&streams_out_req, &streams_out_run, "out");
    if (f_out_iface && f_out_iface->rx_clear != NULL) {
        f_out_iface->rx_clear();
    }

    cycle_buf_init(&buf_out_iface);
    fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_DAC_BUF_CLR_Msk);

    stream_out_cycle_mode = 0;
    stream_out_part_mode = 0;
    stream_out_cycle_load_req = 0;
    stream_out_cycle_sw_req = 0;
    f_out_cycle_stop_req_wt = 0;
    stream_out_cycle_sw_flags = 0;    
    stream_out_cycle_load_size = 0;
    stream_out_cycle_gen_size = 0;

    return err;
}

int stream_out_is_running(void) {
    return streams_out_run;
}

void stream_out_iface_closed(const t_stream_out_iface *iface) {
    /* по закрытию интерфейса в буфере убираем запланированный
     * прием данных, чтобы не получилась рассинхронизация при следующем приеме */
    if (iface == f_out_iface) {        
        buf_out_iface.wr_start = buf_out_iface.wr_done;
    }
}

static inline int stream_out_cycle_calc_multipler(unsigned size) {
    int n = 1;
    if (size < OUT_CYCLE_MIN_SIZE) {
        n = (OUT_CYCLE_MIN_SIZE + size  - 1)/ size;
    }
    return n;
}

t_e502_cm4_errs stream_out_cycle_load_start(const t_stream_out_iface *iface, uint32_t size) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    uint32_t multipler = stream_out_cycle_calc_multipler(size);
    if (!stream_out_cycle_mode && streams_out_run) {
        err = E502_CM4_ERR_OUT_STREAM_RUNNING;
    } else if (stream_out_cycle_sw_req) {
        err = E502_CM4_ERR_OUT_NO_CYCLE_BUF;
    } else if ((size * multipler) > (STREAM_OUT_BUF_SIZE - stream_out_cycle_gen_size)) {
        err = E502_CM4_ERR_OUT_CYCLE_BUF_SIZE;
    } else {
        stream_out_cycle_mode = 1;
        stream_out_cycle_multipler = multipler;
        stream_out_cycle_load_size = size;
        stream_out_cycle_load_req = 1;
        if (!streams_out_run) {
            stream_out_start(iface);
        }

        while (stream_out_cycle_load_req) {
            continue;
        }
    }
    return err;
}

t_e502_cm4_errs stream_out_cycle_setup(uint32_t flags) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    if (!stream_out_cycle_mode) {
        err = E502_CM4_ERR_OUT_STREAM_RUNNING;
    } else if (stream_out_cycle_sw_req) {
        err = E502_CM4_ERR_OUT_NO_CYCLE_BUF;
    } else {
        stream_out_cycle_sw_flags = flags;
        stream_out_cycle_sw_req = 1;
    }
    return err;
}

t_e502_cm4_errs stream_out_cycle_stop(uint32_t flags) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    if (stream_out_is_running()) {
        if (!stream_out_cycle_mode) {
            err = E502_CM4_ERR_OUT_STREAM_RUNNING;
        } else {
            if (flags & X502_OUT_CYCLE_FLAGS_FORCE) {
                err = stream_out_stop();
            } else {
                lprintf("cycle stop request!\n");
                stream_out_cycle_load_size = 0;
                stream_out_cycle_gen_size = 0;
                stream_out_cycle_multipler = 1;
                stream_out_cycle_load_req = 1;

                while (stream_out_cycle_load_req) {
                    continue;
                }


                stream_out_cycle_sw_flags = flags;
                stream_out_cycle_sw_req = 1;
                f_out_cycle_stop_req_wt = 1;
            }
        }
    }
    return err;
}

int stream_out_cycle_setup_done(void) {
    return stream_out_cycle_sw_req==0;
}


void stream_pull(void) {
#if PRINT_DBG_STREAM_BUF_STAT
    if (streams_in_run || streams_out_run) {
        if (ltimer_expired(&f_stat_tmr)) {
            ltimer_reset(&f_stat_tmr);
            lprintf("  in buf: %d wrds, out_buf %d wrds, out iface %d\n",
                    cycle_buf_rd_rdy(&buf_in_sdram),
                    cycle_buf_rd_rdy(&buf_out_sdram),
                    cycle_buf_rd_rdy(&buf_out_iface));            
#if DBG_STREAM_OUT_CHECK_EMPTY
            uint32_t reg;
            fpga_reg_read(E502_REGS_IOHARD_OUTSWAP_ERROR, &reg);
            if (reg & 0x2)
                f_out_empty_cntr++;
            lprintf("reg = 0x%04X, ov_cntr = %d\n", reg, f_out_empty_cntr);
#endif
        }
    }

#endif


    if (streams_in_run && (f_in_iface!=NULL)) {
        int end;
        /* попытка передать данные из буфера интерфейса в сам интерфейс  */
        uint32_t rdy_size = cycle_buf_rd_rdy_cont(&buf_in_iface, &end);
        if (rdy_size) {
            t_lclock_ticks time = lclock_get_ticks();
            if ((rdy_size >= f_in_tx_step) || end ||
                    ((f_in_iface->tx_max_step!=0) && (rdy_size >= f_in_iface->tx_max_step))
                    || (!f_in_tx_step_force && ((time - f_iface_in_last_time) > IN_IFACE_TIME_TRESHOLD))
                    ) {
                if (f_in_tx_step_force && (rdy_size > f_in_tx_step)) {
                    rdy_size = f_in_tx_step;
                }
                int32_t put_size = f_in_iface->tx_data(
                            &buf_in_iface.buf[buf_in_iface.rd_start], rdy_size);
                if (put_size > 0) {
                    f_iface_in_last_time = time;
                    buf_in_iface.rd_start+=put_size;
                    if (buf_in_iface.rd_start == buf_in_iface.buf_size) {
                        buf_in_iface.rd_start = 0;
                    }
                }
            }
        }
    }


    if (streams_out_run && f_out_cycle_stop_req_wt && !stream_out_cycle_sw_req) {
        lprintf("stop cycle buf!\n");
        f_out_cycle_stop_req_wt = 0;
        stream_out_stop();
    }

    /* запуск новых заданий на прием данных из интерфейса в f_buf_out_iface  */
    if (streams_out_run && !f_out_wt_buf && (f_out_iface!=NULL)) {
        int end;
        uint32_t rdy_size = cycle_buf_wr_rdy_cont(&buf_out_iface, &end);
        if (rdy_size >= f_out_iface->rx_min_buf) {
            int part_rdy = 1;
            uint32_t next_out_part = 0;
            /* если прием идет не сплошным массивом, то для запуска задания должно
             * быть также свободно место для сохранения информации об завершенной части */
            if (f_out_iface->part_mode) {
                next_out_part = out_part_wr_start + 1;
                if (next_out_part == STREAM_OUT_PARTS_MAX_CNT)
                    next_out_part = 0;
                part_rdy = (next_out_part != out_part_rd_done);
            }
            if (part_rdy) {
                unsigned var;
                __irq_save(var);
                __irq_disable();

                int32_t start_size = f_out_iface->rx_start(
                            &buf_out_iface.buf[buf_out_iface.wr_start], rdy_size);
                if (start_size > 0) {
                    if (f_out_iface->part_mode)
                        out_part_wr_start = next_out_part;

                    uint32_t wr_start = buf_out_iface.wr_start + start_size;
                    if (wr_start == buf_out_iface.buf_size) {
                        wr_start = 0;
                    }
                    buf_out_iface.wr_start = wr_start;
                } else {
                    /* если не удалось добавить задание, то ожидаем завершения
                     * хотя бы одного, чтобы многократно не вызывать данную функцию */
                    f_out_wt_buf = 1;
                }
                __irq_restore(var);
            }
        }
    }
}

void stream_in_send_done(uint32_t size) {
    if (size != 0) {
        unsigned var;
        uint32_t new_rd_done = buf_in_iface.rd_done += size;
        if (new_rd_done== buf_in_iface.buf_size)
            new_rd_done = 0;

        __irq_save(var);
        __irq_disable();
        buf_in_iface.rd_done = new_rd_done;
        buf_in_iface.full = 0;
        __irq_restore(var);
    }
}

void stream_out_recv_done(uint32_t *addr, uint32_t size) {
    if (f_out_iface->part_mode) {        
        uint32_t part_wr = out_part_wr_done;

        out_parts[part_wr].addr = addr;
        out_parts[part_wr].size = size;
        if (++part_wr == STREAM_OUT_PARTS_MAX_CNT)
            part_wr = 0;
        out_part_wr_done = part_wr;
    }

    if (size !=0) {
        uint32_t wr_done = addr - buf_out_iface.buf + size;
        if (wr_done == buf_out_iface.buf_size)
            wr_done = 0;

        buf_out_iface.wr_done = wr_done;
        f_out_wt_buf = 0;
    }
}




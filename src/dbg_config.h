/* Файл определяет, какие отладочные сообщения будут выводится на UART */
#ifndef DBG_CONFIG_H
#define DBG_CONFIG_H



/* вывод сообщений стека usb */
#define PRINT_DBG_LUSB          0
/* вывод каждого чтения из регистра ПЛИС */
#define PRINT_DBG_FPGA_REG_RD   0
/* вывод каждой записи в регистр ПЛИС */
#define PRINT_DBG_FPGA_REG_WR   0

#define PRINT_DBG_STREAM_STAT   0

#define PRINT_DBG_STREAM_BUF_STAT   1

#define PRINT_DBG_BF_LDR_PARSE  0

#define DBG_STREAM_IN_TST_CNTR  0

#define DBG_STREAM_OUT_TST_CNTR 0

#define DBG_STREAM_OUT_CHECK_EMPTY 0


#define DBG_STREAM_AUTOSTART    0

#define DBG_MDNS_TEST           0



#endif // DBG_CONFIG_H

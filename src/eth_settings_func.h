#ifndef ETH_SETTINGS_FUNC_H
#define ETH_SETTINGS_FUNC_H

#include "e502_eth_config.h"
#include "e502_cm4_defs.h"

void eth_settings_load(t_e502_eth_config *settings);
t_e502_cm4_errs eth_settings_save(const t_e502_eth_config *settings, const char *cur_passwd, const char *new_passwd);
t_e502_cm4_errs eth_settings_set_password(const char *new_passwd, const char *cur_passwd);

#endif // ETH_SETTINGS_FUNC_H


#include "tests.h"
#include "streams_priv.h"


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state);

const t_test_descr test_descr_sdram = {
    E502_CM4_TEST_SDRAM,
    f_test_start,
    f_test_proc,
    f_test_stop
};


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = priv_stream_start(&m0_sdram_test_req, &m0_sdram_test_run, "sdram");
    return err;
}

static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {
    return E502_CM4_ERR_OK;
}

static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = priv_stream_stop(&m0_sdram_test_req, &m0_sdram_test_run, "sdram");
    return err;
}

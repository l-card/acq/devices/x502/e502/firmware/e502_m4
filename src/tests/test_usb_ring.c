/* Кольцевой тест USB. Модуль принимает данные по USB в буфер и по возможности
 * ставит их обратно на передачу, не анализируя их (подразумевается, что
 * проверка данных идет на стороне ПК */

#include "tests.h"
#include "lusb.h"
#include "e502_cm4_defs.h"
#include "streams.h"
#include "streams_priv.h"

static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state);


static uint32_t f_buf_cnt;
static uint32_t f_buf_size;
static uint32_t f_out_part_rd_start;



const t_test_descr test_descr_usb_ring = {
    E502_CM4_TEST_USB_RING,
    f_test_start,
    f_test_proc,
    f_test_stop
};


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    //f_buf_size = STREAM_IN_BUF_SIZE/f_buf_cnt;
    //if (f_buf_size > LUSB_DD_DATA_SIZE_MAX/sizeof(in_buf[0]))
    f_buf_size = LUSB_DD_DATA_SIZE_MAX/sizeof(in_buf[0]);
    f_buf_cnt = STREAM_IN_BUF_SIZE/f_buf_size;
    if (f_buf_cnt > STREAM_OUT_PARTS_MAX_CNT)
        f_buf_cnt = STREAM_OUT_PARTS_MAX_CNT;

    out_part_wr_start = out_part_wr_done = f_out_part_rd_start = out_part_rd_done = 0;

    lprintf("usb test. buf size = %d, buf_cnt = %d\n", f_buf_size, f_buf_cnt);

    return err;

}

static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    uint32_t tx_dds =  lusb_ep_get_dd_in_progress(LUSB_TX_EP_IND);
    uint32_t rx_dds = lusb_ep_get_dd_in_progress(LUSB_RX_EP_IND);
    uint32_t next_rx_part = out_part_wr_start+1 == f_buf_cnt ? 0 : out_part_wr_start+1;


    if ((out_part_wr_done != f_out_part_rd_start) && (tx_dds < LUSB_DMA_EP_DESCR_CNT)) {
        if (out_parts[f_out_part_rd_start].size == 0) {
            if (++f_out_part_rd_start==f_buf_cnt)
                f_out_part_rd_start=0;
        } else {
            int usb_err = lusb_ep_add_dd(LUSB_TX_EP_IND, (uint8_t*)out_parts[f_out_part_rd_start].addr,
                                         out_parts[f_out_part_rd_start].size*sizeof(in_buf[0]), 0);
            if (usb_err == LUSB_ERR_SUCCESS) {
                if (++f_out_part_rd_start==f_buf_cnt)
                    f_out_part_rd_start=0;
            }
        }
    }

    if ((rx_dds < LUSB_DMA_EP_DESCR_CNT)
           && (next_rx_part != out_part_rd_done)) {

        int usb_err = lusb_ep_add_dd(LUSB_RX_EP_IND, &in_buf[f_buf_size*out_part_wr_start], f_buf_size*sizeof(in_buf[0]), 0);
        if (usb_err == LUSB_ERR_SUCCESS) {
            if (++out_part_wr_start==f_buf_cnt)
                out_part_wr_start=0;
        }
    }

    return err;
}

static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    lusb_ep_clear(LUSB_TX_EP_IND);
    lusb_ep_clear(LUSB_RX_EP_IND);

    return err;
}


void usb_ring_send_done(uint32_t size) {
    uint32_t part_rd = out_part_rd_done;

    if (++part_rd == f_buf_cnt)
        part_rd = 0;
    out_part_rd_done = part_rd;
}

void usb_ring_recv_done(uint32_t *addr, uint32_t size) {
    uint32_t part_wr = out_part_wr_done;

    out_parts[part_wr].addr = addr;
    out_parts[part_wr].size = size;
    if (++part_wr == f_buf_cnt)
        part_wr = 0;
    out_part_wr_done = part_wr;
}









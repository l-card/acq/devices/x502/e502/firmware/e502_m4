/* Кольцевой тест интерфейса асинхронной памяти для передачи потоков данных
 * между lpc43 и ПЛИС. Используется режим петли для буферов ПЛИС, когда все переданные
 * данные на вывод возвращаются на прием. При этом необходимо отслеживать разницу
 * переданных и принятых данных, чтобы не переполнить буфер ПЛИС.
 * Исползуется 16-битный счетчик, причем для передваемых 32-битных слов, старшее
 * слово является инверсией младшего, чтобы при передаче по 16-битной шине возникал
 * крайний случай одновременного изменения всех бит данных.
 * Для передачи и приема по синхронному интерфейсу используется тот же механизм,
 * что и штатного приема, реализованный на ядре Cotex-M0 за испключением того, что
 * заполнение данных на передачу и чтение и проверка данных на прием в буферах
 * интерфейсов идет вручную в данном тесте */



#include "tests.h"
#include "chip.h"
#include "fpga.h"
#include "fpga_sram_buf.h"
#include "lprintf.h"
#include "streams_priv.h"

#include <stdlib.h>

static uint16_t f_dac_rdy;
static uint16_t f_wr_val;
static uint16_t f_rd_val;

#define TEST_VAL(idx)   (((idx) & 0xFFFF) | ((~(idx) & 0xFFFF) << 16))

#define MAX_IN_FLY 2000

static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state);

const t_test_descr test_descr_sram_buf_ring = {
    E502_CM4_TEST_SRAM_BUF_RING,
    f_test_start,
    f_test_proc,
    f_test_stop
};


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {    
    t_e502_cm4_errs err = E502_CM4_ERR_OK;

    stream_out_cycle_mode = 0;
    stream_out_part_mode = 0;

    if (!err)
        err = fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_ADC_BUF_CLR_Msk |
                   E502_REGBIT_ARM_DMA_DAC_BUF_CLR_Msk);


    if (!err)
        err = fpga_reg_write(E502_REGS_ARM_DMA, E502_REGBIT_ARM_DMA_RING_MODE_Msk);


    if (!err) {
        f_dac_rdy = FPGA_SRAM_DMA_BUF_RING_SIZE;

        f_wr_val = f_rd_val = 0;

        err = priv_stream_start(&streams_in_req, &streams_in_run, "in");
        if (err == E502_CM4_ERR_OK) {
            err = priv_stream_start(&streams_out_req, &streams_out_run, "out");
        }

        if (err != E502_CM4_ERR_OK) {
            priv_stream_stop(&streams_in_req, &streams_in_run, "in");
            priv_stream_stop(&streams_out_req, &streams_out_run, "out");
        }
    }
    return err;
}

static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state) {
    priv_stream_stop(&streams_in_req, &streams_in_run, "in");
    priv_stream_stop(&streams_out_req, &streams_out_run, "out");
    return E502_CM4_ERR_OK;
}

static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {
    if (!state->err) {
        uint32_t out_rdy = cycle_buf_wr_rdy(&buf_out_iface);
        uint16_t in_fly = (uint16_t)(f_wr_val - f_rd_val);
        in_fly = (uint16_t)(MAX_IN_FLY -in_fly);

        if (out_rdy > in_fly)
            out_rdy = in_fly;


        if (out_rdy != 0)  {
            uint32_t wr_pos = buf_out_iface.wr_start;
            for (uint32_t i=0; i < out_rdy; i++) {
                buf_out_iface.buf[wr_pos] = TEST_VAL(f_wr_val);
                f_wr_val++;
                if (++wr_pos == buf_out_iface.buf_size)
                    wr_pos = 0;
            }

            state->last_wr = TEST_VAL(f_wr_val-1);
            buf_out_iface.wr_start = buf_out_iface.wr_done = wr_pos;
        }

        uint32_t in_rdy = cycle_buf_rd_rdy(&buf_in_iface);
        if (in_rdy != 0) {
            uint32_t rd_pos = buf_in_iface.rd_start;
            for (uint32_t i=0; i < in_rdy; i++) {
                uint32_t val = buf_in_iface.buf[rd_pos];
                uint32_t wr_val = TEST_VAL(f_rd_val);
                if (val != wr_val) {
                    state->last_wr = wr_val;
                    state->last_rd = val;
                    state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
                    break;
                } else {
                    if (++rd_pos == buf_in_iface.buf_size)
                        rd_pos = 0;

                    if (++f_rd_val == 0)
                        state->cntr++;
                }
            }

            if (!state->err)
                state->last_rd = TEST_VAL(f_rd_val-1);
            buf_in_iface.rd_start = buf_in_iface.rd_done = rd_pos;
        }
    }

    return E502_CM4_ERR_OK;
}

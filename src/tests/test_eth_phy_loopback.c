#include "tests.h"
#include "lip.h"
#include "tcp.h"
#include "examples/phy_loop/lip_sample_phy_loop.h"

static t_lip_sample_phy_loop_stat f_test_stat;
static uint32_t f_show_cntr;

static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state);
static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state);

const t_test_descr test_descr_eth_phy_loopback = {
    E502_CM4_TEST_ETH_PHY_LOOPBACK,
    f_test_start,
    f_test_proc,
    f_test_stop
};


static t_e502_cm4_errs f_test_start(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    if (lip_state() != LIP_STATE_OFF) {
        lip_close();
    }
    memset(&f_test_stat, 0, sizeof(f_test_stat));
    f_show_cntr = 0;
    lip_sample_phy_loop_init();

    return err;
}

static t_e502_cm4_errs f_test_proc(t_e502_cm4_test_state *state) {
    uint32_t rx_cntr = f_test_stat.rcv_cntr;
    lip_sample_phy_loop_pull(&f_test_stat);
    if (f_test_stat.rcv_errs != 0) {
        state->err = E502_CM4_ERR_TEST_VALUE_MISMATH;
        state->last_rd = f_test_stat.last_err.rcv_wrd;
        state->last_wr = f_test_stat.last_err.exp_wrd;
    } else if (rx_cntr != f_test_stat.rcv_cntr) {
        uint32_t cur_show_cntr = state->last_rd / (32*64*1024);
        state->last_rd = f_test_stat.rcv_cntr;
        if (f_show_cntr != cur_show_cntr) {
            state->cntr++;
            f_show_cntr = cur_show_cntr;
        }
    }

    return E502_CM4_ERR_OK;
}

static t_e502_cm4_errs f_test_stop(t_e502_cm4_test_state *state) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    lip_sample_phy_loop_finish();
    tcp_config_reload();
    return err;
}


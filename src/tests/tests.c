#include "tests.h"
#include <stdlib.h>
#include <string.h>
#include "lprintf.h"

extern t_test_descr test_descr_sram_buf_ring;
extern t_test_descr test_descr_sram_sdram_ring_dma;
extern t_test_descr test_descr_usb_tx_cntr;
extern t_test_descr test_descr_usb_ring;
extern t_test_descr test_descr_spi_slave;
extern t_test_descr test_descr_sdram;
extern t_test_descr test_descr_eth_phy_loopback;

static const t_test_descr* f_test_descrs[] = {
    &test_descr_sram_buf_ring,
    &test_descr_sram_sdram_ring_dma,
    &test_descr_usb_tx_cntr,
    &test_descr_usb_ring,
    &test_descr_spi_slave,
    &test_descr_sdram,
    &test_descr_eth_phy_loopback
};

volatile t_e502_cm4_test_state test_state;
static unsigned f_last_flags;
static const t_test_descr* f_cur_test_descr = NULL;




t_e502_cm4_errs test_start(t_test_number test, unsigned flags) {
    t_e502_cm4_errs err = E502_CM4_ERR_OK;
    f_cur_test_descr = NULL;



    if (test_is_running())
        err = E502_CM4_ERR_TEST_ALREADY_RUNNING;

    if (!err) {
        for (size_t i=0; (f_cur_test_descr==NULL) &&
             (i < sizeof(f_test_descrs)/sizeof(f_test_descrs[0])); i++) {
            if (f_test_descrs[i]->num==test) {
                f_cur_test_descr = f_test_descrs[i];
            }
        }

        if (f_cur_test_descr==0)
            err = E502_CM4_ERR_TEST_INVALID_NUM;
    }

    if (!err) {
        f_last_flags = flags;

        memset((void*)&test_state, 0, sizeof(test_state));
        test_state.test = test;
        err = f_cur_test_descr->init((t_e502_cm4_test_state*)&test_state);
        if (!err) {
            test_state.run = 1;

        }

        lprintf("test started %d (res = %d)\n", test, err);
    }

    return err;

}


t_test_number test_is_running(void) {
    return test_state.run ? test_state.test : E502_CM4_TEST_NONE;
}


t_e502_cm4_errs test_stop() {
    t_e502_cm4_errs err = 0;
    if (!test_is_running()) {
        err = E502_CM4_ERR_TEST_NOT_RUNNING;
    } else {
        test_state.run = 0;
        if (f_cur_test_descr->stop!=NULL) {
            err = f_cur_test_descr->stop((t_e502_cm4_test_state*)&test_state);
            lprintf("test stopped (res = %d)\n", err);
        }
    }

    return err;
}


t_e502_cm4_errs test_restart_last() {
    return test_start(f_cur_test_descr->num, f_last_flags);
}


t_e502_cm4_errs test_pull() {
    return f_cur_test_descr->progr((t_e502_cm4_test_state *)&test_state);
}





#include "chip.h"
#include "ltimer.h"
#include "lip.h"
#include "lprintf.h"
#include "memtest.h"
#include "usb.h"
#include "tcp.h"
#include "fpga.h"
#include "e502_flash.h"
#include "tests/tests.h"
#include "streams.h"
#include "e502_cm4.h"
#include "m0_defs.h"
#include "lprintf.h"
#include "dbg_config.h"
#include "crc.h"
#include "sdram_test_params.h"
#include "blackfin.h"


#define LED_PIN_DEBUG           LPC_PIN_P7_5_GPIO3_13
#define PIN_DEV_INDUSTRIAL      LPC_PIN_PF_11_GPIO7_25
#define PIN_DEV_ETH_SUPPORT     LPC_PIN_PB_1_GPIO5_21
#define PIN_DEV_DAC8581IPW      LPC_PIN_PF_10_GPIO7_24



static t_test_number f_last_test;
static t_ltimer f_led_tmr;

uint32_t devflgas;



t_lboot_devinfo devinfo = {
    "E502",
    "",
    E502_CM4_FIRM_VERSION_STR,
    "",
    "",
    ""
};

__attribute__ ((section(".appl_info")))
const t_app_info g_app_info = { sizeof(t_app_info),
        LBOOT_APP_FLAGS_STABLE,       //flags
        "E502"  //device name
        };

#ifdef USE_LBOOT

static t_lboot_params f_boot_params;
static int f_boot_req = 0;
static t_ltimer f_boot_tmr;
static t_ltimer f_m0_wdt_tmr;


static inline void m0_core_start(void) {
    extern uint32_t _text_m0;
    unsigned val = ~LPC_RGU->RESET_ACTIVE_STATUS1 & ~(1UL << (RGU_M0APP_RST - 32));
    LPC_CREG->M0APPMEMMAP = (uint32_t)&_text_m0;
    LPC_RGU->RESET_CTRL1 = val;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static inline void m0_core_stop(void) {
    LPC_RGU->RESET_CTRL1 = ~LPC_RGU->RESET_ACTIVE_STATUS1 | (1UL << (RGU_M0APP_RST - 32));
}


t_e502_cm4_errs boot_set_req(t_e502_ifaces intf) {
    e502_proc_stop();

    f_boot_params.hdr.flags = LBOOT_REQ_FLAGS_ENABLE_NO_SIGN | LBOOT_REQ_FLAGS_RECOVERY_WR;
    f_boot_params.hdr.devinfo = devinfo;
    f_boot_params.hdr.timeout = 30000;

    if (intf == E502_IFACE_USB) {
        f_boot_params.hdr.size = LBOOT_REQ_SIZE(usb);
        f_boot_params.hdr.bootmode = LBOOT_BOOTMODE_USB;
        f_boot_params.usb.flags = 0;
        f_boot_params.usb.crc = eval_crc16(0, (uint8_t*)&f_boot_params, f_boot_params.hdr.size - sizeof(f_boot_params.usb.crc));
    } else {
        f_boot_params.hdr.size = LBOOT_REQ_SIZE(tftp);
        f_boot_params.hdr.bootmode = LBOOT_BOOTMODE_TFTP_SERVER;
        tcp_fill_lboot_params(&f_boot_params.tftp);
        f_boot_params.tftp.crc = eval_crc16(0, (uint8_t*)&f_boot_params, f_boot_params.hdr.size - sizeof(f_boot_params.tftp.crc));
    }
    f_boot_req = 1;
    ltimer_set(&f_boot_tmr, LTIMER_MS_TO_CLOCK_TICKS(200));
    return 0;
}

static void enter_boot(void) {
    m0_core_stop();
    if (devflgas & X502_DEVFLAGS_IFACE_SUPPORT_ETH) {
        tcp_close_req();
        lpc_wdt_reset();
        while (!tcp_closed())
            tcp_pull();
    }

    lpc_wdt_reset();

    usb_close();
    lprintf("enter bootmode!\n");
    lprintf_close();
    lclock_disable();

    /** @todo отключение пинов, сброс ПЛИС */

    __disable_irq();

     //устанавливаем стек и
    asm volatile (
       "movs r2, #4 \n\t"
       "movs r3, #1 \n\t"
       "skip_cfg: \n\t"
       "LDR  r1, [%1, #0] \n\t"
       "STR  r1, [%0, #0] \n\t"
       "ADD  %0, r2 \n\t"
       "ADD  %1, r2 \n\t"
       "SUBS %2, r3 \n\t"
       "CMP  %2, #0 \n\t"
       "BNE skip_cfg \n\t"
       "movs r2, #0 \n\t"
       "ldr r1, [r2] \n\t"
       "msr MSP, r1 \n\t"
       "ldr r1, [r2,#4] \n\t"
       "bx  r1 \n\t"
       : : "l" (LBOOT_REQ_ADDR), "l" (&f_boot_params), "l" ((f_boot_params.hdr.size+3)/4)
       : "r1", "r2", "r3" );
}

#endif



static void f_wdt_reset(void) {
    lpc_wdt_reset();
}

static void f_hard_config(void) {
    LPC_PIN_CONFIG(PIN_DEV_ETH_SUPPORT, 1, LPC_PIN_PULLUP);
    LPC_PIN_DIR_IN(PIN_DEV_ETH_SUPPORT);
    LPC_PIN_CONFIG(PIN_DEV_INDUSTRIAL, 1, LPC_PIN_PULLUP);
    LPC_PIN_DIR_IN(PIN_DEV_INDUSTRIAL);
    LPC_PIN_CONFIG(PIN_DEV_DAC8581IPW, 1, LPC_PIN_PULLUP);
    LPC_PIN_DIR_IN(PIN_DEV_DAC8581IPW);


    lclock_init();

    /* настройка отладочного UART'а */
    lprintf_uart_init(115200, 8, LPRINTF_UART_PARITY_NONE);

    lprintf("e502 app started. version = %s!\n", devinfo.soft_ver);
    lprintf("clk = %d\n", LPC_SYSCLK);
#ifdef USE_LBOOT
    const t_lboot_info *bootinfo = (const t_lboot_info *)LBOOT_INFO_ADDR;
    lprintf("bootloader version = %d.%d\n", (bootinfo->ver>>8)&0xFF, bootinfo->ver&0xFF);
#endif

    m0_core_stop();

    /* настройка пинов светодиодов */
    LPC_PIN_CONFIG(LED_PIN_DEBUG, 0, LPC_PIN_PULLUP);
    LPC_PIN_DIR_OUT(LED_PIN_DEBUG);
    LPC_PIN_OUT(LED_PIN_DEBUG, 0);
    ltimer_set(&f_led_tmr, LTIMER_MS_TO_CLOCK_TICKS(1000));

    t_lclock_ticks start_ticks = lclock_get_ticks();
    t_memtest_result memtest_res = memtest_quick_32bit(SDRAM_TEST_MEMPTR, SDRAM_TEST_BLKSIZE,
                                    SDRAM_TEST_MEMSIZE / SDRAM_TEST_BLKSIZE, f_wdt_reset);
    if (memtest_res.addr == NULL) {
        lprintf("sdram test done successfully in %d ms\n", LTIMER_CLOCK_TICKS_TO_MS(lclock_get_ticks()-start_ticks));
    } else {
        lprintf("sdram test error! addr 0x%08X, write 0x%08X, read 0x%08X\n",
                (unsigned)memtest_res.addr, memtest_res.wr_val, memtest_res.rd_val);
        indicate_fatal_error();
    }

    m0_core_start();
    ltimer_set(&f_m0_wdt_tmr, LTIMER_MS_TO_CLOCK_TICKS(M0_WDT_TOUT));
    m0_wdt_sign = 0;


    devflgas = X502_DEVFLAGS_IFACE_SUPPORT_USB;

    uint32_t val = LPC_PIN_IN(PIN_DEV_ETH_SUPPORT);
    if (val) {
        devflgas |= X502_DEVFLAGS_IFACE_SUPPORT_ETH;
    } else {
        devflgas &= ~X502_DEVFLAGS_IFACE_SUPPORT_ETH;
    }

    if (!LPC_PIN_IN(PIN_DEV_INDUSTRIAL)) {
        devflgas |= X502_DEVFLAGS_INDUSTRIAL;
    } else {
        devflgas &= ~X502_DEVFLAGS_INDUSTRIAL;
    }

    LBITFIELD_UPD(devflgas, X502_DEVFLAGS_DAC_TYPE,  LPC_PIN_IN(PIN_DEV_DAC8581IPW) == 0 ? 1 : 0);







    lprintf("init devflags = 0x%08X\n", devflgas);
    e502_flash_init();


    usb_init();
    if (devflgas & X502_DEVFLAGS_IFACE_SUPPORT_ETH)
        tcp_init();
}



void e502_proc_stop(void) {
    f_last_test = test_is_running();
    if (f_last_test)
        test_stop();
    if (stream_in_is_running())
        stream_in_stop();
    if (stream_out_is_running())
        stream_out_stop();

}

void e502_proc_start(void) {
    if (f_last_test)
        test_restart_last();
}


void e502_proc_pull(void) {
    if (test_is_running()) {
        test_pull();
    } else {
        stream_pull();

        /* проверка сигнала жизни от Cortex-M0 */
        if (m0_wdt_sign) {
            m0_wdt_sign = 0;
            ltimer_restart(&f_m0_wdt_tmr);
        } else if (ltimer_expired(&f_m0_wdt_tmr)) {
            /* Вообще тут можно было бы конечно перезпустить второе ядро...
             * Но так как это критический случай во многом аналогичный падению
             * основного и он должен быть диагностируем, сейчас просто зависамем
             * с перезагрузкой по аппаратному WDT */
            ltimer_restart(&f_m0_wdt_tmr);
            lprintf("M0 wdt timer expired! system hang...\n");
            for (;;) {}
        }

    }

    if (devflgas & X502_DEVFLAGS_IFACE_SUPPORT_ETH)
        tcp_pull();

    usb_pull();



#ifdef USE_LBOOT
    lpc_wdt_reset();

    if (f_boot_req && ltimer_expired(&f_boot_tmr)) {
        enter_boot();
    }
#endif

    /* мигание отладочным светодиодом раз в секунду для проверки работы программы */
    if (ltimer_expired(&f_led_tmr)) {
        ltimer_reset(&f_led_tmr);
        LPC_PIN_TOGGLE(LED_PIN_DEBUG);
    }
}



/*------------------------------------------------------------------------------------------------*/



int main(void)  {
    f_hard_config();

    fpga_firm_load();

    bf_init();

#if DBG_STREAM_AUTOSTART
    fpga_reg_write(0x030A, 0x00000000);
    fpga_reg_write(0x0318, 0x00000000);
    fpga_reg_write(0x0200, 0x00000080);
    fpga_reg_write(0x0300, 0x00000000);
    fpga_reg_write(0x0302, 0x00000000);
    fpga_reg_write(0x0412, 0x00000000);
    fpga_reg_write(0x0304, 0x00000000);
    fpga_reg_write(0x0308, 0x00000200);
    fpga_reg_write(0x0306, 0x00000000);
    fpga_reg_write(0x020E, 0x00000000);
    fpga_reg_write(0x020D, 0x00000008);
    fpga_reg_write(0x020C, 0x00000010);
    fpga_reg_write(0x020B, 0x00000098);
    fpga_reg_write(0x020A, 0x000000a0);
    fpga_reg_write(0x0209, 0x000000a8);
    fpga_reg_write(0x0208, 0x000000b0);
    fpga_reg_write(0x0207, 0x000000b8);
    fpga_reg_write(0x0206, 0x000000c0);
    fpga_reg_write(0x0205, 0x000000c8);
    fpga_reg_write(0x0204, 0x000000d0);
    fpga_reg_write(0x0203, 0x000000d8);
    fpga_reg_write(0x0202, 0x000000e0);
    fpga_reg_write(0x0201, 0x000000e8);
    fpga_reg_write(0x0200, 0x000000f0);
    fpga_reg_write(0x0300, 0x0000000e);
    fpga_reg_write(0x0302, 0);//0x0000270f);
    fpga_reg_write(0x0412, 0);//0x0000270f);
    fpga_reg_write(0x0304, 0x00000000);
    fpga_reg_write(0x0308, 0x00000200);
    fpga_reg_write(0x0306, 0x00000000);
    fpga_reg_write(0x0419, 0x00000001);    
    fpga_reg_write(0x030C, 0x00000001);
    t_timer tmr;
    timer_set(&tmr, CLOCK_CONF_SECOND);
    while (!timer_expired(&tmr)) {}
    fpga_reg_write(0x0100, 0x00000001);
    fpga_reg_write(0x030A, 0x00000001);

    stream_in_start();
#endif

    for (;;) {
        e502_proc_pull();
    }
}

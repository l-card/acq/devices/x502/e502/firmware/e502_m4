#ifndef TCP_H
#define TCP_H

#include "e502_eth_config.h"
#include "e502_cm4_defs.h"
#include "lboot_req.h"

void tcp_init(void);
void tcp_pull(void);
void tcp_close_req(void);
int tcp_closed(void);

t_e502_cm4_errs tcp_stream_in_start(void);
t_e502_cm4_errs tcp_stream_out_start(void);
t_e502_cm4_errs tcp_stream_out_cycle_start(uint32_t size);


t_e502_cm4_errs tcp_drop_data_con(void);



t_e502_cm4_errs tcp_set_eth_config(uint32_t flags, const uint8_t *buf, unsigned size, t_e502_ifaces intf);
void tcp_get_eth_config(uint8_t **buf, int *size);
void tcp_config_reload(void);

void tcp_fill_default_mac(const uint8_t *mac);
void tcp_fill_lboot_params(t_lboot_specpar_tftp *tftp_params);


#endif // TCP_H

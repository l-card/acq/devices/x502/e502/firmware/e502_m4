#ifndef CYCLE_BUF_H
#define CYCLE_BUF_H

#include <stdint.h>
#include <stddef.h>

typedef struct {
    uint32_t *buf;
    uint32_t buf_size;
    uint16_t min_free_size;
    uint8_t  cycle;
    uint8_t  wr_single;
    volatile uint32_t wr_start;
    volatile uint32_t wr_done;
    volatile uint32_t rd_start;
    volatile uint32_t rd_done;
    volatile int full;
} t_cycle_buf;



static inline void cycle_buf_init(t_cycle_buf *cntrs) {
    cntrs->wr_start = cntrs->wr_done = cntrs->rd_start = cntrs->rd_done = 0;
    cntrs->full = cntrs->buf_size == 0;
}

static inline uint32_t cycle_buf_rd_rdy(t_cycle_buf *cntrs) {
    uint32_t rd_start = cntrs->rd_start;
    uint32_t wr_done = cntrs->wr_done;
    uint32_t rd_rdy = wr_done >= rd_start ? wr_done - rd_start :
                                           cntrs->buf_size - rd_start + wr_done;
    return rd_rdy;
}


static inline uint32_t cycle_buf_wr_rdy_no_full_check(t_cycle_buf *cntrs) {
    if (cntrs->wr_single)
        return cntrs->buf_size - cntrs->wr_start;

    uint32_t wr_start = cntrs->wr_start;
    uint32_t rd_done = cntrs->rd_done;
    uint32_t wr_rdy = rd_done > wr_start ? rd_done - wr_start :
                                           cntrs->buf_size - wr_start + rd_done;
    return wr_rdy > cntrs->min_free_size ? wr_rdy - cntrs->min_free_size : 0;
}

static inline uint32_t cycle_buf_wr_rdy(t_cycle_buf *cntrs) {
    return cntrs->full ? 0 : cycle_buf_wr_rdy_no_full_check(cntrs);
}

static inline uint32_t cycle_buf_rd_rdy_cont(t_cycle_buf *cntrs, int *end_buf) {
    uint32_t rd_start = cntrs->rd_start;
    uint32_t wr_done = cntrs->wr_done;
    uint32_t rd_rdy = wr_done >= rd_start ? wr_done - rd_start :
                                           cntrs->buf_size - rd_start;
    if (end_buf!=NULL)
        *end_buf =  wr_done < rd_start;
    return rd_rdy;
}

static inline uint32_t cycle_buf_wr_rdy_cont(t_cycle_buf *cntrs, int *end_buf) {
    if (cntrs->full)
        return 0;

    uint32_t wr_start = cntrs->wr_start;
    uint32_t rd_done = cntrs->rd_done;
    uint32_t wr_rdy = rd_done > wr_start ? rd_done - wr_start :
                                           cntrs->buf_size - wr_start;

    if ((rd_done > wr_start) || (rd_done==0)) {
        wr_rdy = wr_rdy > cntrs->min_free_size ? wr_rdy - cntrs->min_free_size : 0;
    }

    if (end_buf!=NULL)
        *end_buf =  !(rd_done > wr_start);

    return wr_rdy;
}

static inline void cycle_buf_update_full(t_cycle_buf *cntrs) {
    cntrs->full = cycle_buf_wr_rdy_no_full_check(cntrs) == 0;
}

static inline int cycle_buf_is_full(t_cycle_buf *cntrs) {
    return cntrs->full;
}

static inline int cycle_buf_is_empty(t_cycle_buf *cntrs) {
    return cntrs->wr_single ? cntrs->wr_done == 0 : cntrs->rd_start == cntrs->wr_done;
}


#endif // CYCLE_BUF_H


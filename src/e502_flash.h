#ifndef E502_FLASH_H
#define E502_FLASH_H


#define X502_ADC_RANGE_CNT          6
#define X502_DAC_CH_CNT             2
#define X502_DEVNAME_SIZE           32
#define X502_SERIAL_SIZE            32

typedef struct {
    double offs; /**< смещение нуля */
    double k; /**< коэффициент шкалы */
} t_x502_cbr_coef;

#include "x502_eeprom.h"
#include "e502_cm4_defs.h"


#define FLASH_ADDR_FPGA_BOOT_FLAG       0x170000

#if 0
#define FLASH_ADDR_FPGA_FIRM_PREFIX     0x180000
#define FLASH_ADDR_FPGA_FIRM_START      0x180010
#define FLASH_ADDR_FPGA_FIRM_LAST       0x1EFFFF
#endif


int32_t e502_flash_init(void) ;
t_e502_cm4_errs e502_flash_erase(uint32_t addr, uint32_t size);
t_e502_cm4_errs e502_flash_write(uint32_t addr, const uint8_t *data, uint32_t size);
t_e502_cm4_errs e502_flash_read(uint32_t addr, uint8_t *data, uint32_t size);

t_e502_cm4_errs e502_flash_set_prot(t_x502_eeprom_prot_state prot, const uint8_t* data, uint32_t size);
t_e502_cm4_errs e502_flash_reload_info(void);



#endif // E502_FLASH_H

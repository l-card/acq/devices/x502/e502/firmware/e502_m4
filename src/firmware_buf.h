#ifndef FIRMWARE_BUF_H
#define FIRMWARE_BUF_H

#include <stdint.h>

#define FIRMWARE_BUF_SIZE 0x80000

extern uint8_t  firmware_buf[];
extern uint32_t firmware_buf_put_size;


int firmware_buf_put(uint32_t addr, const uint8_t *data, uint32_t size);

#endif // FIRMWARE_BUF_H

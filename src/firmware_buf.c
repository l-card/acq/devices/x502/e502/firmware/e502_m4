#include "firmware_buf.h"
#include "e502_cm4_defs.h"
#include <string.h>

uint8_t firmware_buf[FIRMWARE_BUF_SIZE] __attribute__((section(".sdram")));
uint32_t firmware_buf_put_size;


int firmware_buf_put(uint32_t addr, const uint8_t *data, uint32_t size) {
    int err = 0;
    if ((addr + size) <= FIRMWARE_BUF_SIZE) {
        memcpy(&firmware_buf[addr], data, size);
        firmware_buf_put_size = addr+size;
    } else {
        err  = E502_CM4_ERR_FIRM_BUF_OVERFLOW;
    }
    return err;
}

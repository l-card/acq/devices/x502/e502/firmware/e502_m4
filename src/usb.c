/* Файл содержит логику работы USB-интерфейсом.
 * Работа идет с использованием lusb_embeded, которая обработывает все
 * стандартные запросы и дает не зависящий от аппаратуры интерфейс для работы с USB.
 *
 * В данном файле содержится логика обработки пользовательских запросов и
 * логика управления светодиодом индикации состояния USB */

#include "lusb.h"
#include "chip.h"
#include "ltimer.h"
#include "cmd.h"
#include "tcp.h"
#include "streams.h"
#include "tests/tests.h"



#include <stdlib.h>

#define USB_RX_DD_SIZE     512

#define LED_PIN_RED        LPC_PIN_PB_3_GPIO5_23
#define LED_PIN_GREEN      LPC_PIN_PB_5_GPIO5_25

#define USB_STATE_NOT_CONNECTED(state_) ((state_ & LUSB_DEVSTATE_SUSPENDED) || !(state_ & LUSB_DEVSTATE_DEFAULT))
#define USB_STATE_CONFIGURED(state_) (!(state_ & LUSB_DEVSTATE_SUSPENDED) && (state_ & LUSB_DEVSTATE_CONFIGURED))

void usb_ring_send_done(uint32_t size);
void usb_ring_recv_done(uint32_t *addr, uint32_t size);

typedef enum {
    USB_LED_STATE_DISCON = 0x03,
    USB_LED_STATE_FS     = 0x01,
    USB_LED_STATE_HS     = 0x02,
    USB_LED_STATE_OFF    = 0x00
} t_usb_led_state;

static t_usb_led_state f_led_state;
static int32_t last_err;

static inline void f_led_set(t_usb_led_state state) {
    LPC_PIN_OUT(LED_PIN_RED, state & 1);
    LPC_PIN_OUT(LED_PIN_GREEN, (state >> 1) & 1);
}

static void f_led_set_state(t_usb_led_state state) {
    f_led_set(state);
    f_led_state = state;
}

void lusb_appl_cb_activity_start(void) {
    f_led_set(USB_LED_STATE_OFF);
}

void lusb_appl_cb_activity_end(void) {
    f_led_set(f_led_state);
}







static int32_t usb_tx_data(uint32_t *data, uint32_t len);
static int32_t usb_tx_clear(void);
static int32_t usb_rx_start(uint32_t* data, uint32_t len);
static int32_t usb_rx_clear(void);

static t_stream_in_iface f_in_iface = {
    LUSB_DD_DATA_SIZE_MAX/(2*sizeof(in_buf[0])),
    usb_tx_data,
    usb_tx_clear
};

static t_stream_out_iface f_out_iface = {
    1,
    USB_RX_DD_SIZE/sizeof(in_buf[0]),
    usb_rx_start,
    usb_rx_clear
};


void usb_init(void) {
    LPC_PIN_CONFIG(LED_PIN_GREEN, 0, LPC_PIN_PULLUP);
    LPC_PIN_DIR_OUT(LED_PIN_GREEN);
    LPC_PIN_CONFIG(LED_PIN_RED, 0, LPC_PIN_PULLUP);
    LPC_PIN_DIR_OUT(LED_PIN_RED);

    lusb_init();
    lusb_connect(1);

    f_led_set_state(USB_LED_STATE_DISCON);
}

void usb_pull(void) {
    lusb_progress();
}


static int32_t usb_tx_data(uint32_t *data, uint32_t len) {
    int usb_err;
    int32_t done_size = 0;

    if (lusb_ep_get_dd_in_progress(LUSB_STDBULK_TX_EP_NUM) < LUSB_DMA_EP_DESCR_CNT/2) {
        if (len > LUSB_DD_DATA_SIZE_MAX/sizeof(in_buf[0])) {
            len = LUSB_DD_DATA_SIZE_MAX/sizeof(in_buf[0]);
        }
        usb_err = lusb_ep_add_dd(LUSB_STDBULK_TX_EP_NUM, data,
                                 len*sizeof(in_buf[0]), 0);

        if (usb_err == LUSB_ERR_SUCCESS) {
            done_size = len;
        }
    }
    return done_size;
}

static int32_t usb_tx_clear(void) {
    lusb_ep_clear(LUSB_STDBULK_TX_EP_NUM);
    return 0;
}

static int32_t usb_rx_start(uint32_t* data, uint32_t len) {
    int usb_err;
    int32_t done_size = 0;

    if (len >= USB_RX_DD_SIZE/sizeof(data[0]))
        len = USB_RX_DD_SIZE/sizeof(data[0]);

    usb_err = lusb_ep_add_dd(LUSB_STDBULK_RX_EP_NUM, data,
                             len*sizeof(in_buf[0]), 0);

    if (usb_err == LUSB_ERR_SUCCESS) {
        done_size = len;
    }
    return done_size;
}

static int32_t usb_rx_clear(void) {
    lusb_ep_clear(LUSB_STDBULK_RX_EP_NUM);
    return 0;
}


int lusb_appl_cb_custom_ctrlreq_rx(const t_lusb_req* req, uint8_t* buf) {
    int res = LUSB_ERR_UNSUPPORTED_REQ;
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR) {
        last_err = cmd_proc_rx(req->request, buf, req->length,
                          ((uint32_t)req->index << 16) | req->val, E502_IFACE_USB);
        if (last_err == 0)
            res = LUSB_ERR_SUCCESS;
    }
    return res;
}

const void* lusb_appl_cb_custom_ctrlreq_tx(const t_lusb_req *req, int *length, uint8_t* tx_buf) {
    uint8_t* wr_buf = NULL;
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR) {
        switch (req->request) {
            case E502_CM4_CMD_GET_USB_SPEED:
                wr_buf = tx_buf;
                wr_buf[0] = lusb_get_speed() == LUSB_SPEED_HIGH ? 1 : 0;
                *length = 1;
                last_err = 0;
                break;
            case E502_CM4_CMD_GET_LAST_ERROR:
                wr_buf = tx_buf;
                wr_buf[0] = last_err & 0xFF;
                wr_buf[1] = (last_err>>8)  & 0xFF;
                wr_buf[2] = (last_err>>16) & 0xFF;
                wr_buf[3] = (last_err>>24) & 0xFF;
                *length = 4;
                last_err = 0;
                break;            
            default: {
                    wr_buf = tx_buf;
                    *length = req->length;
                    last_err = cmd_proc_tx(req->request, &wr_buf, length,
                                      ((uint32_t)req->index << 16) | req->val, E502_IFACE_USB);
                    if (last_err!=0)
                        wr_buf = NULL;
                }
                break;
        }
    }
    return wr_buf;
}


void lusb_appl_cb_devstate_ch(uint8_t old_state, uint8_t new_state) {
    if (USB_STATE_NOT_CONNECTED(new_state)) {
        f_led_set_state(USB_LED_STATE_DISCON);        
    } else {
        if (lusb_get_speed()==LUSB_SPEED_HIGH) {
            f_led_set_state(USB_LED_STATE_HS);            
        } else {
            f_led_set_state(USB_LED_STATE_FS);            
        }
    }

    if (!USB_STATE_CONFIGURED(new_state)) {
        stream_out_iface_closed(&f_out_iface);
        stream_in_iface_closed(&f_in_iface);
    }
}

void lusb_appl_cb_dd_event(uint8_t iface, uint8_t ep_idx, t_lusb_dd_event event, const t_lusb_dd_status* pDD) {
    if (event == LUSB_DD_EVENT_EOT) {
        if (iface==0) {
            if (ep_idx == LUSB_STDBULK_TX_EP_NUM) {
                if (test_is_running()) {
                    usb_ring_send_done(pDD->trans_cnt/sizeof(in_buf[0]));
                } else {
                    stream_in_send_done(pDD->trans_cnt/sizeof(in_buf[0]));
                }
            } else if (ep_idx == LUSB_STDBULK_RX_EP_NUM) {
                if (test_is_running()) {
                    usb_ring_recv_done((uint32_t*)(pDD->last_addr-pDD->trans_cnt),
                                       pDD->trans_cnt/sizeof(out_buf[0]));
                } else {
                    stream_out_recv_done((uint32_t*)(pDD->last_addr-pDD->trans_cnt),
                                     pDD->trans_cnt/sizeof(out_buf[0]));
                }
            }
        }
    }
}

t_e502_cm4_errs usb_stream_in_start(void) {
    tcp_drop_data_con();
    return stream_in_start(&f_in_iface);
}

t_e502_cm4_errs usb_stream_out_start(void) {
    tcp_drop_data_con();
    return stream_out_start(&f_out_iface);
}

t_e502_cm4_errs usb_stream_out_cycle_start(uint32_t size) {
    tcp_drop_data_con();
    return stream_out_cycle_load_start(&f_out_iface, size);
}


void usb_close(void) {
    lusb_close();

    LPC_PIN_CONFIG(LED_PIN_GREEN, 0, 0);
    LPC_PIN_CONFIG(LED_PIN_RED, 0, 0);
    LPC_PIN_DIR_IN(LED_PIN_GREEN);
    LPC_PIN_DIR_IN(LED_PIN_RED);
}


__attribute__ ((__noreturn__)) void indicate_fatal_error(void) {
    t_ltimer tmr;
    ltimer_set(&tmr, LTIMER_MS_TO_CLOCK_TICKS(250));
    LPC_PIN_OUT(LED_PIN_RED, 1);
    LPC_PIN_OUT(LED_PIN_GREEN, 0);
    for (;;) {
        if (ltimer_expired(&tmr)) {
            LPC_PIN_TOGGLE(LED_PIN_RED);
            ltimer_reset(&tmr);
        }
        lpc_wdt_reset();
    }
}


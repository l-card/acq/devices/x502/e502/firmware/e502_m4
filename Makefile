PROJECT_NAME := e502-m4
TARGET_CPU := lpc43xx

USE_LBOOT := 1
OPT := -O2

include ./lib/chip/chip.mk
include ./lib/lprintf/lprintf.mk
LIP_ENABLE_DOC := 1
include ./lib/lip/lip.mk
include ./lib/lusb/lusb.mk


BUILD_GROUPS = M4 M0 USB_DESCR

LDSCRIPT_TEMPLATE := ./lpc4333.ld.S

DEFS            := NDEBUG
DEFS_M4         := STACK_SIZE=256 $(CHIP_DEFS)
DEFS_USB_DESCR  := $(DEFS_M4)
DEFS_M0         := STACK_SIZE=128 $(CHIP_DEFS_M0)


# List C source files here
SRC_M4  := $(CHIP_SRC) \
       $(CHIP_STARTUP_SRC) \
       $(LPRINTF_SRC) \
       $(LUSB_SRC) \
       $(LIP_SRC) \
       ./src/main.c \
       ./src/fpga.c \
       ./src/memtest.c \
       ./src/blackfin.c \
       ./src/streams.c \
       ./src/firmware_buf.c \
       ./src/usb.c \
       ./src/tcp.c \
       ./src/cmd.c \
       ./src/flash.c \
       ./src/eth_settings_func.c \
       ./lib/crc/fast_crc.c \
       ./lib/lip/src/examples/phy_loop/lip_sample_phy_loop.c \
       ./lib/flash/flash.c \
       ./lib/flash/devices/flash_dev_sst25.c \
       ./lib/flash/ports/lpc43xx_ssp/flash_iface_lpc43xx_ssp.c \
       ./lib/ltimer/ports/cm_systick/lclock.c \
       ./src/tests/tests.c \
       ./src/tests/test_sram_buf_ring.c \
       ./src/tests/test_sram_sdram_ring_dma.c \
       ./src/tests/test_usb_tx_cntr.c \
       ./src/tests/test_usb_ring.c \
       ./src/tests/test_spi_slave.c \
       ./src/tests/test_sdram.c \
       ./src/tests/test_eth_phy_loopback.c

SRC_M0 = $(CHIP_STARTUP_SRC_M0) \
       ./src/streams_m0.c \
       ./src/sdram_test_m0.c


SRC_USB_DESCR := $(LUSB_SRC_DESCR)


CFLAGS = $(OPT) -std=gnu99
CFLAGS_M4 = $(CHIP_CFLAGS)
CFLAGS_M0 = $(CHIP_CFLAGS_M0)
CFLAGS_USB_DESCR = $(CFLAGS_M4) $(LUSB_SRC_DESCR_CPFLAGS)

LDFLAGS  = $(CHIP_LDFLAGS) -nostartfiles


SYS_INC_DIRS := $(CHIP_INC_DIRS)

INC_DIRS := ./src \
           $(LPRINTF_INC_DIRS) \
           $(LUSB_INC_DIRS) \
           $(LIP_INC_DIRS) \
          ./lib/crc \
          ./lib/ltimer \
          ./lib/ltimer/ports/cm_systick \
          ./lib/lcspec \
          ./lib/lcspec/gcc \
          ./lib/flash \
          ./lib/flash/ports/lpc43xx_ssp


ifdef USE_LBOOT
    DEFS += FLASH_START_OFFSET=0x00010100
    DEFS += USE_LBOOT
    DEFS += LBOOT_USE_USB LBOOT_USE_TFTP_CLIENT LBOOT_USE_TFTP_SERVER
    DEFS += LBOOT_CHIP_FLASH_START_ADDR=0x1A000000

    SRC_M4   += ./lib/crc/crc.c
else
    DEFS += FLASH_START_OFFSET=0
endif

LDDEFS = $(DEFS)



include ./lib/gnumake_prj/gcc_prj.mk

.PHONY: doc

$(PROJECT_TARGET): $(PROJECT_BIN) $(PROJECT_HEX) doc

$(PROJECT_TARGET_CLEAN): lip_clean

doc: $(LIP_DOC)

